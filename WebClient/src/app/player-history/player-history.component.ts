import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { RateDataService, ImageService } from '../_services/index';
import { saveAs as importedSaveAs } from 'file-saver';
import { IWarriorMatchDetail } from '../_models/IWarriorMatchDetail';
import { fadeInAnimation } from '../_animations/fade-in.animation';
import { PlayerHistoryService } from '../_services/player-history.service';

@Component({
    templateUrl: './player-history.component.html',
    styleUrls: ['./player-history.component.css'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' },
})
export class PlayerHistoryComponent implements OnInit {
    lastGamesCount: number;
    playerName = '';
    ladderType = '';
    lastGames: IWarriorMatchDetail[];
    page = 0;
    loading: boolean;
    gamesCurrentMonthCount: number;
    gamesAllTimeCount: number;

    constructor(
        private playerHistoryService: PlayerHistoryService,
        private rateDataService: RateDataService,
        private imageService: ImageService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.ladderType = 'Permanent';
        this.lastGamesCount = 10;
        this.getLastGames();
        
        const playerNameFromRouter = this.activatedRoute.snapshot.paramMap.get('name');
        if (playerNameFromRouter) {
            this.playerName = playerNameFromRouter;
            this.getLastGames();
        }
    }

    keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    setLadderType(ladderType: string) {
        this.ladderType = ladderType;
    }

    decrementPage() {
        if (this.page === 0) {
            return;
        }
        this.page = this.page - 1;
        this.getLastGames();
    }
    incrementPage() {
        this.page = this.page + 1;
        this.getLastGames();
    }

    getLastGames() {
        this.loading = true;
        this.lastGames = [];
        this.playerHistoryService
            .getPlayerHistory(this.playerName, this.lastGamesCount, this.ladderType, this.page)
            .subscribe(
                (lastApiGames) => {
                    this.loading = false;
                    this.lastGames = lastApiGames;
                },
                (error) => {
                    this.loading = false;
                }
            );
    }

    downloadReplay(gameIdentifier: number) {
        var fileName = 'Game_' + gameIdentifier + '_Replay.BfME2Replay';
        this.rateDataService.downloadFileNew(gameIdentifier).subscribe((blob) => {
            importedSaveAs(blob, fileName);
        });
    }

    getImageSrc(mapCodeName: string) {
        return this.imageService.getImageBase64String(mapCodeName);
    }
}
