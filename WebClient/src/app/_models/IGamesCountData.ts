﻿export interface IGamesCountData {
    gamesCurrentMonthCount: number;
    gamesAllTimeCount: number;
}
