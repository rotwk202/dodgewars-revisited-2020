export class ApiFailedReport {
    gameId: string;
    message: string;
    occuredOn: Date;
}
