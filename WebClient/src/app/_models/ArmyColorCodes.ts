export enum ArmyColorCodes{
    Men = '#0a4dc9',
    Elves = '#458c0e',
    Dwarves = '#b5b80d',
    Mordor = '#b02f12',
    Isengard = '#878483',
    Goblins = '#a1660d',
    Angmar = '#09aba8'
}