export interface IWarriorDetail {
    name: string;
    displayName: string;
    isLooser: boolean;
    PointsGained: number;
    ssGameHost: boolean;
    delta: number;
    army: string;
    armyColorCode: string;
}
