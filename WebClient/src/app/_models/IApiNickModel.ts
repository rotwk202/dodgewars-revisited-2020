﻿export interface IApiNickModel {
    accountID: number;
    nickname: string;
}
