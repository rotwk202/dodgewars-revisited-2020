export class PlayerMedalDetails {
    playerName: string;
    season: string;
    activity: MedalModel;
    totalWins: MedalModel;
    ambition: MedalModel;
    totalPlayersWinsMedalStagesDict: any[];
}

export enum MedalStage {
    Bronze_1 = 'Bronze_1',
    Bronze_2 = 'Bronze_2',
    Bronze_3 = 'Bronze_3',
    Silver_1 = 'Silver_1',
    Silver_2 = 'Silver_2',
    Silver_3 = 'Silver_3',
    Gold_1 = 'Gold_1',
    Gold_2 = 'Gold_2',
    Gold_3 = 'Gold_3',
}

export class MedalModel {
    level: number;
    progressPercentage: number;
    medalCode: MedalStage;
}
