﻿import { IWarrior } from './index';

export class IApiUnreportedMatchDetails {
    MapName: string;
    RaisedOn: Date;
    PlayedOn: Date;
    Players: Array<IWarrior>;
}
