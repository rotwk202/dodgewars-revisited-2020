export interface ITeamWarrior {
    Nickname: string;
    TeamRating: number;
    TeamGamesLadderWinsCount: number;
    TeamGamesLadderLosesCount: number;
    PointsOnTeamGamesLadder: number;
}
