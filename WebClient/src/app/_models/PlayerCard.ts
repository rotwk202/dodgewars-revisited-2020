import { PlayerMedalDetails } from './PlayerMedalDetails';
import { PlayerOpponentInfo } from './PlayerOpponentInfo';

export class PlayerCard {
    playerName: string;
    permaIndividualRating: number;
    permaTeamRating: number;
    monthlyIndividualRating: number;
    monthlyTeamRating: number;
    permaIndividualWinsCount: number;
    permaIndividualLosesCount: number;
    permaTeamWinsCount: number;
    permaTeamLosesCount: number;
    permaIndividualWinRatio: number;
    permaTeamWinRatio: number;
    playerOpponents: PlayerOpponentInfo[];
    playerMedalDetails: PlayerMedalDetails;
}
