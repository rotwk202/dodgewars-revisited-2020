﻿export interface INickLastSeen {
    Nickname: string;
    LastSeen: string;
}
