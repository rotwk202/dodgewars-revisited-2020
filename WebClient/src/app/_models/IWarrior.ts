﻿export interface IWarrior {
    name: string;
    rating: number;
    winsCount: number;
    losesCount: number;
    IsLooser: boolean;
}
