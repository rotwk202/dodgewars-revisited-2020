export class PlayerOpponentInfo {
    playerName: string;
    wonAgainst: number;
    lostTo: number;
}
