import { IWarriorDetail } from './index';

export interface IWarriorMatchDetail {
    playedOn: string;
    gameId: number;
    displayGameId: number;
    mapCodeName: string;
    mapDisplayName: string;
    isNeutralHost: boolean;
    players: IWarriorDetail[];
}
