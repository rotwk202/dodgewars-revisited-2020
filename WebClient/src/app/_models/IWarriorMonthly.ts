﻿export interface IWarriorMonthly {
    name: string;
    rating: number;
    winsCount: number;
    losesCount: number;
}
