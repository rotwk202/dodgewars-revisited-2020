import { Injectable } from '@angular/core';
import { ImageCacheService } from './imageCachce.service';
import { Http, Response } from '@angular/http';

@Injectable()
export class ImageService {
    constructor(private imageCacheService: ImageCacheService, private http: Http) {}

    getImageBase64String(mapCodeName: string): string {
        if (this.imageCacheService.imageExists(mapCodeName)) {
            return this.imageCacheService.getImageBase64String(mapCodeName);
        } else {
            this.http
                .get('/api/image/map-image/' + mapCodeName)
                .map((response: Response) => response.json())
                .subscribe((data) => {
                    const base64String = 'data:image/bmp;base64,' + data;
                    this.imageCacheService.cacheImage(mapCodeName, base64String);
                    return base64String;
                });
        }
    }
}
