import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ARModel } from '../_models/ARModel';
import { Observable } from 'rxjs';
import { ApiNicknamePublic } from '../_models/ApiNicknamePublic';

@Injectable()
export class AdminService {
    constructor(private http: Http) {}

    processAr(arModel: ARModel) {
        const formData = new FormData();
        formData.append('loserPlayerName', arModel.loserPlayerName);
        formData.append('adminToken', arModel.adminToken);
        formData.append('fileName', arModel.replayFile);

        const url = '/api/gamereport/process-ar-web-client/';

        return this.http.post(url, formData).map((response: Response) => {
            return response;
        });
    }

    getAllPlayerNames(secret: string): Observable<ApiNicknamePublic[]> {
        secret = localStorage.getItem('currentToken');
        const url = `/api/account/all-nicknames?secret=${secret}`;

        return this.http
            .get(url)
            .map((response: Response) => response.json())
            .catch((error) => {
                return error;
            });
    }

    getGamePrimaryKeyIds(){
        const url = `/api/admin/game-primarykeyids`;

        return this.http
            .get(url)
            .map((response: Response) => response.json())
            .catch((error) => {
                return error;
            });
    }

    deleteGame(primaryKeyId: number){
        const url = `/api/admin/delete-game?gamePrimaryKeyId=${primaryKeyId}`;

        return this.http
            .delete(url)
            .map((response: Response) => response);
    }
}
