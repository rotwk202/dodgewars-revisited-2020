import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { OverallArmyStat } from '../_models/overallArmyStat';
import { ActivityStat } from '../_models/activityStat';

@Injectable()
export class StatisticsService {
    constructor(private http: Http) {}

    getOverall(): Observable<OverallArmyStat[]> {
        const url = `/api/statistics/overall`;

        return this.http
            .get(url)
            .map((response: Response) => response.json())
            .catch((error) => {
                return error;
            });
    }

    getMonthActivityStats(dateInput: string): Observable<ActivityStat[]> {
        const url = `/api/statistics/month-activity?dateString=${dateInput}`;

        return this.http
            .get(url)
            .map((response: Response) => response.json())
            .catch((error) => {
                return error;
            });
    }

    getPlayerCard(playerName: string, season: string) {
        //season = '2022.1';
        const url = `/api/statistics/player-card?playerName=${playerName}&season=${season}`;

        return this.http.get(url).map((response: Response) => response);
    }

    getPlayerTotalWinsMedal(playerName: string, season: string) {
        season = '2022.1';
        const url = `/api/rating/get-player-medals?season=${season}&playerName=${playerName}`;

        return this.http.get(url).map((response: Response) => response);
    }
}
