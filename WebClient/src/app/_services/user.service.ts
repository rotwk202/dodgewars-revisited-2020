﻿import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { UserModel, IApiNickLoggedOn, IApiNickModel } from '../_models/index';
import { catchError } from 'rxjs/operators/catchError';
import { of } from 'rxjs/observable/of';
import { NameDisplayName } from '../_models/nameDisplayName';

@Injectable()
export class UserService {
    constructor(private http: Http) {}

    register(user: UserModel) {
        const headers = new Headers({ ClientName: 'web' });
        const options = new RequestOptions({ headers: headers });
        const url = '/api/Account/RegisterUser/';

        return this.http.post(url, user, options).map((response: Response) => {
            return response.json();
        });
    }

    getToken(user: UserModel) {
        const headers = new Headers({ ClientName: 'web' });
        const options = new RequestOptions({ headers: headers });
        const url = '/api/Account/GetToken/';

        return this.http.post(url, user, options).map((response: Response) => {
            return response;
        });
    }

    resetPassword(userName: string, password: string): Observable<Response> {
        const url = '/api/account/reset-password/';
        const user = new UserModel();
        user.email = userName;
        user.password = password;

        return this.http.put(url, user).map((response: Response) => {
            return response;
        });
    }

    createNickname(nickname: string, nicknameDisplay: string) {
        const token = localStorage.getItem('currentToken');
        const userEmail = localStorage.getItem('userEmail');
        const url = `/api/Account/CreateNickname?userEmail=${userEmail}&nickname=${nickname}&nicknameDisplay=${nicknameDisplay}`;

        const headers = new Headers();
        headers.append('Token', token);
        headers.append('ClientName', 'web');
        const options = new RequestOptions({ headers: headers });

        return this.http.post(url, null, options).map((response: Response) => {
            return response.json();
        });
    }

    updatePlayerName(playerName: string, playerDisplayName: string) {
        const token = localStorage.getItem('currentToken');
        const userEmail = localStorage.getItem('userEmail');
        const url = `/api/Account/player-name?userEmail=${userEmail}&playerName=${playerName}&playerDisplayName=${playerDisplayName}`;

        const headers = new Headers();
        headers.append('Token', token);
        headers.append('ClientName', 'web');
        const options = new RequestOptions({ headers: headers });

        return this.http.put(url, null, options).map((response: Response) => {
            return response.json();
        });
    }

    whoIsOnline(): Observable<IApiNickLoggedOn[]> {
        const url = '/api/Account/WhoIsOnline/';

        return this.http
            .get(url)
            .map((response: Response) => response.json().Data)
            .catch((error) => {
                return error;
            });
    }

    getAllNicknames(): Observable<IApiNickModel[]> {
        const url = '/api/Account/all-nicknames-public/';

        return this.http
            .get(url)
            .map((response: Response) => {
                return response.json();
            })
            .catch((error) => {
                return error;
            });
    }

    getNameDisplayNames(login: string): Observable<NameDisplayName[]> {
        const url = `/api/Account/player-names?login=${login}`;

        return this.http
            .get(url)
            .map((response: Response) => {
                return response.json();
            })
            .catch((error) => {
                return error;
            });
    }

    isAdminUser(token) {
        const url = `/api/account/is-admin?secret=${token}`;

        const promise = new Promise((resolve, reject) => {
            this.http
                .get(url)
                .map((response: Response) => {
                    return response;
                })
                .subscribe(
                    (response) => {
                        const isAdmin = response.json();
                        resolve(isAdmin);
                    },
                    (error) => {
                        reject(error);
                    }
                );
        });

        return promise;
    }
}
