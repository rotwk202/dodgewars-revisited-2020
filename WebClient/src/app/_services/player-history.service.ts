import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { IWarriorMatchDetail } from '../_models';

@Injectable()
export class PlayerHistoryService {
    constructor(private http: Http) {}

    getPlayerHistory(
        playerName: string,
        lastGamesCount: number,
        ladderType: string,
        pageNumber: number
    ): Observable<IWarriorMatchDetail[]> {
        const url = `/api/playerhistory?gamesCount=${lastGamesCount}&playerName=${playerName}&pageNumber=${pageNumber}&ladderType=${ladderType}`;

        return this.http
            .get(url)
            .map((response: Response) => {
                return response.json();
            })
            .catch((error) => {
                return error;
            });
    }
}
