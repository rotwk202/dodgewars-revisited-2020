import { Injectable } from '@angular/core';

interface IDictionary {
    add(key: string, value: any): void;
    get(key: string): string;
    keyExists(key: string): boolean;
}

export class Dictionary implements IDictionary {
    _keys: string[] = [];
    _values: string[] = [];

    constructor() {}

    add(key: string, value: string) {
        this[key] = value;
        this._keys.push(key);
        this._values.push(value);
    }

    get(key: string) {
        return this[key];
    }

    keyExists(key: string): boolean {
        if (typeof this[key] === 'undefined') {
            return false;
        }

        return true;
    }
}

@Injectable()
export class ImageCacheService {
    localImageCacheDictionary: Dictionary = new Dictionary();

    constructor() {}

    getImageBase64String(mapCodeName: string) {
        if (this.localImageCacheDictionary.get(mapCodeName) !== undefined) {
            return this.localImageCacheDictionary.get(mapCodeName);
        }
    }

    cacheImage(mapCodeName: string, mapBase64String: string) {
        if (this.localImageCacheDictionary.get(mapCodeName) === undefined) {
            this.localImageCacheDictionary.add(mapCodeName, mapBase64String);
        }
    }

    imageExists(mapCodeName: string): boolean {
        if (this.localImageCacheDictionary.keyExists(mapCodeName)) {
            return true;
        }

        return false;
    }
}
