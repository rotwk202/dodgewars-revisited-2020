import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { ApiFailedReport } from '../_models/FailedReport';
//import { Headers, Http, RequestOptions, Response, ResponseContentType } from '@angular/http';

@Injectable()
export class FailedReportService {
    constructor(private http: Http) {}

    getAll(): Observable<ApiFailedReport[]> {
        const url = `/api/failedreport`;

        return this.http
            .get(url)
            .map((response: Response) => response.json())
            .catch((error) => {
                return error;
            });
    }

    // getLastGames(gamesCount: number): Observable<IWarriorMatchDetail[]> {
    //     const url = `/api/rating/recent-games?gamesCount=${gamesCount}`;

    //     return this.http
    //         .get(url)
    //         .map((response: Response) => response.json())
    //         .catch((error) => {
    //             return error;
    //         });
    // }

}
