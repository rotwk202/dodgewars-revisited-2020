﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AlertService, UserService } from '../_services/index';
import { slide } from '../_animations/index';
import { CircleComponent } from '../_spinners/index';
import { fadeInAnimation } from '../_animations/fade-in.animation';

declare var jquery: any;
declare var $: any;

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html',
    styleUrls: ['register.component.css'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' },
})
export class RegisterComponent implements OnInit {
    registerUserViewModel: any = {};
    rForm: FormGroup;
    email: string = '';
    initialNickname: string = '';
    initialDisplayName: string = '';
    password: string = '';
    public loading = false;
    private alertMessage: string = '';
    private isFailedAlert: boolean = false;
    private isAlertVisible: boolean = false;

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private fb: FormBuilder
    ) {
        this.rForm = fb.group({
            email: [null, Validators.compose([Validators.required])],
            initialNickname: [null, Validators.compose([Validators.required, Validators.maxLength(10)])],
            initialDisplayName: [null, Validators.required],
            password: [null, Validators.compose([Validators.required, Validators.minLength(7)])],
            'terms-checkbox': [false, Validators.pattern('true')],
        });
    }

    ngOnInit(): void {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    }

    hideAlert() {
        this.isAlertVisible = false;
    }

    showInfoAlert(eventType: string, message: string): void {
        if (eventType == 'success') {
            this.isAlertVisible = true;
            this.alertMessage = message;
            this.isFailedAlert = false;
        } else if (eventType == 'error') {
            this.isAlertVisible = true;
            this.alertMessage = message;
            this.isFailedAlert = true;
        }
    }

    register() {
        this.loading = true;
        this.userService.register(this.registerUserViewModel).subscribe(
            (data) => {
                this.loading = false;
                this.showInfoAlert('success', data.message);
            },
            (error) => {
                this.loading = false;
                this.showInfoAlert('error', error);
            }
        );
    }
}
