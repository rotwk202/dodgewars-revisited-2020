import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { customHttpProvider } from './_helpers/index';
import { AlertComponent } from './_directives/index';
import { CircleComponent, BulletsComponent, CircleSidenavComponent } from './_spinners/index';
import { AuthGuard } from './_guards/index';
import { AlertService, UserService, RateDataService, ImageService } from './_services/index';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { NavbarComponent } from './navbar/index';
import { LadderOneComponent } from './ladderone/index';
import { MatchesHistoryComponent } from './matcheshistory/index';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RecentgamesComponent } from './recentgames/recentgames.component';
import { TeamladderComponent } from './teamladder/teamladder.component';
import { NewsComponent } from './news/news.component';
import { FooterComponent } from './footer/footer.component';
import { RulesComponent } from './rules/rules.component';
import { GetstartedComponent } from './getstarted/getstarted.component';
import { ContactComponent } from './contact/contact.component';
import { MonthlyladderoneComponent } from './monthlyladderone/monthlyladderone.component';
import { MonthlyteamladderComponent } from './monthlyteamladder/monthlyteamladder.component';
import { ImageCacheService } from './_services/imageCachce.service';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { PlayerHistoryComponent } from './player-history/player-history.component';
import { PlayerHistoryService } from './_services/player-history.service';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';

import { Ng2CompleterModule } from 'ng2-completer';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { AdminService } from './_services/admin.service';
import { AuthAdminGuard } from './_guards/authAdmin.guard';
import { ErrorPageComponent } from './error-page/error-page.component';

import 'angular2-navigate-with-data';
import { PlayerNamesComponent } from './player-names/player-names.component';
import { FailedReportsComponent } from './failed-reports/failed-reports.component';
import { FailedReportService } from './_services/failed-report.service';
import { StatisticsComponent } from './statistics/statistics.component';
import { StatisticsService } from './_services/statistics.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ActivityStatsComponent } from './statistics/activity-stats/activity-stats.component';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { ArmiesStatsComponent } from './statistics/armies-stats/armies-stats.component';
import { PlayerCardComponent } from './statistics/player-card/player-card.component';
import { PlayerPanelComponent } from './playerpanel/playerpanel.component';
import { ProgressBarModule } from 'angular-progress-bar';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        routing,
        BrowserAnimationsModule,
        Ng2CompleterModule,
        AngularMultiSelectModule,
        NguiAutoCompleteModule,
        NgxChartsModule,
        AngularDateTimePickerModule,
        MyDatePickerModule,
        ProgressBarModule,
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        NavbarComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        LadderOneComponent,
        CircleComponent,
        BulletsComponent,
        CircleSidenavComponent,
        MatchesHistoryComponent,
        RecentgamesComponent,
        TeamladderComponent,
        NewsComponent,
        FooterComponent,
        RulesComponent,
        GetstartedComponent,
        ContactComponent,
        MonthlyladderoneComponent,
        MonthlyteamladderComponent,
        PasswordResetComponent,
        PlayerHistoryComponent,
        PlayerNamesComponent,
        AdminPanelComponent,
        ErrorPageComponent,
        FailedReportsComponent,
        StatisticsComponent,
        ActivityStatsComponent,
        ArmiesStatsComponent,
        PlayerCardComponent,
        PlayerPanelComponent,
    ],
    exports: [],
    providers: [
        customHttpProvider,
        AuthGuard,
        AuthAdminGuard,
        AlertService,
        UserService,
        RateDataService,
        ImageService,
        ImageCacheService,
        PlayerHistoryService,
        AdminService,
        FailedReportService,
        StatisticsService,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
