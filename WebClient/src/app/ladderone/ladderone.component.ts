﻿import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { RateDataService } from '../_services/index';
import { IWarrior } from '../_models/index';
import { slide } from '../_animations/index';
import { CircleComponent } from '../_spinners/index';
import { Router } from '@angular/router';
import { fadeInAnimation } from '../_animations/fade-in.animation';

@Component({
    moduleId: module.id,
    templateUrl: 'ladderone.component.html',
    styleUrls: ['./ladderone.component.css'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' },
})
export class LadderOneComponent implements OnInit {
    warriors: IWarrior[];
    public loading = false;

    constructor(private http: Http, private rateDataService: RateDataService, private router: Router) {}

    ngOnInit(): void {
        this.getLadderOneData();
    }

    showWarriorHistory(nick: string) {
        this.router.navigate(['/matcheshistory', nick]);
    }

    getLadderOneData() {
        this.loading = true;
        this.rateDataService.getLadderOneData().subscribe(
            (warriors) => {
                this.loading = false;
                this.warriors = warriors;
            },
            (error) => {
                this.loading = false;
            }
        );
    }
}
