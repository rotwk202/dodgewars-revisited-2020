﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { slide } from '../_animations/index';

@Component({
    moduleId: module.id,
    templateUrl: 'logout.component.html',
    animations: [slide],
    host: { '[@slide]': '' },
})
export class LogoutComponent implements OnInit {
    constructor(private router: Router) {}

    ngOnInit() {}

    removeToken() {
        localStorage.removeItem('token');
        this.router.navigate(['/']);
    }
}
