import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from '../_animations/fade-in.animation';

@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.css'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' },
})
export class NewsComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
}
