﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'circle-sidenav-spinner',
    templateUrl: 'circle-sidenav.component.html',
    styleUrls: ['circle-sidenav.css'],
})
export class CircleSidenavComponent {}
