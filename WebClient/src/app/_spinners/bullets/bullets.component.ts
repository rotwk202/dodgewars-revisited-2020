﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'bullets-spinner',
    templateUrl: 'bullets.component.html',
    styleUrls: ['bullets.css'],
})
export class BulletsComponent {}
