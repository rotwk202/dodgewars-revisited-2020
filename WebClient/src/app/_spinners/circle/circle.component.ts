﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'circle-spinner',
    templateUrl: 'circle.component.html',
    styleUrls: ['circle.css'],
})
export class CircleComponent {}
