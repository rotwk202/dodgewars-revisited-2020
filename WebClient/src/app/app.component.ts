import { Component, ViewChild, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.css'],
})
export class AppComponent implements OnInit {
    @ViewChild('sidenavbar') sidenavElement;
    @ViewChild('toggleButton') toggleButtonElement;
    showSidenav: boolean;

    constructor() {}
    location: Location;
    ngOnInit() {
        // if (location.protocol === 'http:') {
        //     window.location.href = location.href.replace('http', 'https');
        // }
    }

    //hide sidenav
    onNotify(message: string): void {
        this.showSidenav = false;
    }

    //show sidenav
    toggle() {
        this.showSidenav = true;
    }
}
