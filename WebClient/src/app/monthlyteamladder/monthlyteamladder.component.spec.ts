import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyteamladderComponent } from './monthlyteamladder.component';

describe('MonthlyteamladderComponent', () => {
    let component: MonthlyteamladderComponent;
    let fixture: ComponentFixture<MonthlyteamladderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MonthlyteamladderComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MonthlyteamladderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
