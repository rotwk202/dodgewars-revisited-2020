import { Component, OnInit } from '@angular/core';
import { IWarriorMonthly } from '../_models';
import { Http, Headers, Response } from '@angular/http';
import { RateDataService } from '../_services/index';
import { slide } from '../_animations/index';
import { CircleComponent } from '../_spinners/index';
import { Router } from '@angular/router';
import { fadeInAnimation } from '../_animations/fade-in.animation';
import * as moment from 'moment';

@Component({
    selector: 'app-monthlyteamladder',
    templateUrl: './monthlyteamladder.component.html',
    styleUrls: ['./monthlyteamladder.component.css'],
})
export class MonthlyteamladderComponent implements OnInit {
    warriors: IWarriorMonthly[];
    public loading = false;
    public displayArray = [];
    public selectedDateString: string;
    public selectedDateDisplay: string;

    constructor(private http: Http, private rateDataService: RateDataService, private router: Router) {}

    ngOnInit(): void {
        this.getMonthlyTourneys();
        this.selectedDateString = this.displayArray[0].DateString;
        this.selectedDateDisplay = this.displayArray.find((x) => x.DateString === this.selectedDateString).DateDisplay;
        this.getMonthlyTeamLadderData(this.selectedDateString);
    }

    dateRange(startDate, endDate): Array<any> {
        var start = startDate.split('-');
        var end = endDate.split('-');
        var startYear = parseInt(start[0]);
        var endYear = parseInt(end[0]);
        var dates = [];

        for (var i = startYear; i <= endYear; i++) {
            var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
            var startMon = i === startYear ? parseInt(start[1]) - 1 : 0;
            for (var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j + 1) {
                var month = j + 1;
                var displayMonth = month < 10 ? '0' + month : month;
                dates.push([i, displayMonth, '01'].join('-'));
            }
        }
        return dates;
    }

    getMonthlyTourneys() {
        const monthNames = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];

        var dateStrings = this.dateRange('2018-01-01', moment().add(1, 'y').toDate().toISOString());

        for (var i = 0; i < dateStrings.length; i++) {
            var date = new Date(dateStrings[i]);
            var displayString = monthNames[date.getMonth()] + ' ' + date.getFullYear();
            var element = {
                DateString: dateStrings[i],
                DateDisplay: displayString,
            };
            this.displayArray.push(element);
        }

        this.displayArray.sort(function (a, b) {
            return new Date(b.DateString).getTime() - new Date(a.DateString).getTime();
        });
    }

    getMonthlyTeamLadderData(dateString: string) {
        this.selectedDateString = dateString;
        this.selectedDateDisplay = this.displayArray.find((x) => x.DateString === this.selectedDateString).DateDisplay;
        this.loading = true;
        this.rateDataService.getMonthlyTeamLadderData(dateString).subscribe(
            (warriors) => {
                this.loading = false;
                this.warriors = warriors;
            },
            (error) => {
                this.loading = false;
            }
        );
    }
}
