import { Component, OnInit } from '@angular/core';
import { ApiFailedReport } from '../_models/FailedReport';
import { FailedReportService } from '../_services/failed-report.service';

@Component({
    selector: 'app-failed-reports',
    templateUrl: './failed-reports.component.html',
    styleUrls: ['./failed-reports.component.css'],
})
export class FailedReportsComponent implements OnInit {
    failedReports: ApiFailedReport[];
    loading: boolean;

    constructor(private failedReportService: FailedReportService) {}

    ngOnInit() {
        this.getAll();
    }

    getAll() {
        this.loading = true;
        this.failedReportService.getAll().subscribe(
            (failedReports) => {
                this.loading = false;
                this.failedReports = failedReports;
            },
            (error) => {
                this.loading = false;
            }
        );
    }

}
