﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../_services/index';
import { fadeInAnimation } from '../_animations/fade-in.animation';
import { NameDisplayName } from '../_models/nameDisplayName';

@Component({
    moduleId: module.id,
    templateUrl: 'playerpanel.component.html',
    styleUrls: ['./playerpanel.component.css'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' },
})
export class PlayerPanelComponent implements OnInit {
    rForm: FormGroup;
    playerNameForm: FormGroup;
    nickname: string = '';
    nicknameDisplay: string = '';
    playerCodeName: string = '';
    playerDisplayName: string = '';
    public loading = false;
    private alertMessage: string = '';
    private isFailedAlert: boolean = false;
    private isAlertVisible: boolean = false;
    // selectedPlayerName: string;
    // selectedPlayerDisplayName: string;
    playerNameModels: NameDisplayName[];
    playerNames: string[];
    //playerDisplayNames: string[];

    constructor(private http: Http, private userService: UserService, private router: Router, fb: FormBuilder) {
        this.rForm = fb.group({
            nickname: [null, Validators.compose([Validators.required, Validators.maxLength(10)])],
            nicknameDisplay: [null, Validators.compose([Validators.required])],
        });
        this.playerNameForm = fb.group({
            playerName: [null, null],
            playerDisplayName: [null, null],
        });
    }

    ngOnInit(): void {
        const login = localStorage.getItem('userEmail');
        console.log('login: ', login);
        this.userService.getNameDisplayNames(login).subscribe(
            (data) => {
                //this.loading = false;
                //let res = JSON.parse(data);
                //this.showInfoAlert('success', data.message);

                console.log('names: ', data);
                this.playerNameModels = data;

                this.playerNames = this.playerNameModels.map((x) => x.inReplayName);
                this.playerCodeName = this.playerNames[0];
                this.playerDisplayName = this.playerNameModels.find(
                    (x) => x.inReplayName === this.playerCodeName
                ).displayName;
                console.log('playerNames: ', this.playerNames);
            },
            (error) => {
                //this.loading = false;
                this.showInfoAlert('error', error);
            }
        );
    }

    setSelectedPlayerName(playerName: string) {
        this.playerCodeName = playerName;
        this.playerDisplayName = this.playerNameModels.find((x) => x.inReplayName === playerName).displayName;
    }

    showInfoAlert(eventType: string, message: string): void {
        if (eventType == 'success') {
            this.isAlertVisible = true;
            this.alertMessage = message;
            this.isFailedAlert = false;
        } else if (eventType == 'error') {
            this.isAlertVisible = true;
            this.alertMessage = message;
            this.isFailedAlert = true;
        }
    }

    hideAlert() {
        this.isAlertVisible = false;
    }

    createNickname() {
        this.loading = true;
        return this.userService.createNickname(this.nickname, this.nicknameDisplay).subscribe(
            (data) => {
                this.loading = false;
                //let res = JSON.parse(data);
                this.showInfoAlert('success', data.message);
            },
            (error) => {
                this.loading = false;
                this.showInfoAlert('error', error);
            }
        );
    }

    updatePlayerName() {
        this.userService.updatePlayerName(this.playerCodeName, this.playerDisplayName).subscribe((result) => {
            console.log("success result: " , result);
            this.showInfoAlert('success', result.message);
        },
        (error) => {
            console.log('error happened: ', error);
            this.showInfoAlert('error', error);
        });
    }
}
