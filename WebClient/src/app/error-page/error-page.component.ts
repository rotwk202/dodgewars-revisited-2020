import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-error-page',
    templateUrl: './error-page.component.html',
    styleUrls: ['./error-page.component.css'],
})
export class ErrorPageComponent implements OnInit {
    errorMessage: string;

    constructor(private router: Router) {
      this.errorMessage = this.router.getNavigatedData();
        if (!this.errorMessage) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
    }
}
