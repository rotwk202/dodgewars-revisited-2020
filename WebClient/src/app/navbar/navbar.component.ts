﻿import { Component, OnInit } from '@angular/core';

declare var jquery: any;
declare var $: any;

@Component({
    selector: 'navbar',
    moduleId: module.id,
    templateUrl: 'navbar.component.html',
})
export class NavbarComponent implements OnInit {
    private token: string = null;
    private isAdminUser: boolean = false;

    constructor() {}

    ngOnInit() {}

    logout() {
        if (this.token && this.token.length) {
            localStorage.removeItem('currentToken');
            localStorage.removeItem('userEmail');
        }
    }

    isLoggedIn(): boolean {
        this.token = localStorage.getItem('currentToken');
        if (this.token && this.token.length) {
            return true;
        } else {
            return false;
        }
    }
}
