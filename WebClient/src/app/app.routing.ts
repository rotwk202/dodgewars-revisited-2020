﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { LadderOneComponent } from './ladderone/index';
import { MatchesHistoryComponent } from './matcheshistory/index';
import { AuthGuard } from './_guards/index';
import { RecentgamesComponent } from './recentgames/recentgames.component';
import { TeamladderComponent } from './teamladder/teamladder.component';
import { NewsComponent } from './news/news.component';
import { RulesComponent } from './rules/rules.component';
import { GetstartedComponent } from './getstarted/getstarted.component';
import { MonthlyladderoneComponent } from './monthlyladderone/monthlyladderone.component';
import { MonthlyteamladderComponent } from './monthlyteamladder/monthlyteamladder.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { PlayerHistoryComponent } from './player-history/player-history.component';
import { PlayerNamesComponent } from './player-names/player-names.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { AuthAdminGuard } from './_guards/authAdmin.guard';
import { ErrorPageComponent } from './error-page/error-page.component';
import { FailedReportsComponent } from './failed-reports/failed-reports.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { PlayerCardComponent } from './statistics/player-card/player-card.component';
import { PlayerPanelComponent } from './playerpanel/playerpanel.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'log-in', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'individual-ladder', component: LadderOneComponent },
    { path: 'team-ladder', component: TeamladderComponent },
    { path: 'recent-games', component: RecentgamesComponent },
    { path: 'player-panel', component: PlayerPanelComponent, canActivate: [AuthGuard] },
    { path: 'matcheshistory/:nick', component: MatchesHistoryComponent },
    { path: 'news', component: NewsComponent },
    { path: 'rules', component: RulesComponent },
    { path: 'get-started', component: GetstartedComponent },
    { path: 'monthly-individual-ladder', component: MonthlyladderoneComponent },
    { path: 'monthly-team-ladder', component: MonthlyteamladderComponent },
    { path: 'password-reset', component: PasswordResetComponent },
    { path: 'player-history/:name', component: PlayerHistoryComponent },
    { path: 'player-names', component: PlayerNamesComponent },
    { path: 'error-page', component: ErrorPageComponent },
    { path: 'admin-panel', component: AdminPanelComponent, canActivate: [AuthAdminGuard] },
    { path: 'failed-reports', component: FailedReportsComponent },
    { path: 'statistics', component: StatisticsComponent },
    { path: 'player-card/:name', component: PlayerCardComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' },
];

export const routing = RouterModule.forRoot(appRoutes);
