﻿// import the required animation functions from the angular animations module
import { trigger, state, animate, transition, style } from '@angular/animations';

export const slide =
    // trigger name for attaching this animation to an element using the [@triggerName] syntax
    trigger('slide', [
        state('void', style({ position: 'absolute', width: '100%' })),
        state('*', style({ position: 'absolute', width: '100%' })),
        transition(':enter', [
            // before 2.1: transition('void => *', [
            style({ transform: 'translateX(100%)' }),
            animate('1s ease-in-out', style({ transform: 'translateX(0%)' })),
        ]),
        transition(':leave', [
            // before 2.1: transition('* => void', [
            style({ transform: 'translateX(0%)' }),
            animate('1s ease-in-out', style({ transform: 'translateX(-100%)' })),
        ]),
    ]);
