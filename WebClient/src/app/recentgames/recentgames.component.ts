import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { RateDataService, ImageService } from '../_services/index';
import { saveAs as importedSaveAs } from 'file-saver';
import { IWarriorMatchDetail } from '../_models/IWarriorMatchDetail';
import { fadeInAnimation } from '../_animations/fade-in.animation';

@Component({
    templateUrl: './recentgames.component.html',
    styleUrls: ['./recentgames.component.css'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' },
})
export class RecentgamesComponent implements OnInit {
    lastGamesCount: number;
    lastGames: IWarriorMatchDetail[];
    loading: boolean;
    gamesCurrentMonthCount: number;
    gamesAllTimeCount: number;
    page = 0;

    constructor(
        private http: Http,
        private rateDataService: RateDataService,
        private route: ActivatedRoute,
        private imageService: ImageService
    ) {}

    ngOnInit() {
        this.lastGamesCount = 10;
        this.getLastGames();
        this.getGamesCountData();
    }

    keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    decrementPage() {
        if (this.page === 0) {
            return;
        }
        this.page = this.page - 1;
        this.getLastGames();
    }
    incrementPage() {
        this.page = this.page + 1;
        this.getLastGames();
    }

    getLastGames() {
        this.loading = true;
        this.lastGames = [];
        console.log('going to call service: ', this.lastGamesCount, this.page);
        this.rateDataService.getLastGames(this.lastGamesCount, this.page).subscribe(
            (lastApiGames) => {
                this.loading = false;
                this.lastGames = lastApiGames;
            },
            (error) => {
                this.loading = false;
            }
        );
    }

    downloadReplay(gameIdentifier: number) {
        var fileName = 'Game_' + gameIdentifier + '_Replay.BfME2Replay';
        this.rateDataService.downloadFileNew(gameIdentifier).subscribe((blob) => {
            importedSaveAs(blob, fileName);
        });
    }

    getImageSrc(mapCodeName: string) {
        return this.imageService.getImageBase64String(mapCodeName);
    }

    getGamesCountData() {
        this.loading = true;
        this.rateDataService.getGamesCountData().subscribe(
            (apiGamesCountData) => {
                this.gamesCurrentMonthCount = apiGamesCountData.gamesCurrentMonthCount;
                this.gamesAllTimeCount = apiGamesCountData.gamesAllTimeCount;
                this.loading = false;
            },
            (error) => {
                this.loading = false;
            }
        );
    }
}
