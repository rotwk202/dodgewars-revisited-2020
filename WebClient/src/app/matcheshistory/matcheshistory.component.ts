import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { RateDataService } from '../_services/index';
import { IWarriorMatchHistory } from '../_models/index';
import { slide } from '../_animations/index';
import { saveAs as importedSaveAs } from 'file-saver';
import { IWarriorMatchDetail } from '../_models/IWarriorMatchDetail';
import { fadeInAnimation } from '../_animations/fade-in.animation';
import { IWarriorDetail } from '../_models/IWarriorDetail';

@Component({
    templateUrl: './matcheshistory.component.html',
    styleUrls: ['./matcheshistory.component.css'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' },
})
export class MatchesHistoryComponent implements OnInit {
    //public data: IWarriorMatchHistory[];
    public data: IWarriorMatchDetail[];
    selected = 15;
    pageNumber = 1;
    retrievedNick: string;
    offset: number;

    constructor(private http: Http, private rateDataService: RateDataService, private route: ActivatedRoute) {}

    ngOnInit(): void {
        this.retrievedNick = this.route.snapshot.paramMap.get('nick');
        this.updateContent();
    }

    setDisplayNumber(selectedNumber: number) {
        this.selected = selectedNumber;
        this.updateContent();
    }

    isWinningTeam(gamePlayers: IWarriorDetail[]): boolean {
        var isWinningTeam = gamePlayers.filter((x) => x.isLooser === false).length > 0;
        return isWinningTeam;
    }

    isLoosingTeam(gamePlayers: IWarriorDetail[]): boolean {
        var isLoosingTeam = gamePlayers.filter((x) => x.isLooser === true).length > 0;
        return isLoosingTeam;
    }

    updatePageNumber(increment: number) {
        if (this.pageNumber == 1 && increment == -1) {
            return;
        }

        this.pageNumber = this.pageNumber + increment;
        this.updateContent();
    }

    pointsGained(pointsCount: number) {
        return Math.abs(pointsCount);
    }

    updateContent() {
        //this.loading = true;
        this.data = [];
        this.offset = this.getPaginationOffset(this.pageNumber, this.selected);
        this.rateDataService.getWarriorHistory(this.retrievedNick, this.offset, this.selected).subscribe(
            (historyData) => {
                //this.loading = false;
                this.data = historyData;
            },
            (error) => {
                //this.loading = false;
            }
        );
    }

    getPaginationOffset(pageNumber: number, itemsToDisplay: number): number {
        if (pageNumber == 1) {
            return 1;
        } else {
            return (pageNumber - 1) * itemsToDisplay;
        }
    }

    downloadReplay(gameIdentifier: number) {
        var fileName = 'Game_' + gameIdentifier + '_Replay.BfME2Replay';
        this.rateDataService.downloadFile(gameIdentifier).subscribe((blob) => {
            importedSaveAs(blob, fileName);
        });
    }
}
