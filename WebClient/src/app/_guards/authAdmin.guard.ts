import { Injectable } from '@angular/core';
import {
    Router,
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    ChildActivationEnd,
    CanActivateChild,
    NavigationExtras,
} from '@angular/router';
import { UserService } from '../_services';
import { Observable, Subject } from 'rxjs';
import { of } from 'rxjs/observable/of';

@Injectable()
export class AuthAdminGuard implements CanActivate {
    isAdmin: any;
    watchMe = new Subject<boolean>();

    constructor(private router: Router, private userService: UserService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const login = localStorage.getItem('userEmail');
        const token = localStorage.getItem('currentToken');
        if (!login || !token) {
            this.router.navigate(['/log-in'], { queryParams: { returnUrl: state.url } });
            return of(false);
        }

        return this.userService.isAdminUser(token).then((authenticated: boolean) => {
            if (authenticated) {
                return true;
            } else {
                this.router.navigate(['/log-in']);
            }
        })
        .catch(err => {
            this.router.navigateByData({
                url: ["/error-page"],
                data: err,
            });
            
            return false;
        });
    }
}
