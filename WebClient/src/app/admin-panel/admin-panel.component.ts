import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { ARModel } from '../_models/index';
import { AdminService } from '../_services/admin.service';
import { Http, Response } from '@angular/http';

@Component({
    selector: 'app-admin-panel',
    templateUrl: './admin-panel.component.html',
    styleUrls: ['./admin-panel.component.css'],
})
export class AdminPanelComponent implements OnInit {
    arFormGroup: FormGroup;
    arReplayFile: File;
    loserPlayerName = '';
    nickSearchString: string;
    isFailedAlertVisible = false;
    isSuccessAlertVisible = false;
    alertMessage = '';
    constructor(private fb: FormBuilder, private _sanitizer: DomSanitizer, private adminService: AdminService) {
        this.arFormGroup = fb.group({
            arReplayFile: [null, Validators.compose([Validators.required])],
            loserPlayerName: [null, Validators.compose([Validators.required])],
        });
    }
    gameIdentifierToRemove = '';

    list: string[] = [];
    gamePrimaryKeyIdsList: string[] = [];

    observableSource(keyword: any) {
        let filteredList = this.list.filter((el) => el.indexOf(keyword) !== -1);
        return Observable.of(filteredList);
    }

    gamePrimaryKeyIdsSource(keyword: any) {
        console.log('click action', this.gamePrimaryKeyIdsList);
        let filteredList = this.gamePrimaryKeyIdsList.filter((el) => el.indexOf(keyword) !== -1);
        return Observable.of(filteredList);
    }

    ngOnInit() {
        this.getAllPlayerNames();
        this.getGamePrimaryKeyIds();
    }

    getAllPlayerNames() {
        this.adminService.getAllPlayerNames('').subscribe((result) => {
            this.list = result.map((x) => x.nickname);
        });
    }

    getGamePrimaryKeyIds() {
        this.adminService.getGamePrimaryKeyIds().subscribe((result) => {
            console.log('ids: ', result);
            this.gamePrimaryKeyIdsList = result.map((x) => x.toString());
        });
    }

    removeGame() {
        console.log('converted string to number: ', +this.gameIdentifierToRemove);
        this.adminService.deleteGame(+this.gameIdentifierToRemove).subscribe(
            (result: Response) => {
                console.log('delete result: ', result);
                this.showInfoAlert('success', result.text());
            },
            (error) => {
                console.log('error I got: ', error);
                this.showInfoAlert('error', error);
            }
        );
    }

    hideAlert() {
        this.isFailedAlertVisible = false;
        this.isSuccessAlertVisible = false;
    }

    onArSubmit() {
        const arModel = new ARModel();
        arModel.replayFile = this.arReplayFile;
        arModel.loserPlayerName = this.arFormGroup.controls['loserPlayerName'].value;
        arModel.adminToken = localStorage.getItem('currentToken');

        this.adminService.processAr(arModel).subscribe(
            (result: Response) => {
                this.showInfoAlert('success', result.text());
            },
            (error) => {
                this.arReplayFile = null;
                this.arFormGroup.controls['arReplayFile'].setValue(null);
                this.showInfoAlert('error', error);
            }
        );
    }

    showInfoAlert(eventType: string, message: string): void {
        if (eventType === 'success') {
            this.isFailedAlertVisible = false;
            this.isSuccessAlertVisible = true;
            this.alertMessage = message;
        } else if (eventType === 'error') {
            this.isSuccessAlertVisible = false;
            this.isFailedAlertVisible = true;
            this.alertMessage = message;
        }
    }

    // autocompleListFormatter = (data: any): SafeHtml => {
    //     let html = `<span>${data.name}</span>`;
    //     return this._sanitizer.bypassSecurityTrustHtml(html);
    // };

    fileChange(event) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            this.arReplayFile = file;
        }
    }
}
