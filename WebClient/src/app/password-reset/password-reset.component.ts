import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../_services/user.service';
import { MustMatch } from '../_helpers/must-match.helper';

@Component({
    selector: 'app-password-reset',
    templateUrl: './password-reset.component.html',
    styleUrls: ['./password-reset.component.css'],
})
export class PasswordResetComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
    returnUrl: string;
    public loading = false;
    private isFailedAlert = false;
    private isAlertVisible = false;
    private alertMessage = '';

    constructor(private formBuilder: FormBuilder, private userService: UserService) {}

    ngOnInit() {
        this.registerForm = this.formBuilder.group(
            {
                userName: ['', [Validators.required]],
                password: ['', [Validators.required]],
                confirmPassword: ['', Validators.required],
            },
            {
                validator: MustMatch('password', 'confirmPassword'),
            }
        );
    }

    get f() {
        return this.registerForm.controls;
    }

    hideFailedAlert() {
        this.isAlertVisible = false;
    }

    showInfoAlert(eventType: string, message: string): void {
        if (eventType === 'success') {
            this.isAlertVisible = true;
            this.isFailedAlert = false;
            this.alertMessage = message;
        } else if (eventType === 'error') {
            this.isAlertVisible = true;
            this.isFailedAlert = true;
            this.alertMessage = message;
        }
    }

    resetPassword() {
        this.submitted = true;
        this.loading = true;

        if (!this.registerForm.valid) {
            this.loading = false;
            return;
        }

        this.userService
            .resetPassword(this.registerForm.controls['userName'].value, this.registerForm.controls['password'].value)
            .subscribe(
                (data) => {
                    this.loading = false;
                    this.showInfoAlert('success', data.text());
                },
                (error) => {
                    this.loading = false;
                    this.showInfoAlert('error', error);
                }
            );
    }
}
