﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AlertService, UserService } from '../_services/index';
import { fadeInAnimation } from '../_animations/fade-in.animation';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' },
})
export class LoginComponent implements OnInit {
    loginUserViewModel: any = {};
    rForm: FormGroup;
    email: string = '';
    password: string = '';
    returnUrl: string;
    public loading = false;
    private isFailedAlertVisible: boolean = false;
    private alertMessage: string = '';

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private fb: FormBuilder
    ) {
        this.rForm = fb.group({
            email: [null, Validators.compose([Validators.required])],
            password: [null, Validators.compose([Validators.required, Validators.minLength(7)])],
        });
    }

    ngOnInit() {
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    hideFailedAlert() {
        this.isFailedAlertVisible = false;
    }

    showInfoAlert(eventType: string, message: string): void {
        if (eventType == 'success') {
            this.router.navigate([this.returnUrl]);
        } else if (eventType == 'error') {
            this.isFailedAlertVisible = true;
            this.alertMessage = message;
        }
    }

    login() {
        this.loading = true;
        this.userService.getToken(this.loginUserViewModel).subscribe(
            (data) => {
                this.loading = false;
                let obj: any = data;
                var tokenRetrieved = obj._body;
                localStorage.setItem('currentToken', tokenRetrieved);
                localStorage.setItem('userEmail', this.loginUserViewModel.email);
                this.showInfoAlert('success', 'You are successfully logged in.');
            },
            (error) => {
                this.loading = false;
                //let res = JSON.parse(error);
                this.showInfoAlert('error', 'Something went wrong!');
            }
        );
    }
}
