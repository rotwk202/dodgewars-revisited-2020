import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyladderoneComponent } from './monthlyladderone.component';

describe('MonthlyladderoneComponent', () => {
    let component: MonthlyladderoneComponent;
    let fixture: ComponentFixture<MonthlyladderoneComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MonthlyladderoneComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MonthlyladderoneComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
