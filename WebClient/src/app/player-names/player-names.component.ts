import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services';

@Component({
    selector: 'app-player-names',
    templateUrl: './player-names.component.html',
    styleUrls: ['./player-names.component.css'],
})
export class PlayerNamesComponent implements OnInit {
    allPlayerNames: string[] = [];

    constructor(private userService: UserService) {}

    ngOnInit() {
        this.userService.getAllNicknames().subscribe((result) => {
            this.allPlayerNames = result.map((x) => x.nickname);
        });
    }
}
