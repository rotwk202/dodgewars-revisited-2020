import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { RateDataService } from '../_services/index';
import { IWarrior } from '../_models/index';
import { CircleComponent } from '../_spinners/index';
import { Router } from '@angular/router';
import { ITeamWarrior } from '../_models/ITeamWarrior';
import { fadeInAnimation } from '../_animations/fade-in.animation';

@Component({
    selector: 'app-teamladder',
    templateUrl: './teamladder.component.html',
    styleUrls: ['./teamladder.component.css'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' },
})
export class TeamladderComponent implements OnInit {
    teamWarriors: ITeamWarrior[];
    public loading = false;

    constructor(private http: Http, private rateDataService: RateDataService, private router: Router) {}

    ngOnInit(): void {
        this.getTeamLadderData();
    }

    showWarriorHistory(nick: string) {
        this.router.navigate(['/matcheshistory', nick]);
    }

    getTeamLadderData() {
        this.loading = true;
        this.rateDataService.getTeamLadderData().subscribe(
            (warriors) => {
                this.loading = false;
                this.teamWarriors = warriors;
            },
            (error) => {
                this.loading = false;
            }
        );
    }
}
