import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamladderComponent } from './teamladder.component';

describe('TeamladderComponent', () => {
    let component: TeamladderComponent;
    let fixture: ComponentFixture<TeamladderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TeamladderComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TeamladderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
