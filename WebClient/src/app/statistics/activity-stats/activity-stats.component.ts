import { Component, OnInit } from '@angular/core';
import { StatisticsService } from '../../_services/statistics.service';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import * as moment from 'moment';
import { Moment } from 'moment';

@Component({
    selector: 'app-activity-stats',
    templateUrl: './activity-stats.component.html',
    styleUrls: ['./activity-stats.component.scss'],
})
export class ActivityStatsComponent implements OnInit {
    multi: any[];

    view: any[] = [1200, 400];
    legend: boolean = false;
    showLabels: boolean = true;
    animations: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showYAxisLabel: boolean = true;
    showXAxisLabel: boolean = true;
    xAxisLabel: string = 'Date';
    yAxisLabel: string = 'Games count';
    timeline: boolean = true;
    single: any[] = [];

    date: Date = new Date();
    momentDate: Moment;
    loading = false;
    isValidDateProp = true;

    colorScheme = {
        domain: ['#5AA454'],
    };

    public myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'dd.mm.yyyy',
        width: '100%',
    };

    public model: any = { date: { year: 2020, month: 8, day: 1 } };
    monthName: string;

    constructor(private statisticsService: StatisticsService) {}

    onDateChanged(event: IMyDateModel) {
        this.date.setFullYear(event.date.year, event.date.month - 1, event.date.day);
    }

    getMonthActivityData() {
        this.loading = true;

        this.statisticsService.getMonthActivityStats(this.date.toISOString()).subscribe(
            (monthActivityData) => {
                this.multi = [{ name: 'seria', series: [] }];
                monthActivityData.forEach((stat) => {
                    this.multi[0].series.push({ name: stat.dateString, value: stat.gamesCount });
                });

                this.isValidDate(this.date.toISOString());
                this.momentDate = moment(this.date.toISOString());
                this.loading = false;
            },
            (error) => {
                this.loading = false;
            }
        );
    }

    ngOnInit() {
        this.momentDate = moment(this.date.toISOString());
        this.getMonthActivityData();
    }

    isValidDate(dateString: string) {
        this.isValidDateProp = true;

        if (dateString.includes('-000001')) {
            this.isValidDateProp = false;
            return;
        }

        const date = new Date(dateString);

        if (date instanceof Date && !isNaN(date.getMilliseconds())) {
            this.isValidDateProp = true;
            return;
        }
    }
}
