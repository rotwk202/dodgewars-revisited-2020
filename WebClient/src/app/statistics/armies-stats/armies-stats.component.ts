import { Component, OnInit } from '@angular/core';
import { StatisticsService } from '../../_services/statistics.service';
import { ArmyColorCodes } from '../../_models/ArmyColorCodes';

@Component({
    selector: 'app-armies-stats',
    templateUrl: './armies-stats.component.html',
    styleUrls: ['./armies-stats.component.css'],
})
export class ArmiesStatsComponent implements OnInit {
    single: any[] = [];
    view: any[] = [700, 400];
    loading = false;

    ngOnInit() {
        this.loading = true;
        this.statisticsService.getOverall().subscribe(
            (overallStats) => {
                this.single = [];
                overallStats.forEach((stat) => {
                    this.single.push({ name: stat.army, value: stat.winPercentage });
                });
                this.loading = false;
            },
            (error) => {
                this.loading = false;
            }
        );
    }

    gradient: boolean = true;
    showLegend: boolean = true;
    showLabels: boolean = true;
    isDoughnut: boolean = false;
    legendPosition: string = 'below';
    showYAxis: boolean = true;

    colorScheme = {
        domain: [
            ArmyColorCodes.Men,
            ArmyColorCodes.Elves,
            ArmyColorCodes.Dwarves,
            ArmyColorCodes.Mordor,
            ArmyColorCodes.Isengard,
            ArmyColorCodes.Goblins,
            ArmyColorCodes.Angmar,
        ],
    };

    constructor(private statisticsService: StatisticsService) {}
}
