import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { StatisticsService } from '../../_services/statistics.service';
import { ActivatedRoute } from '@angular/router';
import { PlayerCard } from '../../_models/PlayerCard';
import { MedalStage, PlayerMedalDetails } from '../../_models/PlayerMedalDetails';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as moment from 'moment';

@Component({
    selector: 'app-player-card',
    templateUrl: './player-card.component.html',
    styleUrls: ['./player-card.component.css'],
})
export class PlayerCardComponent implements OnInit, AfterViewInit {
    playerName: string;
    playerCard: PlayerCard;
    errorMessage: string;
    totalWinsCountMedalImage: string;
    seasons = [];
    public selectedDateString: string;
    public selectedDateDisplay: string;

    constructor(
        private statisticsService: StatisticsService,
        private activatedRoute: ActivatedRoute,
        private cdr: ChangeDetectorRef
    ) {}
    ngAfterViewInit(): void {
        this.cdr.detectChanges();
    }

    ngOnInit() {
        this.initializeSeasons();
        this.selectedDateString = this.seasons[0].DateString;
        this.selectedDateDisplay = this.seasons.find((x) => x.DateString === this.selectedDateString).DateDisplay;
        this.playerName = this.activatedRoute.snapshot.paramMap.get('name');
        this.getPlayerCard(this.selectedDateString);
    }

    getActivityMedalImage() {
        return `/assets/medals/Activity/${this.playerCard.playerMedalDetails.activity.medalCode}.png`;
    }

    getTotalWinsMedalImage() {
        return `/assets/medals/TotalWins/${this.playerCard.playerMedalDetails.totalWins.medalCode}.png`;
    }

    getAmbitionMedalImage() {
        return `/assets/medals/Ambition/${this.playerCard.playerMedalDetails.ambition.medalCode}.png`;
    }

    dateRange(startDate, endDate) {
        var start = startDate.split('-');
        var end = endDate.split('-');
        var startYear = parseInt(start[0]);
        var endYear = parseInt(end[0]);
        var dates = [];

        for (var i = startYear; i <= endYear; i++) {
            var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
            var startMon = i === startYear ? parseInt(start[1]) - 1 : 0;
            for (var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j + 1) {
                var month = j + 1;
                var displayMonth = month < 10 ? '0' + month : month;
                dates.push([i, displayMonth, '01'].join('-'));
            }
        }
        return dates;
    }

    initializeSeasons() {
        const monthNames = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];

        var dateStrings = this.dateRange('2018-01-01', moment().add(1, 'y').toDate().toISOString());

        for (var i = 0; i < dateStrings.length; i++) {
            var date = new Date(dateStrings[i]);

            if (date.getMonth() % 3 === 0) {
                const month = date.getMonth() + 1;
                let dateString = '';
                let quarter = 0;

                if (month >= 1 && month < 4) {
                    quarter = 1;
                    dateString = `${date.getFullYear()}.${quarter}`;
                }
                if (month >= 4 && month < 7) {
                    quarter = 2;
                    dateString = `${date.getFullYear()}.${quarter}`;
                }
                if (month >= 7 && month < 10) {
                    quarter = 3;
                    dateString = `${date.getFullYear()}.${quarter}`;
                }
                if (month >= 10) {
                    quarter = 4;
                    dateString = `${date.getFullYear()}.${quarter}`;
                }

                var element = {
                    DateString: dateString,
                    DateDisplay: `${date.getFullYear()} season ${quarter}`,
                };
                this.seasons.push(element);
            }
        }

        this.seasons.sort(function (a, b) {
            return new Date(b.DateString).getTime() - new Date(a.DateString).getTime();
        });
    }

    getPlayerCard(selectedDateString: string) {
        console.log('call it: ');
        this.selectedDateString = selectedDateString;
        this.selectedDateDisplay = this.seasons.find((x) => x.DateString === this.selectedDateString).DateDisplay;
        this.statisticsService.getPlayerCard(this.playerName, selectedDateString).subscribe(
            (playerCard) => {
                this.errorMessage = null;
                console.log('playerCard: ', playerCard);
                this.playerCard = playerCard.json();
            },
            (error) => {
                this.errorMessage = error;
            }
        );
    }
}
