# dodgewars-revisited-2020

Dodgewars is a well known Rise of the Witch-King based ladder system allowing player to track their progress on games.
This is a continuation project after the original one:

List of things that were changed and improved:

* web api project moved from .NET Framework to .NET Core 3.1
* improved replay parser
* replay parsing operation is now being done on the server side, not on the parser anymore. That makes the whole ecosystem less error-prune
* added replay parser report loss button as an alternative to F6 button
* updated database schema regarding points calculation