﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AllGamesReporter
{
    public class ReportUtility
    {
        private static readonly string _apiServerEndpoint = "https://localhost:44380/api/";
        private const string _reportLossRoute = "GameReport/ReportLossRecalculation";
        private readonly Uri _apiServer;

        public ReportUtility()
        {
            _apiServer = new Uri(_apiServerEndpoint);
        }

        public async Task ReportLossNew(string filePath, string userEmail)
        {
            var client = new RestClient(_apiServer);
            var request = new RestRequest(_reportLossRoute, Method.POST);
            request.AlwaysMultipartFormData = true;
            request.AddHeader("Content-Type", "multipart/form-data");
            request.AddFile("file", filePath, "bfme2replay");

            request.AddHeader("Email", userEmail);

             IRestResponse response = client.Execute(request);

            if(response.ResponseStatus == ResponseStatus.Completed && response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return;
            }
            else
            {
                throw new Exception("Something went wrong reporting game.");
            }
        }
    }
}
