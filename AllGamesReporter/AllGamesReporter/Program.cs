﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AllGamesReporter
{
    class Program
    {
        static void Main(string[] args)
        {
            var utility = new ReportUtility();

            var allGamesDir = @"C:\Users\Dell\Desktop\temp\DW-GAMES";
            var replays = Directory.GetFiles(allGamesDir, "*.bfme2replay")
                .OrderBy(f => int.Parse(Path.GetFileNameWithoutExtension(f).Split(',')[0]))
                .ToArray();
            var allEmails = new List<string>();
            foreach (var item in replays)
            {
                allEmails.Add( item.Split(',')[1].Replace(".bfme2replay", ""));
            }

            if (allEmails.Count != replays.Count())
            {
                throw new InvalidDataException("emails and replays count were not equal");
            }

            for (int i = 0; i < replays.Count(); i++)
            {
                Debug.WriteLine($"iteration number {i}, email: {allEmails[i]}, replay file: {replays[i]}");
                utility.ReportLossNew(replays[i], allEmails[i]);
            }
            
            Console.WriteLine("All done!");
            Console.ReadKey();
        }
    }
}
