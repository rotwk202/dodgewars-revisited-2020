﻿using DW.Manager.App.Services;
using System;
using System.Configuration;
using System.IO;

namespace DW.Manager.App
{
    public class DomainService
    {
        private readonly ApiService _apiService;
        private readonly MapService _mapService;

        public DomainService()
        {
            _apiService = new ApiService();
            _mapService = new MapService();
        }

        public void UploadsMaps()
        {
            Console.Write("Enter file name containig map simple properties (including .txt extension): ");
            var fileName = Console.ReadLine();
            var rawUserMapDataFilePath = Path.Combine(ConfigurationManager.AppSettings["mapsSimplePropertiesDirectory"], fileName);

            try
            {
                var mapModels = _mapService.GetMapModels(rawUserMapDataFilePath);
                var response = _apiService.UploadMaps(mapModels);
                Console.WriteLine(response);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
