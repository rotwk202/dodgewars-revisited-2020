﻿using System;

namespace DW.Manager.App
{
    public class Program
    {
        private static DomainService _domainService;

        static void Main(string[] args)
        {
            _domainService = new DomainService();

            Console.WriteLine("Choose 2 to uploads maps.");
            Console.Write("Your choice: ");

            var userChoice = Console.ReadLine();

            try
            {
                if (userChoice == "2")
                {
                    _domainService.UploadsMaps();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            while (true) { }
        }
    }
}
