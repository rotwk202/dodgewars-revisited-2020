﻿using DW.Manager.App.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace DW.Manager.App.Services
{
    public class MapService
    {
        private const int _codeNameIndex = 0;
        private const int _displayNameIndex = 1;
        private const int _replayMapTextIdIndex = 2;
        private const char _mapPropsSeparator = ',';

        public List<ApiMap> GetMapModels(string simpleMapPropertiesFilePath)
        {
            List<ApiMap> mapModels = new List<ApiMap>();



            if (string.IsNullOrEmpty(simpleMapPropertiesFilePath))
            {
                throw new NullReferenceException("Could not find raw user map data file path.");
            }

            if (!File.Exists(simpleMapPropertiesFilePath))
            {
                throw new FileNotFoundException("The file you specified does not exist.");
            }

            var rawUserMapData = GetRawUserMapData(simpleMapPropertiesFilePath);
            var separatedMapLines = GetSeparatedMapLines(rawUserMapData);

            mapModels = SetMapSimpleProperties(separatedMapLines);
            mapModels = SetMapImages(mapModels);

            return mapModels;
        }

        private string GetRawUserMapData(string rawUserMapDataFilePath)
        {
            try
            {
                return File.ReadAllText(rawUserMapDataFilePath);
            }
            catch (Exception)
            {
                throw new ArgumentException("There were some invalid characters in simple map data file path.");
            }
        }

        private string[] GetSeparatedMapLines(string rawUserMapData)
        {
            var mapLines = rawUserMapData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            //mapLines[mapLines.Length - 1] = mapLines.Last().Replace(";", string.Empty);

            return mapLines;
        }

        private List<ApiMap> SetMapSimpleProperties(string[] separatedMapLines)
        {
            List<ApiMap> mapModels = new List<ApiMap>();
            foreach (var mapLine in separatedMapLines)
            {
                var mapProperties = mapLine.Split(_mapPropsSeparator);

                mapModels.Add(new ApiMap()
                {
                    CodeName = mapProperties[_codeNameIndex],
                    DisplayName = mapProperties[_displayNameIndex],
                    Image = null,
                    ReplayMapTextId = mapProperties[_replayMapTextIdIndex]
                });
            }

            return mapModels;
        }

        private List<ApiMap> SetMapImages(List<ApiMap> mapModels)
        {
            var mapsImagesDirectory = ConfigurationManager.AppSettings["mapsImagesDirectory"];
            var allImages = Directory.GetFiles(mapsImagesDirectory);

            foreach (var map in mapModels)
            {
                var imagePath = allImages.FirstOrDefault(x => x.Contains(map.CodeName));

                if (imagePath == null)
                {
                    throw new NullReferenceException("Unable to find this map image: " + map.CodeName);
                }

                map.Image = File.ReadAllBytes(imagePath);
            }

            return mapModels;
        }
    }
}
