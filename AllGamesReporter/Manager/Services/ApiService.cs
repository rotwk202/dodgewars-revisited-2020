﻿using DW.Manager.App.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace DW.Manager.App.Services
{
    public class ApiService
    {
        private readonly string apiServerEndpoint = ConfigurationManager.AppSettings["targetServer"];
        private readonly Uri _apiServer;

        private const string _uploadMapsRoute = "api/Admin/UploadMaps/";

        public ApiService()
        {
            _apiServer = new Uri(apiServerEndpoint);
        }
        public string UploadMaps(List<ApiMap> mapModels)
        {
            var url = new Uri(_apiServer, _uploadMapsRoute).ToString();
            var serializedJson = JsonConvert.SerializeObject(mapModels);
            var encoding = new UTF8Encoding(false);
            var data = encoding.GetBytes(serializedJson);

            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;
            using (var sr = request.GetRequestStream())
            {
                sr.Write(data, 0, data.Length);
            }
            // Get the response.
            string responseFromServer = string.Empty;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream dataStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    responseFromServer = reader.ReadToEnd();
                }
            }
            catch (WebException wex)
            {
                throw wex;
            }

            return responseFromServer;
        }

    }
}
