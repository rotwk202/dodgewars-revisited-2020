﻿using System.Collections.Generic;

namespace DWManager.Models
{
    public class ArFileGame
    {
        public List<string> Winners { get; set; }
        public List<string> Losers { get; set; }
    }
}
