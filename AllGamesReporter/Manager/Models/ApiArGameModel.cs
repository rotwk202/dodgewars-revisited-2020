﻿using System;
using System.Collections.Generic;

namespace DW.Manager.App.Models
{
    public class ApiArGameModel
    {
        public DateTime PlayedOn { get; set; }
        public List<ApiPlayer> Players { get; set; }
    }
}
