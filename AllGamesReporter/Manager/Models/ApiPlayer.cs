﻿namespace DW.Manager.App.Models
{
    public class ApiPlayer
    {
        public string Name { get; set; }

        public bool IsLooser { get; set; }
    }
}
