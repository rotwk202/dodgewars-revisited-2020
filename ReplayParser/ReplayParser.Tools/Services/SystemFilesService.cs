﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ReplayParser.Tools.Services
{
    public static class SystemFilesService
    {
        private static readonly string _appDataRoamingFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        private static readonly List<string> _possibleFileDirectories;
        private static readonly List<string> _possibleFilePaths;

        //C:\Users\Radek\AppData\Roaming\Властелин Колец, Под знаменем Короля-чародея - Мои файлы\Replays\Последний видеоролик.BfME2Replay
        private static readonly string _englishRotwKFolderPart = @"My The Lord of the Rings, The Rise of the Witch-king Files\Replays";
        private const string _germanRotwKFolderPart = @"Meine Der Herr der Ringe™, Aufstieg des Hexenkönigs™-Dateien\Replays";
        private const string _spanishRotwKFolderPart = @"Mis archivos de El Señor de los Anillos, El Resurgir del Rey Brujo\Replays";
        private const string _frenchRotwKFolderPart = @"Mes fichiers de LSDA, L'Avènement du Roi-sorcier™\Replays";
        private const string _italianRotwKFolderPart = @"File de Il Signore degli Anelli™ - L'Ascesa del Re Stregone™\Replays";
        private const string _russianRotwKFolderPart = @"Властелин Колец, Под знаменем Короля-чародея - Мои файлы\Replays";
        private const string _polishRotwKFolderPart = @"Moje pliki gry Władca Pierścieni, Król Nazguli\Replays";
        private const string _dutchRotwKFolderPart = @"Mijn The Lord of the Rings, The Rise of the Witch-king-bestanden\Replays";

        private const string _englishFileNameToTrack  = "Last Replay.BfME2Replay";
        private const string _germanFileNameToTrack = "Letzte Wiederholung.BfME2Replay";
        private const string _spanishFileNameToTrack = "Última repetición.BfME2Replay";
        private const string _frenchFileNameToTrack = "Dernière Redif..BfME2Replay";
        private const string _italianFileNameToTrack = "Ultimo replay.BfME2Replay";
        private const string _russianFileNameToTrack = "Последний видеоролик.BfME2Replay";
        private const string _polishFileNameToTrack = "Ostatnia powtórka.BfME2Replay";
        private const string _dutchFileNameToTrack = "Laatste herhaling.BfME2Replay";

        static SystemFilesService()
        {
            _possibleFilePaths = new List<string>();
            var list = new List<string>()
            {
                Path.Combine(_appDataRoamingFolder, _englishRotwKFolderPart, _englishFileNameToTrack),
                Path.Combine(_appDataRoamingFolder, _germanRotwKFolderPart, _germanFileNameToTrack),
                Path.Combine(_appDataRoamingFolder, _spanishRotwKFolderPart, _spanishFileNameToTrack),
                Path.Combine(_appDataRoamingFolder, _frenchRotwKFolderPart, _frenchFileNameToTrack),
                Path.Combine(_appDataRoamingFolder, _italianRotwKFolderPart, _italianFileNameToTrack),
                Path.Combine(_appDataRoamingFolder, _russianRotwKFolderPart, _russianFileNameToTrack),
                Path.Combine(_appDataRoamingFolder, _polishRotwKFolderPart, _polishFileNameToTrack),
                Path.Combine(_appDataRoamingFolder, _dutchRotwKFolderPart, _dutchFileNameToTrack)
            };
            _possibleFilePaths.AddRange(list);

            _possibleFileDirectories = new List<string>();
            var listOfDirs = new List<string>()
            {
                Path.Combine(_appDataRoamingFolder, _englishRotwKFolderPart),
                Path.Combine(_appDataRoamingFolder, _germanRotwKFolderPart),
                Path.Combine(_appDataRoamingFolder, _spanishRotwKFolderPart),
                Path.Combine(_appDataRoamingFolder, _frenchRotwKFolderPart),
                Path.Combine(_appDataRoamingFolder, _italianRotwKFolderPart),
                Path.Combine(_appDataRoamingFolder, _russianRotwKFolderPart),
                Path.Combine(_appDataRoamingFolder, _polishRotwKFolderPart),
                Path.Combine(_appDataRoamingFolder, _dutchRotwKFolderPart)
            };
            _possibleFileDirectories.AddRange(listOfDirs);
        }

        public static string SearchLastReplayFilePath()
        {
            var output = string.Empty;

            foreach (var pfp in _possibleFilePaths)
            {
                if (File.Exists(pfp))
                {
                    output = pfp;
                }
            }

            if (string.IsNullOrEmpty(output))
            {
                foreach (var pfd in _possibleFileDirectories)
                {
                    if (Directory.Exists(pfd) && string.IsNullOrEmpty(output))
                    {
                        output = _possibleFilePaths.Find(x => x.Contains(pfd));
                    }
                }
            }

            if (string.IsNullOrEmpty(output))
            {
                throw new Exception("Unable to find both last replay file path and it's directory.");
            }

            return output;
        }

        public static string GetLastReplayFilePath()
        {
            var replayDir = ConfigurationManager.AppSettings["LastReplayFileDirectory"];
            var gameLang = ConfigurationManager.AppSettings["GameLanguage"];

            if (string.IsNullOrEmpty(replayDir))
            {
                MessageBox.Show("Unable to get replay file directory. Please configure and then restart your app.");
            }

            if (string.IsNullOrEmpty(gameLang))
            {
                MessageBox.Show("Unable to game language setting. Please configure and then restart your app.");
            }

            var replayFileName = GetFileName(gameLang);

            return Path.Combine(replayDir, replayFileName);
        }

        public static bool IsValid(string lastReplayFilePath)
        {
            if (string.IsNullOrEmpty(lastReplayFilePath))
            {
                return false;
            }

            return _possibleFilePaths.Any(x => x == lastReplayFilePath);
        }

        private static string GetFileName(string gameLanguage)
        {
            switch (gameLanguage)
            {
                case "English":
                    return _englishFileNameToTrack;

                case "German":
                    return _germanFileNameToTrack;

                case "Spanish":
                    return _spanishFileNameToTrack;

                case "French":
                    return _frenchFileNameToTrack;

                case "Italian":
                    return _italianFileNameToTrack;

                case "Russian":
                    return _russianFileNameToTrack;

                case "Polish":
                    return _polishFileNameToTrack;

                case "Dutch":
                    return _dutchFileNameToTrack;

                default:
                    return null;
            }
        }
    }
}
