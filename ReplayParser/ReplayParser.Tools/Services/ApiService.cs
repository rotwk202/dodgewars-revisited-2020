﻿using ReplayParser.Tools.Model;
using System;
using System.Net;
using System.Configuration;
using System.Threading.Tasks;
using RestSharp;
using log4net;
using System.Windows.Forms;

namespace ReplayParser.Tools.Services
{
    public class ApiService
    {
        private static readonly string _apiServerEndpoint = ConfigurationManager.AppSettings["TargetServer"];
        private readonly Uri _apiServer;
        private const string _getTokenRoute = "Account/GetToken";
        private const string _reportLossRoute = "GameReport/ReportLoss";
        private string ParserClientName = "console";
        private readonly ILog _logger;

        public ApiService()
        {
            _apiServer = new Uri(_apiServerEndpoint);
            _logger = LogManager.GetLogger("InfoLogger");
        }

        public async Task<string> GetToken(ApiUser user)
        {
            var client = new RestClient(_apiServer);
            var request = new RestRequest(_getTokenRoute, Method.POST);
            request.AddJsonBody(user);
            request.AddHeader("ClientName", ParserClientName);
            
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Content.Replace("\"", string.Empty);
            }

            MessageBox.Show(response.Content.Replace("\"", string.Empty));
            return null;
        }

        public async Task<bool> ReportLossNew(string filePath, string userEmail)
        {
            var client = new RestClient(_apiServer);
            var request = new RestRequest(_reportLossRoute, Method.POST);
            request.AlwaysMultipartFormData = true;
            request.AddHeader("Content-Type", "multipart/form-data");
            request.AddFile("file", filePath, "bfme2replay");
            request.AddHeader("Email", userEmail);
            request.AddHeader("Token", CoreAppDomainService._token);
            request.AddHeader("ClientName", ParserClientName);

            _logger.Info($"ApiService.ReportLossNew(): file path: {filePath}");
            _logger.Info($"ApiService.ReportLossNew(): email: {userEmail}");

            IRestResponse response = await client.ExecuteAsync(request);
            _logger.Info($"ApiService.ReportLossNew(): reponse - {response}");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            MessageBox.Show(response.Content.Replace("\"", string.Empty));
            return false;
        }
    }
}
