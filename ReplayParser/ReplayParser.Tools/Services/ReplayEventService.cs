﻿using log4net;
using ReplayParser.Tools.Properties;
using System;

namespace ReplayParser.Tools.Services
{
    public class ReplayEventService
    {
        private readonly GameReportService _gameReportService;
        public event EventHandler GameReportedSuccessfully;
        private readonly ILog _logger;

        public ReplayEventService(string lastReplayPath)
        {
            _gameReportService = new GameReportService();
            _logger = LogManager.GetLogger("InfoLogger");
        }

        public async void OnRoTWKLossButtonPress(object source, HotKeyEventArgsRoTWKLoss e)
        {
            _logger.Info($"ReplayEventService.OnRoTWKLossButtonPress(): report key was pressed.");
            var lossReportedCorrectly = await _gameReportService.ReportLoss();
            EventHandler handler = GameReportedSuccessfully;

            if (lossReportedCorrectly)
            {
                PlayerLoseNotification();
                handler?.Invoke(this, EventArgs.Empty);
            }
        }

        public void PlayerLoseNotification()
        {
            var allResources = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceNames();
            if (allResources.Length > 0)
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer(Resources.eva_defeated_notification);
                player.Play();
            }
        }
    }
}
