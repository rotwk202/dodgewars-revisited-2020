﻿using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReplayParser.Tools.Services
{
    public class GameReportService
    {
        private readonly ApiService _apiService;

        public GameReportService()
        {
            _apiService = new ApiService();
        }

        public async Task<bool> ReportLoss()
        {
            var dwsUser = CoreAppDomainService._dodgeWarsUser;
            var filePath = CoreAppDomainService.LastReplayFilePath;

            if(dwsUser is null)
            {
                MessageBox.Show("Please log in first.");
                return false;
            }

            if (!SystemFilesService.IsValid(CoreAppDomainService.LastReplayFilePath))
            {
                MessageBox.Show("Last replay file path is invalid. Please configure your app first.");
                return false;
            }

            return await _apiService.ReportLossNew(filePath, dwsUser.Email);
        }
    }
}
