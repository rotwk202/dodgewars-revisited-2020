﻿using Newtonsoft.Json;
using ReplayParser.Tools.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using Microsoft.Win32;
using log4net;
using System.Configuration;
using System.Threading.Tasks;

namespace ReplayParser.Tools.Services
{
    public class CoreAppDomainService
    {
        private readonly ApiService _apiService;
        public static ApiUser _dodgeWarsUser;
        public static string _token = string.Empty;
        private static string _lastReplayPath = string.Empty;
        private const Keys _reportLossButtonRoTWK = Keys.F6;
        public static string _appLogFilePath;
        private readonly ILog _logger;
        private ReplayEventService _replayEventService;
        private readonly string _myCredentialsFilePath;
        public event EventHandler UpdateGameReportMessage;
        private GameReportService _gameReportService;

        public static string LastReplayFilePath { get { return _lastReplayPath; } }

        public CoreAppDomainService(ApiService apiService)
        {
            _apiService = apiService;
            _logger = LogManager.GetLogger("InfoLogger");
            _myCredentialsFilePath = ConfigurationManager.AppSettings["UserCredentialsFilePath"];
            _gameReportService = new GameReportService();
        }

        public async Task<bool> LogInUserAsync(string login, string password)
        {
            _dodgeWarsUser = new ApiUser();

            if(!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(password))
            {
                _dodgeWarsUser.Email = login;
                _dodgeWarsUser.Password = password;
                _token = await GetToken(_dodgeWarsUser);

                if (!string.IsNullOrEmpty(_token))
                {
                    return true;
                }
                return false;
            }

            return false;
        }

        public void LogOutUser()
        {
            _token = null;
            if (File.Exists(_myCredentialsFilePath))
            {
                File.Delete(_myCredentialsFilePath);
            }
        }

        public void StartProgram()
        {
            SetReplayFilePath();
            InitializeService();
            RegisterGlobalHotkeys();
        }

        private void RegisterGlobalHotkeys()
        {
            HotKeyRoTWKLossListener.RegisterHotKey(_reportLossButtonRoTWK, KeyModifiersRoTWKLoss.NoRepeat);
            HotKeyRoTWKLossListener.HotKeyPressed += _replayEventService.OnRoTWKLossButtonPress;
        }

        private void InitializeService()
        {
            _replayEventService = new ReplayEventService(_lastReplayPath);
            _replayEventService.GameReportedSuccessfully += NotifyAppForm;
        }

        private void NotifyAppForm(object sender, EventArgs e)
        {
            UpdateGameReportMessage?.Invoke(this, e);
        }

        private void SetReplayFilePath()
        {
            try
            {
                _lastReplayPath = SystemFilesService.GetLastReplayFilePath();
                _logger.Info($"Replay file path to report set to: {_lastReplayPath}");
            }
            catch (Exception e)
            {
                _logger.Info($"CoreAppDomainService.SetReplayFilePath() exception: {e.Message}");
            }
        }

        private void SetUserCredentialsFromFile(ApiUser dodgewarsUser)
        {
            var data = GetUserCredentialsFromFile();
            if (data.Count == 0)
            {
                return;
            }
            dodgewarsUser.Email = data[0];
            dodgewarsUser.Password = data[1];
        }

        private async Task<string> GetToken(ApiUser dodgeWarsUser)
        {
            Result<string> failResult;

            var output = string.Empty;
            try
            {
                output = await CallApiForToken(dodgeWarsUser);
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                    {
                        failResult = JsonConvert.DeserializeObject<Result<string>>(reader.ReadToEnd());
                        return output;
                    }
                }
                else
                {
                    return output;
                }
            }

            return output;
        }

        private async Task<string> CallApiForToken(ApiUser dodgeWarsUser)
        {
            try
            {
                return await _apiService.GetToken(dodgeWarsUser);
            }
            catch (WebException wex)
            {
                throw wex;
            }
        }

        private void WriteUserCredentialsToFile(string email, string password)
        {
            var content = string.Format("{0},{1}", email, password);
            using (var fileStream = File.OpenWrite(_myCredentialsFilePath))
            {
                using (TextWriter tw = new StreamWriter(fileStream))
                {
                    tw.WriteLine(content);
                    tw.Close();
                }
            }
        }

        private List<string> GetUserCredentialsFromFile()
        {
            var output = new List<string>();
            if (!File.Exists(_myCredentialsFilePath))
            {
                return output;
            }

            var content = File.ReadAllLines(_myCredentialsFilePath);

            if(content.Length == 0)
            {
                return output;
            }

            string c = content[0];

            return c.Split(',').ToList();
        }

        private void RemoveCredentialsFile()
        {
            if (File.Exists(_myCredentialsFilePath))
            {
                File.Delete(_myCredentialsFilePath);
            }
        }

        public void RememberMe(bool isRememberMeChecked)
        {
            if (_dodgeWarsUser is null) _dodgeWarsUser = new ApiUser();
            if (isRememberMeChecked)
            {
                SetUserCredentialsFromFile(_dodgeWarsUser);
            }
            else
            {
                RemoveCredentialsFile();
            }
        }

        public bool IsAppStartupSet()
        {
            var value = Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\\Microsoft\Windows\CurrentVersion\Run", "ReplayParser", null);

            return value != null;
        }

        public void SetApplicationStartup(bool isAppStartupChecked)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey
                (@"SOFTWARE\\Microsoft\Windows\CurrentVersion\Run", true);

            if (isAppStartupChecked)
            {
                rk.SetValue("ReplayParser", Application.ExecutablePath);
            }
            else
            {
                rk.DeleteValue("ReplayParser", false);
            }
        }
    
        public async Task ReportLoss()
        {
            _logger.Info($"CoreAppDomainService.ReportLoss(): UI button was pressed.");
            var lossReportedCorrectly = await _gameReportService.ReportLoss();

            if (lossReportedCorrectly)
            {
                _replayEventService.PlayerLoseNotification();
                NotifyAppForm(null, null);
            }
            else
            {

            }
        }
    }
}
