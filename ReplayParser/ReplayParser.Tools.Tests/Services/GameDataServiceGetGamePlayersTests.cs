﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReplayParser.Tools.Services;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace ReplayParser.Tools.Tests
{
    [TestClass]
    public class GameDataServiceGetGamePlayersTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void UsesEmptyListAsParameter()
        {
            //Arrange
            var service = new GameDataService();
            var rows = new List<string>();

            //Act
            service.GetGamePlayers(rows);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UsesNullAsParameter()
        {
            //Arrange
            var service = new GameDataService();

            //Act
            service.GetGamePlayers(null);

            //Assert
        }

        [TestMethod]
        public void ReturnsThreePlayers()
        {
            //Arrange
            var service = new GameDataService();
            var rows = new List<string>()
            {
                "S=HBQSS^,5E7B9C3D,8094,TT,-1,-1,0,-1,0,1,0",
                "HLava_Lad,26201DC,8094,TT,-1,-2,-1,-1,0,1,0",
                "HViva,4FC3D44F,8094,TT,11,-1,-1,-1,0,1,0"
            };

            //Act
            var players = service.GetGamePlayers(rows);

            //Assert
            Assert.IsNotNull(players);
            Assert.AreEqual(3, players.Count);
        }

        [TestMethod]
        public void RemovesUselessRows()
        {
            //Arrange
            var service = new GameDataService();
            var rows = new List<string>()
            {
                "S=HBQSS^,5E7B9C3D,8094,TT,-1,-1,0,-1,0,1,0",
                "X",
                "HLava_Lad,26201DC,8094,TT,-1,-2,-1,-1,0,1,0",
                "HViva,4FC3D44F,8094,TT,11,-1,-1,-1,0,1,0",
                "X"
            };

            //Act
            var players = service.GetGamePlayers(rows);

            //Assert
            Assert.IsNotNull(players);
            Assert.AreEqual(3, players.Count);
        }

        [TestMethod]
        public void PlayerHasCorrectValues()
        {
            //Arrange
            var service = new GameDataService();
            var rows = new List<string>()
            {
                "S=HBQSS^,5E7B9C3D,8094,TT,-1,-1,0,-1,0,1,0",
            };

            //Act
            var players = service.GetGamePlayers(rows);

            //Assert
            Assert.IsNotNull(players);
            var player = players.FirstOrDefault();
            Assert.IsNotNull(player);
            Assert.AreEqual("BQSS^", player.Nickname);
            Assert.AreEqual(-1, player.Team);
            Assert.AreNotEqual(-2, player.IsObserver);
        }

        [TestMethod]
        public void ThreePlayersHostWasObserver()
        {
            //Arrange
            var service = new GameDataService();

            var rows = new List<string>()
            {
                "S=HDeimaN,53952E06,8094,TT,4,-2,-1,-1,0,1,0",          //game host
                "HMr.SmoKkkk,B25DA9EA,8094,TT,8,-1,-1,-1,0,1,0",        //game player
                "HBraham,5450AFFC,8094,TT,7,-1,-1,-1,0,1,0"             //game player
            };

            //Act
            var players = service.GetGamePlayers(rows);

            //Assert
            Assert.AreEqual(players.Find(x => x.Nickname == "Mr.SmoKkkk").IsGameHost, false);
            Assert.AreEqual(players.Find(x => x.Nickname == "Braham").IsGameHost, false);
        }

        [TestMethod]
        public void ThreePlayersHostWasPlayer()
        {
            //Arrange
            var service = new GameDataService();

            var rows = new List<string>()
            {
                "S=HBraham,5450AFFC,8094,TT,7,-1,-1,-1,0,1,0",         //game player, game host
                "HDeimaN,53952E06,8094,TT,4,-1,-2,-1,0,1,0",           //game observer
                "HMr.SmoKkkk,B25DA9EA,8094,TT,8,-1,-1,-1,0,1,0"        //game player
            };

            //Act
            var players = service.GetGamePlayers(rows);

            //Assert
            Assert.AreEqual(players.Find(x => x.Nickname == "Mr.SmoKkkk").IsGameHost, false);
            Assert.AreEqual(players.Find(x => x.Nickname == "Braham").IsGameHost, true);
        }

        [TestMethod]
        public void SetsCorrectIsGameHostFlags_HostObsTwoPlayersPlay()
        {
            //Arrange
            var sut = new GameDataService();
            var testReplayPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestResources", "HostObsTwoPlayersPlay.bfme2replay");
            var parserService = new ParserService(testReplayPath);
            var gameDataService = new GameDataService();

            //Act
            var splittedRows = parserService.ParseReplay();
            var playersInGame = gameDataService.GetGamePlayers(splittedRows);

            //Assert
            Assert.AreEqual(playersInGame.Find(x => x.Nickname == "kYGo`").IsGameHost, true);
            Assert.AreEqual(playersInGame.Find(x => x.Nickname == "Asba`").IsGameHost, false);
            Assert.AreEqual(playersInGame.Find(x => x.Nickname == "andy").IsGameHost, false);
            Assert.AreEqual(playersInGame.Find(x => x.Nickname == "Saussage").IsGameHost, false);
            Assert.AreEqual(playersInGame.Find(x => x.Nickname == "Random").IsGameHost, false);
        }

        [TestMethod]
        public void SetsCorrectIsGameHostFlags_HostIsPlayingOneVsOne()
        {
            //Arrange
            var sut = new GameDataService();
            var testReplayPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestResources", "HostIsPlayingOneVsOne.bfme2replay");
            var parserService = new ParserService(testReplayPath);
            var gameDataService = new GameDataService();

            //Act
            var splittedRows = parserService.ParseReplay();
            var playersInGame = gameDataService.GetGamePlayers(splittedRows);

            //Assert
            Assert.AreEqual(playersInGame.Find(x => x.Nickname == "Muadd_Gurl").IsGameHost, true);
            Assert.AreEqual(playersInGame.Find(x => x.Nickname == "[Ace]YodA`").IsGameHost, false);
            Assert.AreEqual(playersInGame.Find(x => x.Nickname == "Andy").IsGameHost, false);
        }
    }
}
