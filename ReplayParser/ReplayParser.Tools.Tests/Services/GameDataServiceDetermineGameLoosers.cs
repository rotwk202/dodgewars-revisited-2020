﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReplayParser.Tools.Services;
using System.Collections.Generic;
using System.Linq;
using ReplayParser.Tools.Model;

namespace ReplayParser.Tools.Tests
{
    [TestClass]
    public class GameDataServiceDetermineGameLoosers
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UsesNulLReporterNickname()
        {
            //Arrange
            var service = new GameDataService();
            string reporterNickname = null;
            var players = new List<Player>
            {
                new Player { Nickname = "Joe", Team = 1, ArmyReplayId = 2 },
                new Player { Nickname = "Jan", Team = 22, ArmyReplayId = 33 },
            };

            //Act
            service.DetermineGameLoosers(reporterNickname, players);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UsesEmptyReporterNicknameString()
        {
            //Arrange
            var service = new GameDataService();
            string reporterNickname = string.Empty;
            var players = new List<Player>
            {
                new Player { Nickname = "Joe", Team = 1, ArmyReplayId = 2 },
                new Player { Nickname = "Jan", Team = 22, ArmyReplayId = 33 },
            };

            //Act
            service.DetermineGameLoosers(reporterNickname, players);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void DetectsFFAGame()
        {
            //Arrange
            var service = new GameDataService();

            //Act
            var players = new List<Player>
            {
                new Player { Nickname = "Joe", Team = -1, ArmyReplayId = 2 },
                new Player { Nickname = "Jan", Team = -1, ArmyReplayId = 33 },
                new Player { Nickname = "Chriss", Team = -1, ArmyReplayId = 33 },
                new Player { Nickname = "Bob", Team = -1, ArmyReplayId = 33 },
            };
         
            string reporterNickname = "Hombre";
            service.DetermineGameLoosers(reporterNickname, players);
            
            //Assert
        }

        [TestMethod]
        public void DeterminesOneVsOneMatchCase0()
        {
            //Arrange
            var service = new GameDataService();
            var players = new List<Player>
            {
                new Player { Nickname = "Joe", Team = -1, ArmyReplayId = 2 },
                new Player { Nickname = "Jan", Team = -1, ArmyReplayId = 33 },
            };

            //Act
            var reporterNickname = "Joe";
            service.DetermineGameLoosers(reporterNickname, players);

            var loosingPlayer = players.First(x => x.Nickname.Contains(reporterNickname));
            var loosingPlayerActualFlag = loosingPlayer.IsLooser;
            var loosingPlayerExpectedFlag = true;

            //Assert
            Assert.AreEqual(loosingPlayerExpectedFlag, loosingPlayerActualFlag);
            Assert.AreEqual(false, loosingPlayer.IsObserver);
            Assert.AreEqual(-1, loosingPlayer.Team);
        }

        [TestMethod]
        public void DeterminesOneVsOneMatchCase4()
        {
            //Arrange
            var service = new GameDataService();

            var players = new List<Player>
            {
                new Player { Nickname = "Hanna", Team = -1, ArmyReplayId = 2 },
                new Player { Nickname = "anna", Team = -1, ArmyReplayId = 33 },
            };

            //Act
            var reporterNickname = "Hanna";

            service.DetermineGameLoosers(reporterNickname, players);
            var loosingPlayer = players.FirstOrDefault(x => x.Nickname == reporterNickname);

            //Assert
            Assert.IsNotNull(loosingPlayer);
            Assert.AreEqual(true, loosingPlayer.IsLooser);
            Assert.AreEqual(false, loosingPlayer.IsObserver);
        }

        [TestMethod]
        public void DeterminesOneVsOneMatchCase4Reversed()
        {
            //Arrange
            var service = new GameDataService();

            var players = new List<Player>
            {
                new Player { Nickname = "Hanna", Team = -1, ArmyReplayId = 2 },
                new Player { Nickname = "anna", Team = -1, ArmyReplayId = 33 },
            };

            //Act
            var reporterNickname = "anna";

            service.DetermineGameLoosers(reporterNickname, players);
            var loosingPlayer = players.FirstOrDefault(x => x.Nickname == reporterNickname);

            //Assert
            Assert.IsNotNull(loosingPlayer);
            Assert.AreEqual(true, loosingPlayer.IsLooser);
            Assert.AreEqual(false, loosingPlayer.IsObserver);
        }

        //1v1
        [TestMethod]
        public void DeterminesOneVsOneMatchCase1()
        {
            //Arrange
            var service = new GameDataService();
            var players = new List<Player>
            {
                new Player { Nickname = "OhReaLLy", Team = -1, ArmyReplayId = 2 },
                new Player { Nickname = "Otto", Team = -1, ArmyReplayId = 33 },
            };
            //Act
            var reporterNickname = "Otto";
            service.DetermineGameLoosers(reporterNickname, players);
            var loosingPlayer = players.First(x => x.Nickname.Contains(reporterNickname));
            var loosingPlayerActualFlag = loosingPlayer.IsLooser;
            var loosingPlayerExpectedFlag = true;

            //Assert
            Assert.AreEqual(loosingPlayerExpectedFlag, loosingPlayerActualFlag);
            Assert.AreEqual(false, loosingPlayer.IsObserver);
            Assert.AreEqual(-1, loosingPlayer.Team);
        }

        [TestMethod]
        public void DeterminesTwoVsTwoMatchNonobserversCount()
        {
            //Arrange
            var service = new GameDataService();
            var rows = new List<string>()
            {
                "S=HTHE_GLUTEM,B29BDDD7,8094,TT,-1,-2,-1,-1,0,1,0",
                "Hn00bAlexis,27A6DCE,8094,TT,-1,-1,1,0,0,1,0",
                "HAGG|DJDiap,5B9D0A9C,8094,TT,11,-1,2,0,0,1,0",
                "HAndy_San,D98102E3,8094,TT,9,-1,-1,1,0,1,0",
                "HSolas,2F444B48,8094,TT,-1,-1,-1,1,0,1,0"
            };
            //Act
            var reporterNickname = "Solas";
            var players = service.GetGamePlayers(rows);
            service.DetermineGameLoosers(reporterNickname, players);
            var nonObserverPlayersCount = players.Where(x => x.IsObserver == false).Count();

            //Assert
            Assert.AreEqual(4, nonObserverPlayersCount);
        }
    }
}
