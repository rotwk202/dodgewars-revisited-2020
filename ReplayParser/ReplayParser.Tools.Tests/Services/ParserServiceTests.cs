﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReplayParser.Tools.Services;
using System;
using System.IO;

namespace ReplayParser.Tools.Tests.Services
{
    [TestClass]
    public class ParserServiceTests
    {
        //nieistniejaca sciezka do last replay
        [TestMethod]
        [ExpectedException(typeof(DirectoryNotFoundException))]
        public void ParseReplay_UsesNotExistingFullFilePath()
        {
            //Arrange
            const string fullFilePath = @"D:\Games\RoTWK\My CW replays this month\vs john isen elves 1-0.BfME2Replay";
            var service = new ParserService(fullFilePath);

            //Act
            service.ParseReplay();

            //Assert
        }

        //poda nulle
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParseReplay_UsesNullAsFullFilePath()
        {
            //Arrange
            const string fullFilePath = null;
            var service = new ParserService(fullFilePath);

            //Act
            var splittedRows = service.ParseReplay();

            //Assert
            Assert.IsNotNull(splittedRows);
        }

        //poda sama sciezke bez pliku
        [TestMethod]
        [ExpectedException(typeof(DirectoryNotFoundException))]
        public void ParseReplay_UsesNotFullFilePath()
        {
            //Arrange
            const string fullFilePath = @"C:\Users\Radek\AppData\Roaming\My The Lord of the Rings, The Rise of the Witch-king Files\Replays\";
            var service = new ParserService(fullFilePath);

            //Act
            var splittedRows = service.ParseReplay();

            //Assert
            Assert.IsNotNull(splittedRows);
        }

        //poda pusta sciezke
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ParseReplay_UsesEmptyStringAsFullFilePath()
        {
            //Arrange
            string fullFilePath = string.Empty;
            var service = new ParserService(fullFilePath);

            //Act
            var splittedRows = service.ParseReplay();

            //Assert
            Assert.IsNotNull(splittedRows);
        }

        //poda plik lezacy na dowolnej sciezce o rozsZerzeniu BfME2Replay, ale z inna nazwa, niz Last Replay
    }
}
