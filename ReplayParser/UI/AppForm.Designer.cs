﻿namespace UI
{
    partial class AppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppForm));
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.cbWindowsStartup = new System.Windows.Forms.CheckBox();
            this.btnReportLoss = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnConfigureApp = new System.Windows.Forms.Button();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblLogin = new System.Windows.Forms.Label();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pbInvalidFilePath = new System.Windows.Forms.PictureBox();
            this.pbCorrectFilePath = new System.Windows.Forms.PictureBox();
            this.lblLastReplayFilePathValue = new System.Windows.Forms.Label();
            this.lblLastReplayFilePathHeader = new System.Windows.Forms.Label();
            this.LblMessage = new System.Windows.Forms.Label();
            this.lblLoginStatus = new System.Windows.Forms.Label();
            this.lblLoggedIn = new System.Windows.Forms.Label();
            this.lblStartupStatus = new System.Windows.Forms.Label();
            this.lblInfoHeaderOne = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbInvalidFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCorrectFilePath)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnLogOut);
            this.panel1.Controls.Add(this.cbWindowsStartup);
            this.panel1.Controls.Add(this.btnReportLoss);
            this.panel1.Controls.Add(this.btnLogin);
            this.panel1.Controls.Add(this.btnConfigureApp);
            this.panel1.Controls.Add(this.lblPassword);
            this.panel1.Controls.Add(this.lblLogin);
            this.panel1.Controls.Add(this.tbPassword);
            this.panel1.Controls.Add(this.tbLogin);
            this.panel1.Location = new System.Drawing.Point(4, 257);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(376, 215);
            this.panel1.TabIndex = 0;
            // 
            // btnLogOut
            // 
            this.btnLogOut.Location = new System.Drawing.Point(278, 143);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(75, 23);
            this.btnLogOut.TabIndex = 7;
            this.btnLogOut.Text = "Log out";
            this.btnLogOut.UseVisualStyleBackColor = true;
            this.btnLogOut.Visible = false;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // cbWindowsStartup
            // 
            this.cbWindowsStartup.AutoSize = true;
            this.cbWindowsStartup.Location = new System.Drawing.Point(23, 114);
            this.cbWindowsStartup.Name = "cbWindowsStartup";
            this.cbWindowsStartup.Size = new System.Drawing.Size(199, 17);
            this.cbWindowsStartup.TabIndex = 5;
            this.cbWindowsStartup.Text = "Start application on Windows startup";
            this.cbWindowsStartup.UseVisualStyleBackColor = true;
            this.cbWindowsStartup.CheckedChanged += new System.EventHandler(this.cbWindowsStartup_CheckedChanged);
            // 
            // btnReportLoss
            // 
            this.btnReportLoss.Location = new System.Drawing.Point(23, 178);
            this.btnReportLoss.Name = "btnReportLoss";
            this.btnReportLoss.Size = new System.Drawing.Size(73, 23);
            this.btnReportLoss.TabIndex = 6;
            this.btnReportLoss.Text = "Report loss";
            this.btnReportLoss.UseVisualStyleBackColor = true;
            this.btnReportLoss.Click += new System.EventHandler(this.btnReportLoss_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Enabled = false;
            this.btnLogin.Location = new System.Drawing.Point(278, 143);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "Log in";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnConfigureApp
            // 
            this.btnConfigureApp.Location = new System.Drawing.Point(278, 114);
            this.btnConfigureApp.Name = "btnConfigureApp";
            this.btnConfigureApp.Size = new System.Drawing.Size(75, 23);
            this.btnConfigureApp.TabIndex = 5;
            this.btnConfigureApp.Text = "Configure";
            this.btnConfigureApp.UseVisualStyleBackColor = true;
            this.btnConfigureApp.Click += new System.EventHandler(this.btnConfigureApp_Click);
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(20, 61);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Password";
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Location = new System.Drawing.Point(20, 11);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(33, 13);
            this.lblLogin.TabIndex = 2;
            this.lblLogin.Text = "Login";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(23, 77);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(330, 20);
            this.tbPassword.TabIndex = 1;
            this.tbPassword.TextChanged += new System.EventHandler(this.tbPassword_TextChanged);
            // 
            // tbLogin
            // 
            this.tbLogin.Location = new System.Drawing.Point(23, 27);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(330, 20);
            this.tbLogin.TabIndex = 0;
            this.tbLogin.TextChanged += new System.EventHandler(this.tbLogin_TextChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pbInvalidFilePath);
            this.panel2.Controls.Add(this.pbCorrectFilePath);
            this.panel2.Controls.Add(this.lblLastReplayFilePathValue);
            this.panel2.Controls.Add(this.lblLastReplayFilePathHeader);
            this.panel2.Controls.Add(this.LblMessage);
            this.panel2.Controls.Add(this.lblLoginStatus);
            this.panel2.Controls.Add(this.lblLoggedIn);
            this.panel2.Controls.Add(this.lblStartupStatus);
            this.panel2.Controls.Add(this.lblInfoHeaderOne);
            this.panel2.Location = new System.Drawing.Point(4, 478);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(376, 135);
            this.panel2.TabIndex = 1;
            // 
            // pbInvalidFilePath
            // 
            this.pbInvalidFilePath.Image = ((System.Drawing.Image)(resources.GetObject("pbInvalidFilePath.Image")));
            this.pbInvalidFilePath.Location = new System.Drawing.Point(338, 84);
            this.pbInvalidFilePath.Name = "pbInvalidFilePath";
            this.pbInvalidFilePath.Size = new System.Drawing.Size(24, 23);
            this.pbInvalidFilePath.TabIndex = 10;
            this.pbInvalidFilePath.TabStop = false;
            this.pbInvalidFilePath.Visible = false;
            // 
            // pbCorrectFilePath
            // 
            this.pbCorrectFilePath.Image = ((System.Drawing.Image)(resources.GetObject("pbCorrectFilePath.Image")));
            this.pbCorrectFilePath.Location = new System.Drawing.Point(338, 84);
            this.pbCorrectFilePath.Name = "pbCorrectFilePath";
            this.pbCorrectFilePath.Size = new System.Drawing.Size(29, 23);
            this.pbCorrectFilePath.TabIndex = 9;
            this.pbCorrectFilePath.TabStop = false;
            this.pbCorrectFilePath.Visible = false;
            // 
            // lblLastReplayFilePathValue
            // 
            this.lblLastReplayFilePathValue.AutoSize = true;
            this.lblLastReplayFilePathValue.Location = new System.Drawing.Point(8, 84);
            this.lblLastReplayFilePathValue.MaximumSize = new System.Drawing.Size(320, 0);
            this.lblLastReplayFilePathValue.Name = "lblLastReplayFilePathValue";
            this.lblLastReplayFilePathValue.Size = new System.Drawing.Size(0, 13);
            this.lblLastReplayFilePathValue.TabIndex = 8;
            this.lblLastReplayFilePathValue.Visible = false;
            // 
            // lblLastReplayFilePathHeader
            // 
            this.lblLastReplayFilePathHeader.AutoSize = true;
            this.lblLastReplayFilePathHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLastReplayFilePathHeader.Location = new System.Drawing.Point(8, 71);
            this.lblLastReplayFilePathHeader.Name = "lblLastReplayFilePathHeader";
            this.lblLastReplayFilePathHeader.Size = new System.Drawing.Size(123, 13);
            this.lblLastReplayFilePathHeader.TabIndex = 7;
            this.lblLastReplayFilePathHeader.Text = "Last replay file path:";
            // 
            // LblMessage
            // 
            this.LblMessage.AutoSize = true;
            this.LblMessage.Location = new System.Drawing.Point(207, 17);
            this.LblMessage.Name = "LblMessage";
            this.LblMessage.Size = new System.Drawing.Size(0, 13);
            this.LblMessage.TabIndex = 4;
            // 
            // lblLoginStatus
            // 
            this.lblLoginStatus.AutoSize = true;
            this.lblLoginStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLoginStatus.Location = new System.Drawing.Point(8, 43);
            this.lblLoginStatus.Name = "lblLoginStatus";
            this.lblLoginStatus.Size = new System.Drawing.Size(80, 13);
            this.lblLoginStatus.TabIndex = 3;
            this.lblLoginStatus.Text = "Login status:";
            // 
            // lblLoggedIn
            // 
            this.lblLoggedIn.AutoSize = true;
            this.lblLoggedIn.Location = new System.Drawing.Point(102, 43);
            this.lblLoggedIn.Name = "lblLoggedIn";
            this.lblLoggedIn.Size = new System.Drawing.Size(70, 13);
            this.lblLoggedIn.TabIndex = 2;
            this.lblLoggedIn.Text = "Unauthorized";
            // 
            // lblStartupStatus
            // 
            this.lblStartupStatus.AutoSize = true;
            this.lblStartupStatus.Location = new System.Drawing.Point(102, 17);
            this.lblStartupStatus.Name = "lblStartupStatus";
            this.lblStartupStatus.Size = new System.Drawing.Size(41, 13);
            this.lblStartupStatus.TabIndex = 1;
            this.lblStartupStatus.Text = "Not set";
            // 
            // lblInfoHeaderOne
            // 
            this.lblInfoHeaderOne.AutoSize = true;
            this.lblInfoHeaderOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblInfoHeaderOne.Location = new System.Drawing.Point(8, 17);
            this.lblInfoHeaderOne.Name = "lblInfoHeaderOne";
            this.lblInfoHeaderOne.Size = new System.Drawing.Size(76, 13);
            this.lblInfoHeaderOne.TabIndex = 0;
            this.lblInfoHeaderOne.Text = "App startup:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(359, 238);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DodgeWars Replay Parser v.1.0";
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::UI.Properties.Resources._2;
            this.panel3.Location = new System.Drawing.Point(7, 20);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(346, 212);
            this.panel3.TabIndex = 0;
            // 
            // AppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 614);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AppForm";
            this.Text = "Replay Parser";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbInvalidFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCorrectFilePath)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.CheckBox cbWindowsStartup;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label lblInfoHeaderOne;
		private System.Windows.Forms.Label lblStartupStatus;
        private System.Windows.Forms.Label lblLoggedIn;
        private System.Windows.Forms.Label lblLoginStatus;
        private System.Windows.Forms.Label LblMessage;
        private System.Windows.Forms.Button btnConfigureApp;
        private System.Windows.Forms.Button btnReportLoss;
        private System.Windows.Forms.Label lblLastReplayFilePathValue;
        private System.Windows.Forms.Label lblLastReplayFilePathHeader;
        private System.Windows.Forms.PictureBox pbCorrectFilePath;
        private System.Windows.Forms.PictureBox pbInvalidFilePath;
        private System.Windows.Forms.Button btnLogOut;
    }
}

