﻿using System.Configuration;

namespace UI
{
    partial class ConfigureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLastReplayDirectoryHeader = new System.Windows.Forms.Label();
            this.lblLastReplayDirectoryValue = new System.Windows.Forms.Label();
            this.lblGameLanguageHeader = new System.Windows.Forms.Label();
            this.cbGameLanguages = new System.Windows.Forms.ComboBox();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.btnSaveConfig = new System.Windows.Forms.Button();
            this.btnOpenReplayDir = new System.Windows.Forms.Button();
            this.lbValidReplayDirectories = new System.Windows.Forms.ListBox();
            this.lblValidFileDirectoriesHeader = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblLastReplayDirectoryHeader
            // 
            this.lblLastReplayDirectoryHeader.AutoSize = true;
            this.lblLastReplayDirectoryHeader.Location = new System.Drawing.Point(13, 13);
            this.lblLastReplayDirectoryHeader.Name = "lblLastReplayDirectoryHeader";
            this.lblLastReplayDirectoryHeader.Size = new System.Drawing.Size(104, 13);
            this.lblLastReplayDirectoryHeader.TabIndex = 0;
            this.lblLastReplayDirectoryHeader.Text = "Last replay directory:";
            // 
            // lblLastReplayDirectoryValue
            // 
            this.lblLastReplayDirectoryValue.AutoSize = true;
            this.lblLastReplayDirectoryValue.Location = new System.Drawing.Point(13, 35);
            this.lblLastReplayDirectoryValue.Name = "lblLastReplayDirectoryValue";
            this.lblLastReplayDirectoryValue.Size = new System.Drawing.Size(0, 13);
            this.lblLastReplayDirectoryValue.TabIndex = 1;
            // 
            // lblGameLanguageHeader
            // 
            this.lblGameLanguageHeader.AutoSize = true;
            this.lblGameLanguageHeader.Location = new System.Drawing.Point(13, 119);
            this.lblGameLanguageHeader.Name = "lblGameLanguageHeader";
            this.lblGameLanguageHeader.Size = new System.Drawing.Size(100, 13);
            this.lblGameLanguageHeader.TabIndex = 2;
            this.lblGameLanguageHeader.Text = "My game language:";
            // 
            // cbGameLanguages
            // 
            this.cbGameLanguages.FormattingEnabled = true;
            this.cbGameLanguages.Items.AddRange(new object[] {
            "English",
            "German",
            "Spanish",
            "French",
            "Italian",
            "Russian",
            "Polish",
            "Dutch"});
            this.cbGameLanguages.Location = new System.Drawing.Point(12, 135);
            this.cbGameLanguages.Name = "cbGameLanguages";
            this.cbGameLanguages.Size = new System.Drawing.Size(121, 21);
            this.cbGameLanguages.TabIndex = 3;
            // 
            // btnSaveConfig
            // 
            this.btnSaveConfig.Location = new System.Drawing.Point(440, 320);
            this.btnSaveConfig.Name = "btnSaveConfig";
            this.btnSaveConfig.Size = new System.Drawing.Size(75, 23);
            this.btnSaveConfig.TabIndex = 4;
            this.btnSaveConfig.Text = "Save";
            this.btnSaveConfig.UseVisualStyleBackColor = true;
            this.btnSaveConfig.Click += new System.EventHandler(this.btnSaveConfig_Click);
            // 
            // btnOpenReplayDir
            // 
            this.btnOpenReplayDir.Location = new System.Drawing.Point(12, 71);
            this.btnOpenReplayDir.Name = "btnOpenReplayDir";
            this.btnOpenReplayDir.Size = new System.Drawing.Size(75, 23);
            this.btnOpenReplayDir.TabIndex = 5;
            this.btnOpenReplayDir.Text = "Browse...";
            this.btnOpenReplayDir.UseVisualStyleBackColor = true;
            this.btnOpenReplayDir.Click += new System.EventHandler(this.btnOpenReplayDir_Click);
            // 
            // lbValidReplayDirectories
            // 
            this.lbValidReplayDirectories.FormattingEnabled = true;
            this.lbValidReplayDirectories.HorizontalScrollbar = true;
            this.lbValidReplayDirectories.Items.AddRange(new object[] {
            "English: C:\\Users\\YOUR_USERNAME\\AppData\\Roaming\\My The Lord of the Rings, The Ris" +
                "e of the Witch-king Files\\Replays",
            "German: C:\\Users\\YOUR_USERNAME\\AppData\\Roaming\\Meine Der Herr der Ringe™, Aufstie" +
                "g des Hexenkönigs™-Dateien\\Replays",
            "Spanish: C:\\Users\\YOUR_USERNAME\\AppData\\Roaming\\Mis archivos de El Señor de los A" +
                "nillos, El Resurgir del Rey Brujo\\Replays",
            "French: C:\\Users\\YOUR_USERNAME\\AppData\\Roaming\\Mes fichiers de LSDA, L\'Avènement " +
                "du Roi-sorcier™\\Replays",
            "Italian: C:\\Users\\YOUR_USERNAME\\AppData\\Roaming\\File de Il Signore degli Anelli™ " +
                "- L\'Ascesa del Re Stregone™\\Replays",
            "Russian: C:\\Users\\YOUR_USERNAME\\AppData\\Roaming\\Властелин Колец, Под знаменем Кор" +
                "оля-чародея - Мои файлы\\Replays",
            "Polish: C:\\Users\\YOUR_USERNAME\\AppData\\Roaming\\Moje pliki gry Władca Pierścieni, " +
                "Król Nazguli\\Replays",
            "Dutch: C:\\Users\\YOUR_USERNAME\\AppData\\Roaming\\Mijn The Lord of the Rings, The Ris" +
                "e of the Witch-king-bestanden\\Replays"});
            this.lbValidReplayDirectories.Location = new System.Drawing.Point(12, 191);
            this.lbValidReplayDirectories.Name = "lbValidReplayDirectories";
            this.lbValidReplayDirectories.Size = new System.Drawing.Size(394, 108);
            this.lbValidReplayDirectories.TabIndex = 6;
            // 
            // lblValidFileDirectoriesHeader
            // 
            this.lblValidFileDirectoriesHeader.AutoSize = true;
            this.lblValidFileDirectoriesHeader.Location = new System.Drawing.Point(13, 172);
            this.lblValidFileDirectoriesHeader.Name = "lblValidFileDirectoriesHeader";
            this.lblValidFileDirectoriesHeader.Size = new System.Drawing.Size(238, 13);
            this.lblValidFileDirectoriesHeader.TabIndex = 7;
            this.lblValidFileDirectoriesHeader.Text = "Valid replay directories based on game language:";
            // 
            // ConfigureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 355);
            this.Controls.Add(this.lblValidFileDirectoriesHeader);
            this.Controls.Add(this.lbValidReplayDirectories);
            this.Controls.Add(this.btnOpenReplayDir);
            this.Controls.Add(this.btnSaveConfig);
            this.Controls.Add(this.cbGameLanguages);
            this.Controls.Add(this.lblGameLanguageHeader);
            this.Controls.Add(this.lblLastReplayDirectoryValue);
            this.Controls.Add(this.lblLastReplayDirectoryHeader);
            this.Name = "ConfigureForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configure app";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLastReplayDirectoryHeader;
        private System.Windows.Forms.Label lblLastReplayDirectoryValue;
        private System.Windows.Forms.Label lblGameLanguageHeader;
        private System.Windows.Forms.ComboBox cbGameLanguages;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Button btnSaveConfig;
        private System.Windows.Forms.Button btnOpenReplayDir;
        private System.Windows.Forms.ListBox lbValidReplayDirectories;
        private System.Windows.Forms.Label lblValidFileDirectoriesHeader;
    }
}