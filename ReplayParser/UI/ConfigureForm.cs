﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class ConfigureForm : Form
    {
        public ConfigureForm()
        {
            InitializeComponent();

            this.lblLastReplayDirectoryValue.Text = ConfigurationManager.AppSettings["LastReplayFileDirectory"];
            this.cbGameLanguages.SelectedItem = ConfigurationManager.AppSettings["GameLanguage"];
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            if (cbGameLanguages.SelectedItem == null)
            {
                MessageBox.Show("Please choose your game language.");
                return;
            }

            if (string.IsNullOrEmpty(this.lblLastReplayDirectoryValue.Text) || this.lblLastReplayDirectoryValue == null)
            {
                MessageBox.Show("Please set your last replay file directory.");
                return;
            }

            Configuration configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configFile.AppSettings.Settings["GameLanguage"].Value = cbGameLanguages.SelectedItem.ToString();
            configFile.AppSettings.Settings["LastReplayFileDirectory"].Value = this.lblLastReplayDirectoryValue.Text;

            configFile.Save();
            Application.Exit();
        }

        private void btnOpenReplayDir_Click(object sender, EventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                
                dialog.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    this.lblLastReplayDirectoryValue.Text = dialog.SelectedPath;
                }
            }
        }
    }
}
