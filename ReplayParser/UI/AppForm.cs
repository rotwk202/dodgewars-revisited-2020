﻿using log4net.Config;
using ReplayParser.Tools.Services;
using System;
using System.Windows.Forms;
using System.Configuration;
using System.Threading.Tasks;

namespace UI
{
    public partial class AppForm : Form
    {
        private static ApiService _apiService;
        private static CoreAppDomainService _domainService;
        private string _loggedInMessage;
        private readonly string _guestMessage = "Unauthorized";
        private ConfigureForm configureForm;

        public AppForm()
        {
            XmlConfigurator.Configure();
            _apiService = new ApiService();
            _domainService = new CoreAppDomainService(_apiService);
            _domainService.UpdateGameReportMessage += UpdateGameReportMessage;
            InitializeComponent();

            UpdateWinStartupLabel();
            UpdateCBWindowsStartup();

            _domainService.StartProgram();
            UpdateReplayFilePathLabel();
        }

        private void UpdateCBWindowsStartup()
        {
            if (_domainService.IsAppStartupSet())
            {
                cbWindowsStartup.Checked = true;
            }
        }

        private void UpdateGameReportMessage(object sender, EventArgs e)
        {
            SetText(DateTime.Now.ToShortTimeString());
        }

        private delegate void SetTextCallback(string text);

        private void SetText(string text)
        {
            if (LblMessage.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                Invoke(d, new object[] { text });
            }
            else
            {
                LblMessage.Text = "Last game reported at: " + text;
            }
        }

        private void UpdateWinStartupLabel()
        {
            if (_domainService.IsAppStartupSet())
            {
                lblStartupStatus.Text = "Starts on Windows startup";
            }
            else
            {
                lblStartupStatus.Text = "Not set";
            }
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            if (await _domainService.LogInUserAsync(tbLogin.Text, tbPassword.Text))
            {
                _loggedInMessage = $"Logged in as {CoreAppDomainService._dodgeWarsUser.Email}";
                lblLoggedIn.Text = _loggedInMessage;
                btnLogin.Visible = false;
                btnLogOut.Visible = true;
            }
            else
            {
                lblLoggedIn.Text = _guestMessage;
                btnLogin.Visible = true;
                btnLogOut.Visible = false;
            }
        }

        private void tbLogin_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbLogin.Text) && !string.IsNullOrWhiteSpace(tbPassword.Text))
            {
                btnLogin.Enabled = true;
            }
        }

        private void tbPassword_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbLogin.Text) && !string.IsNullOrWhiteSpace(tbPassword.Text))
            {
                btnLogin.Enabled = true;
            }
        }

        private void cbWindowsStartup_CheckedChanged(object sender, EventArgs e)
        {
            _domainService.SetApplicationStartup(cbWindowsStartup.Checked);
            UpdateWinStartupLabel();
        }

        private void btnConfigureApp_Click(object sender, EventArgs e)
        {
            configureForm = new ConfigureForm();

            configureForm.Show();
        }

        private void UpdateReplayFilePathLabel()
        {
            Configuration configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var x = configFile.AppSettings.Settings["GameLanguage"].Value;
            var a = configFile.AppSettings.Settings["LastReplayFileDirectory"].Value;

            if (SystemFilesService.IsValid(CoreAppDomainService.LastReplayFilePath))
            {
                lblLastReplayFilePathValue.Text = CoreAppDomainService.LastReplayFilePath;
                lblLastReplayFilePathValue.Visible = true;
                pbCorrectFilePath.Visible = true;
                pbInvalidFilePath.Visible = false;
            }
            else
            {
                lblLastReplayFilePathValue.Text = CoreAppDomainService.LastReplayFilePath;
                lblLastReplayFilePathValue.Visible = true;
                pbCorrectFilePath.Visible = false;
                pbInvalidFilePath.Visible = true;
            }
        }

        private async void btnReportLoss_Click(object sender, EventArgs e)
        {
            await _domainService.ReportLoss();
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            _domainService.LogOutUser();
            btnLogOut.Visible = false;
            btnLogin.Visible = true;
            lblLoggedIn.Text = _guestMessage;

            

        }
    }
}
