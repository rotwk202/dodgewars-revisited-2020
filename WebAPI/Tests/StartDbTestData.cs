﻿using System;
using System.Collections.Generic;
using System.Text;
using DALMySql.POCO;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Tests
{
    namespace Tests.CommonTests
    {
        //[TestFixture]
        public class StartDbTestData
        {
            //[Test]
            public async Task One()
            {
                //Arrange
                using var dbWrapper = InmemoryDbWrapper.GetWrapper();

                var map = new Map() { Id = 1, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };

                var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
                var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
                var users = new List<User>() { userOne, userTwo };
                var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "One", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "Two", UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };

                //Games came in incorrect order. Games 4 and 5 are those reported by admin. So right now there is incorrect order.
                //It should be 1-4-5-2-4, but because someone didn't report it is 1,2,3,4,5 - talking about dates here, not ids.
                var game1 = new Game() { Id = 1, PlayedOn = new DateTime(2018, 01, 01), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null, Identifier = 1.ToString() };
                var game2 = new Game() { Id = 2, PlayedOn = new DateTime(2018, 01, 04), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null, Identifier = 2.ToString() };
                var game3 = new Game() { Id = 3, PlayedOn = new DateTime(2018, 01, 05), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null, Identifier = 3.ToString() };
                var game4 = new Game() { Id = 4, PlayedOn = new DateTime(2018, 01, 02), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null, Identifier = 4.ToString() };
                var game5 = new Game() { Id = 5, PlayedOn = new DateTime(2018, 01, 03), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null, Identifier = 5.ToString() };

                //var replay1 = new Replay() { Id = 1, File = new byte[8], GameId = 1, ReplayFileSize = 1024 };
                //var replay2 = new Replay() { Id = 2, File = new byte[8], GameId = 2, ReplayFileSize = 1024 };
                //var replay3 = new Replay() { Id = 3, File = new byte[8], GameId = 3, ReplayFileSize = 1024 };
                //var replay4 = new Replay() { Id = 4, File = new byte[8], GameId = 4, ReplayFileSize = 1024 };
                //var replay5 = new Replay() { Id = 5, File = new byte[8], GameId = 5, ReplayFileSize = 1024 };

                //Regular game that was lost by UserOne and reported.
                var GameDetails1Game1 = new GameDetail() { Id = 1, GameId = 1, PlayerId = 1, IsLooser = true, Team = -1 };
                var GameDetails2Game1 = new GameDetail() { Id = 2, GameId = 1, PlayerId = 2, IsLooser = false, Team = -1 };

                //These are another games reported by UserOne because he's fair.
                var GameDetails3Game2 = new GameDetail() { Id = 3, GameId = 2, PlayerId = 1, IsLooser = true, Team = -1 };
                var GameDetails4Game2 = new GameDetail() { Id = 4, GameId = 2, PlayerId = 2, IsLooser = false, Team = -1 };
                var GameDetails5Game3 = new GameDetail() { Id = 5, GameId = 3, PlayerId = 1, IsLooser = true, Team = -1 };
                var GameDetails6Game3 = new GameDetail() { Id = 6, GameId = 3, PlayerId = 2, IsLooser = false, Team = -1 };

                //These are two games that Player1 won, but Player2 did not report. The came reported by admin.
                var GameDetails7Game4 = new GameDetail() { Id = 7, GameId = 4, PlayerId = 1, IsLooser = false, Team = -1 };
                var GameDetails8Game4 = new GameDetail() { Id = 8, GameId = 4, PlayerId = 2, IsLooser = true, Team = -1 };
                var GameDetails9Game5 = new GameDetail() { Id = 9, GameId = 5, PlayerId = 1, IsLooser = false, Team = -1 };
                var GameDetails10Game5 = new GameDetail() { Id = 10, GameId = 5, PlayerId = 2, IsLooser = true, Team = -1 };

                var GameDetailss = new List<GameDetail>()
            {
                GameDetails1Game1,
                GameDetails2Game1,
                GameDetails3Game2,
                GameDetails4Game2,
                GameDetails5Game3,
                GameDetails6Game3,
                GameDetails7Game4,
                GameDetails8Game4,
                GameDetails9Game5,
                GameDetails10Game5
            };

                var mapsList = new List<Map>() { map };
                var gamesList = new List<Game>() { game1, game2, game3, game4, game5 };
                //var replayList = new List<Replay>() { replay1, replay2, replay3, replay4, replay5 };

                await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
                await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
                await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
                await dbWrapper.PopulateDbWithDataAsync(gamesList.ToArray());
                await dbWrapper.PopulateDbWithDataAsync(GameDetailss.ToArray());
                //await dbWrapper.PopulateDbWithDataAsync(replayList.ToArray());


                //var configMock = new Mock<IConfigProvider>();
                //configMock.Setup(x => x.GetAllowUnknownMapsSetting()).Returns(false);
                //var sut = new AdminService(dbWrapper.DbContext);


            }
        }
    }

}
