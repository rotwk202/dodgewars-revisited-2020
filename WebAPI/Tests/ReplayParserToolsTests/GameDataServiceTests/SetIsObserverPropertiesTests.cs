﻿using Common.Model;
using NUnit.Framework;
using ReplayParserTools.Services;
using System.Collections.Generic;
using System.Linq;

namespace Tests.ReplayParserToolsTests.GameDataServiceTests
{
    [TestFixture]
    public class SetIsObserverPropertiesTests
    {
        [Test]
        public void TeamGameWithObs()
        {
            //Arrange
            var service = new GameDataService();
            var playersInGame = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "OhReaLLy", Team = -1, ArmyReplayId = -2 },
                new ApiPlayer { Name = "Otto", Team = 0, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Asba", Team = 0, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Braham", Team = 1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Dobby", Team = 1, ArmyReplayId = -1 },
            };
            //Act
            service.SetIsObserverProperties(playersInGame);

            //Assert
            Assert.IsTrue(playersInGame.First(x => x.Name == "OhReaLLy").IsObserver);
            Assert.IsTrue(playersInGame.Where(x => x.Name != "OhReaLLy").All(x => x.IsObserver == false));
        }

        [Test]
        public void IndividualGameWithObs()
        {
            //Arrange
            var service = new GameDataService();
            var playersInGame = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "OhReaLLy", Team = -1, ArmyReplayId = -2 },
                new ApiPlayer { Name = "Otto", Team = 0, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Asba", Team = 0, ArmyReplayId = -1 }
            };
            //Act
            service.SetIsObserverProperties(playersInGame);

            //Assert
            Assert.IsTrue(playersInGame.First(x => x.Name == "OhReaLLy").IsObserver);
            Assert.IsTrue(playersInGame.Where(x => x.Name != "OhReaLLy").All(x => x.IsObserver == false));
        }

        [Test]
        public void IndividualGameNoObs()
        {
            //Arrange
            var service = new GameDataService();
            var playersInGame = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "Otto", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Asba", Team = -1, ArmyReplayId = -1 }
            };
            //Act
            service.SetIsObserverProperties(playersInGame);

            //Assert
            Assert.IsTrue(playersInGame.All(x => x.IsObserver == false));
        }

        [Test]
        public void TeamGameNoObs()
        {
            //Arrange
            var service = new GameDataService();
            var playersInGame = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "Otto", Team = 0, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Asba", Team = 0, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Braham", Team = 1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Dobby", Team = 1, ArmyReplayId = -1 }
            };
            //Act
            service.SetIsObserverProperties(playersInGame);

            //Assert
            Assert.IsTrue(playersInGame.All(x => x.IsObserver == false));
        }
    }
}
