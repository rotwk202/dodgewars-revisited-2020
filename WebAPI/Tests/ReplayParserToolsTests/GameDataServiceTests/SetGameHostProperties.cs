﻿using Common.Model;
using NUnit.Framework;
using ReplayParserTools.Services;
using System.Collections.Generic;
using System.Linq;

namespace Tests.ReplayParserToolsTests.GameDataServiceTests
{
    [TestFixture]
    public class SetGameHostProperties
    {
        [Test]
        public void TeamGameWithObs()
        {
            //Arrange
            var service = new GameDataService();
            var playersInGame = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "OhReaLLy", Team = -1, ArmyReplayId = -2 },
                new ApiPlayer { Name = "Otto", Team = 0, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Asba", Team = 0, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Braham", Team = 1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Dobby", Team = 1, ArmyReplayId = -1 },
            };
            //Act
            service.SetGameHostProperties(playersInGame);

            //Assert
            Assert.IsTrue(playersInGame.First(x => x.Name == "OhReaLLy").IsGameHost);
            Assert.IsTrue(playersInGame.Where(x => x.Name != "OhReaLLy").All(x => x.IsGameHost == false));
        }

        [Test]
        public void IndividualGameWithObs()
        {
            //Arrange
            var service = new GameDataService();
            var playersInGame = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "OhReaLLy", Team = -1, ArmyReplayId = -2 },
                new ApiPlayer { Name = "Otto", Team = 0, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Asba", Team = 0, ArmyReplayId = -1 }
            };
            //Act
            service.SetGameHostProperties(playersInGame);

            //Assert
            Assert.IsTrue(playersInGame.First(x => x.Name == "OhReaLLy").IsGameHost);
            Assert.IsTrue(playersInGame.Where(x => x.Name != "OhReaLLy").All(x => x.IsGameHost == false));
        }

    }
}
