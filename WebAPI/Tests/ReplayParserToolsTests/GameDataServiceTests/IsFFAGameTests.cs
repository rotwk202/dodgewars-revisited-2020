﻿using Common.Model;
using NUnit.Framework;
using ReplayParserTools.Services;
using System.Collections.Generic;

namespace Tests.ReplayParserToolsTests.GameDataServiceTests
{
    [TestFixture]
    public class IsFFAGameTests
    {
        [Test]
        public void FFAGameWithoutObsCase1_NoOnePickedTheirTeam()
        {
            //Arrange
            var service = new GameDataService();
            var playersInGame = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "Otto", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Asba", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Braham", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Dobby", Team = -1, ArmyReplayId = -1 }
            };
            //Act
            var result = service.IsFFAGame(playersInGame);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void FFAGameWithoutObsCase_AtLeastOnePlayerPickedTeam()
        {
            //Arrange
            var service = new GameDataService();
            var playersInGame = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "Otto", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Asba", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Braham", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Dobby", Team = 0, ArmyReplayId = -1 },
            };
            //Act
            var result = service.IsFFAGame(playersInGame);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void FFAGameWithObs()
        {
            //Arrange
            var service = new GameDataService();
            var playersInGame = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "OhReaLLy", Team = -1, ArmyReplayId = -2 },      //obs
                new ApiPlayer { Name = "Otto", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Asba", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Braham", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Dobby", Team = -1, ArmyReplayId = -1 },
            };
            //Act
            var result = service.IsFFAGame(playersInGame);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void IndividualGameWithObs()
        {
            //Arrange
            var service = new GameDataService();
            var playersInGame = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "OhReaLLy", Team = -1, ArmyReplayId = -2 },
                new ApiPlayer { Name = "Otto", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Asba", Team = -1, ArmyReplayId = -1 }
            };
            //Act
            var result = service.IsFFAGame(playersInGame);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IndividualGameWithoutObs()
        {
            //Arrange
            var service = new GameDataService();
            var playersInGame = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "Otto", Team = -1, ArmyReplayId = -1 },
                new ApiPlayer { Name = "Asba", Team = -1, ArmyReplayId = -1 }
            };
            //Act
            var result = service.IsFFAGame(playersInGame);

            //Assert
            Assert.IsFalse(result);
        }
    }
}
