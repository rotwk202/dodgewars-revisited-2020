﻿using Common.Model;
using NUnit.Framework;
using ReplayParserTools.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tests.ReplayParserToolsTests.GameDataServiceTests
{
    [TestFixture]
    public class DetermineGameLoosersTests
    {
        [Test]
        public void UsesNulLReporterName()
        {
            //Arrange
            var sut = new GameDataService();
            string reporterName = null;
            var ApiPlayers = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "Joe", Team = 1, ArmyReplayId = 2 },
                new ApiPlayer { Name = "Jan", Team = 22, ArmyReplayId = 33 },
            };

            //Act & Assert
            var ex = Assert.Throws<ArgumentNullException>(() => sut.DetermineGameLoosers(reporterName, ApiPlayers));
        }

        [Test]
        public void UsesEmptyReporterNameString()
        {
            //Arrange
            var sut = new GameDataService();
            string reporterName = string.Empty;
            var ApiPlayers = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "Joe", Team = 1, ArmyReplayId = 2 },
                new ApiPlayer { Name = "Jan", Team = 22, ArmyReplayId = 33 },
            };

            //Act & Assert
            var ex = Assert.Throws<ArgumentNullException>(() => sut.DetermineGameLoosers(reporterName, ApiPlayers));
        }

        [Test]
        public void DetectsFFAGame()
        {
            //Arrange
            var service = new GameDataService();

            //Act
            var ApiPlayers = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "Joe", Team = -1, ArmyReplayId = 2 },
                new ApiPlayer { Name = "Jan", Team = -1, ArmyReplayId = 33 },
                new ApiPlayer { Name = "Chriss", Team = -1, ArmyReplayId = 33 },
                new ApiPlayer { Name = "Bob", Team = -1, ArmyReplayId = 33 },
            };

            string reporterName = "Hombre";
            //Act & Assert
            var ex = Assert.Throws<InvalidOperationException>(() => service.DetermineGameLoosers(reporterName, ApiPlayers));
        }

        [Test]
        public void DeterminesOneVsOneMatchCase0()
        {
            //Arrange
            var service = new GameDataService();
            var ApiPlayers = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "Joe", Team = -1, ArmyReplayId = 2 },
                new ApiPlayer { Name = "Jan", Team = -1, ArmyReplayId = 33 },
            };

            //Act
            var reporterName = "Joe";
            service.DetermineGameLoosers(reporterName, ApiPlayers);

            var loosingApiPlayer = ApiPlayers.First(x => x.Name.Contains(reporterName));
            var loosingApiPlayerActualFlag = loosingApiPlayer.IsLooser;
            var loosingApiPlayerExpectedFlag = true;

            //Assert
            Assert.AreEqual(loosingApiPlayerExpectedFlag, loosingApiPlayerActualFlag);
            Assert.AreEqual(false, loosingApiPlayer.IsObserver);
            Assert.AreEqual(-1, loosingApiPlayer.Team);
        }

        [Test]
        public void DeterminesOneVsOneMatchCase4()
        {
            //Arrange
            var service = new GameDataService();

            var ApiPlayers = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "Hanna", Team = -1, ArmyReplayId = 2 },
                new ApiPlayer { Name = "anna", Team = -1, ArmyReplayId = 33 },
            };

            //Act
            var reporterName = "Hanna";

            service.DetermineGameLoosers(reporterName, ApiPlayers);
            var loosingApiPlayer = ApiPlayers.FirstOrDefault(x => x.Name == reporterName);

            //Assert
            Assert.IsNotNull(loosingApiPlayer);
            Assert.AreEqual(true, loosingApiPlayer.IsLooser);
            Assert.AreEqual(false, loosingApiPlayer.IsObserver);
        }

        [Test]
        public void DeterminesOneVsOneMatchCase4Reversed()
        {
            //Arrange
            var service = new GameDataService();

            var ApiPlayers = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "Hanna", Team = -1, ArmyReplayId = 2 },
                new ApiPlayer { Name = "anna", Team = -1, ArmyReplayId = 33 },
            };

            //Act
            var reporterName = "anna";

            service.DetermineGameLoosers(reporterName, ApiPlayers);
            var loosingApiPlayer = ApiPlayers.FirstOrDefault(x => x.Name == reporterName);

            //Assert
            Assert.IsNotNull(loosingApiPlayer);
            Assert.AreEqual(true, loosingApiPlayer.IsLooser);
            Assert.AreEqual(false, loosingApiPlayer.IsObserver);
        }

        //1v1
        [Test]
        public void DeterminesOneVsOneMatchCase1()
        {
            //Arrange
            var service = new GameDataService();
            var ApiPlayers = new List<ApiPlayer>
            {
                new ApiPlayer { Name = "OhReaLLy", Team = -1, ArmyReplayId = 2 },
                new ApiPlayer { Name = "Otto", Team = -1, ArmyReplayId = 33 },
            };
            //Act
            var reporterName = "Otto";
            service.DetermineGameLoosers(reporterName, ApiPlayers);
            var loosingApiPlayer = ApiPlayers.First(x => x.Name.Contains(reporterName));
            var loosingApiPlayerActualFlag = loosingApiPlayer.IsLooser;
            var loosingApiPlayerExpectedFlag = true;

            //Assert
            Assert.AreEqual(loosingApiPlayerExpectedFlag, loosingApiPlayerActualFlag);
            Assert.AreEqual(false, loosingApiPlayer.IsObserver);
            Assert.AreEqual(-1, loosingApiPlayer.Team);
        }
    }
}
