using ReplayParser.Tools.Model;
using NUnit.Framework;
using Common;

namespace Tests.ReplayParserToolsTests.RotwkRngServiceTests
{
    [TestFixture]

    public class RngRangeOutcomeTest
    {
        [TestCase(
            (uint)442253781, // with this seed
            0, 7,            // and this range
            3,               // this should be the first random outcome
            2)]              // and this should be the second

        [TestCase(
            (uint)1191213156,
            21, 137,
            31,
            97)]


        public void RngRangeOutcomeTestCase(
            uint replaySeed,
            int start,
            int end,
            int firstRngOutcome,
            int secondRngOutcome)
        {
            var rng = new ReplayParser.Tools.Services.RotwkRngService(replaySeed);

            Assert.AreEqual(firstRngOutcome, rng.NextRngInRange(start, end));
            Assert.AreEqual(secondRngOutcome, rng.NextRngInRange(start, end));
        }
    }

    public class RngRawOutcomeTest
    {
        [TestCase((uint)442253781, 2334596401, (uint)1342508051, 3508179258)]
        [TestCase((uint)1191213156, 3136753812, (uint)706892365, 4272281518)]
        [TestCase((uint)1840140, 4260004636, 3724098003, (uint)1688209706)]
        [TestCase((uint)5, (uint)2147483553, 2161329113, 2991948277)]

        public void RngRawOutcomeTestCase(
            uint replaySeed,
            uint expectedInitialRngSeed,
            uint firstRngOutcome,
            uint secondRngOutcome)
        {
            var rng = new ReplayParser.Tools.Services.RotwkRngService(replaySeed);
            Assert.AreEqual(expectedInitialRngSeed, rng.GetSeed());

            Assert.AreEqual(firstRngOutcome, rng.NextRng());
            Assert.AreEqual(secondRngOutcome, rng.NextRng());
        }
    }

    public class RngFactionOutcomeTest
    {
        [TestCase((uint)442253781)]


        public void RngFactionOutcomeTestCase(
            uint replaySeed)
        {
            var rng = new ReplayParser.Tools.Services.RotwkRngService(replaySeed);
            Assert.AreEqual(Constants.Isengard, rng.NextRandomFaction().ToString());
            Assert.AreEqual(Constants.Isengard, rng.NextRandomFaction().ToString());
            Assert.AreEqual(Constants.Elves, rng.NextRandomFaction().ToString());
            Assert.AreEqual(Constants.Men, rng.NextRandomFaction().ToString());
            Assert.AreEqual(Constants.Angmar, rng.NextRandomFaction().ToString());
            Assert.AreEqual(Constants.Mordor, rng.NextRandomFaction().ToString());
            Assert.AreEqual(Constants.Isengard, rng.NextRandomFaction().ToString());
            Assert.AreEqual(Constants.Dwarves, rng.NextRandomFaction().ToString());
            Assert.AreEqual(Constants.Goblins, rng.NextRandomFaction().ToString());
        }
    }

    public class FactionTest
    {
        [TestCase()]
        public void FactionTestCase()
        {
            var obs = RotwkFaction.FromName(Constants.Observer);
            Assert.AreEqual(obs.GetRawIdentifier(), -2);

            var random = RotwkFaction.FromName(Constants.Random);
            Assert.AreEqual(random.GetRawIdentifier(), -1);

            Assert.AreEqual(
                RotwkFaction.FromRandom(0).GetRawIdentifier(),
                3);
            Assert.AreEqual(
                RotwkFaction.FromRandom(1).GetRawIdentifier(),
                5);
            Assert.AreEqual(
                RotwkFaction.FromRandom(2).GetRawIdentifier(),
                6);
            Assert.AreEqual(
                RotwkFaction.FromRandom(3).GetRawIdentifier(),
                7);

            Assert.AreEqual(RotwkFaction.GetNumberOfPlayableFactions(), 7);
        }
    }
}
