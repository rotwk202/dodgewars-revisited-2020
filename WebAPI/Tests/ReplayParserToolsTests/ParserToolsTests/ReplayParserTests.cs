using Common.Model;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System;
using Common;

namespace Tests.ReplayParserToolsTests.ParserToolsTests
{
    [TestFixture]
    public class ReplayParsingTests
    {
        [TestCase(
            "Replay 39.BfME2Replay",
            Constants.Goblins, Constants.Isengard)]

        [TestCase(
            "Replay 30.BfME2Replay",
            Constants.Angmar, Constants.Mordor, Constants.Dwarves, Constants.Dwarves)]

        [TestCase(
            "gerald_and_nabalt_reked__[GameReplays.org].bfme2replay",
            Constants.Dwarves, Constants.Angmar, Constants.Elves, Constants.Dwarves, Constants.Isengard, Constants.Men)]

        [TestCase(
            "elves_vs_mordor_14.BfME2Replay",
            Constants.Elves, Constants.Observer, Constants.Mordor)]

        [TestCase(
            "Replay 20.BfME2Replay",
            Constants.Isengard, Constants.Observer, Constants.Men, Constants.Observer)]

        [TestCase(
            "Goblins_Mirror fixed.BfME2Replay",
            Constants.Observer, Constants.Goblins, Constants.Goblins)]

        [TestCase(
            "Loser_s_Finals_Game_5__[GameReplays.org].bfme2replay",
            Constants.Observer, Constants.Angmar, Constants.Elves, Constants.Observer)]

        [TestCase(
            "elves_vs_angmar_1_plat__[GameReplays.org].bfme2replay",
            Constants.Observer, Constants.Elves, Constants.Angmar)]

        [TestCase(
            "platino_men_v_elf__[GameReplays.org].bfme2replay",
            Constants.Men, Constants.Observer, Constants.Elves)]

        [TestCase(
            "Negative Seed Test.BfME2Replay",
            Constants.Random, Constants.Isengard, Constants.Angmar, Constants.Random)]

        public void FacionExtractionTest(
            string replayFile,
            params string[] expectedFactions)
        {
            var replayFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", replayFile);

            using (var replayFileStream = new FileStream(replayFilePath, FileMode.Open))
            {
                using (var replayParser = new ReplayParserTools.ParserTools.ReplayParser(replayFileStream))
                {
                    var _replayData = replayParser.Parse();

                    List<ApiPlayer> outcome = _replayData.HeaderFields.Players;

                    for (int i = 0; i < outcome.Count; i++)
                    {
                        // Assert.AreEqual(outcome.Capacity, 3);
                        string actual = outcome[i].Army;
                        string expected = expectedFactions[i];
                        Assert.AreEqual(expected, actual);
                    }
                }

            }
        }
    }
}
