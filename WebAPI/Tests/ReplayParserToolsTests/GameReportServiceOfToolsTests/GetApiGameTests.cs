﻿using Common.Model;
using Common.Services;
using DALMySql.POCO;
using Moq;
using NUnit.Framework;
using ReplayParserTools.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Tests.ReplayParserToolsTests.GameReportServiceOfToolsTests
{
    [TestFixture]
    public class GetApiGameTests
    {
        [Test]
        public async Task TrollingUser_TriesToReportReplay_ForSomeoneElse()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();

            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var trollingUser = new User() { Id = 3, Email = "trollingUser@gmail.com", Password = "ccc", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo, trollingUser };
            var userNicknames = new List<Player>()
            {
                new Player() { Id = 1, Name = "One", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "StrayTest1", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 3, Name = "Testacc", UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 4, Name = "fakeName", UserId = trollingUser.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 }
            };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(userNicknames.ToArray());
            var failedReportServiceMock = new Mock<FailedReportService>(dbWrapper._DbContext);
            var sut = new GameReportServiceOfTools(dbWrapper._DbContext, failedReportServiceMock.Object);

            //Act & Assert
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", "valid-1v1-replay.BfME2Replay");
            using (var inputStream = File.OpenRead(filePath))
            {
                var ex = Assert.Throws<InvalidOperationException>(() => sut.GetApiGame(inputStream, trollingUser.Email));
                Assert.IsTrue(ex.Message.Contains("You're not present in this replay! Your nicknames:"));
            }
        }

        [Test]
        public async Task UsersTriesToReportGame_WhenHeHasNoNicknameCreated()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();

            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            var userNicknames = new List<Player>()
            {
                new Player() { Id = 1, Name = "One", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "StrayTest1", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(userNicknames.ToArray());
            var sut = new GameReportServiceOfTools(dbWrapper._DbContext, null);

            //Act & Assert
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", "valid-1v1-replay.BfME2Replay");
            using (var inputStream = File.OpenRead(filePath))
            {
                var ex = Assert.Throws<ArgumentNullException>(() => sut.GetApiGame(inputStream, userTwo.Email));
                Assert.AreEqual("No nicknames bound to this dodgewars account were found. (Parameter 'userNicknames')",
                    ex.Message);
            }
        }

        [Test]
        public async Task Parses_1v1Game_Correctly()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();

            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            const string stratTestNickname = "StrayTest1";
            const string testAccNickname = "Testacc";
            var userNicknames = new List<Player>()
            {
                new Player() { Id = 1, Name = stratTestNickname, UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = testAccNickname, UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 }
            };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(userNicknames.ToArray());
            var sut = new GameReportServiceOfTools(dbWrapper._DbContext, null);

            //Act
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", "valid-1v1-replay.BfME2Replay");
            ApiGame result;
            using (var inputStream = File.OpenRead(filePath))
            {
                result = sut.GetApiGame(inputStream, userOne.Email);
            }

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("80E0A4B62BD153029F34A6A5F6EBFD34", result.Identifier);
            Assert.AreEqual("map mp fords of isen", result.ReplayMapTextId);
            Assert.AreEqual(new DateTime(2015, 7, 24, 22, 5, 12), result.PlayedOn);
            Assert.IsNotNull(result.Players);
            Assert.AreEqual(2, result.Players.Count);

            //Assert StrayTest1
            Assert.AreEqual(stratTestNickname, result.Players.FirstOrDefault(x => x.Name == stratTestNickname).Name);
            Assert.AreEqual(3, result.Players.FirstOrDefault(x => x.Name == stratTestNickname).ArmyReplayId);
            Assert.AreEqual(-1, result.Players.FirstOrDefault(x => x.Name == stratTestNickname).Team);
            Assert.IsFalse(result.Players.FirstOrDefault(x => x.Name == stratTestNickname).IsObserver);
            Assert.IsTrue(result.Players.FirstOrDefault(x => x.Name == stratTestNickname).IsLooser);
            Assert.IsTrue(result.Players.FirstOrDefault(x => x.Name == stratTestNickname).IsGameHost);

            //Assert Testacc
            Assert.AreEqual(testAccNickname, result.Players.FirstOrDefault(x => x.Name == testAccNickname).Name);
            Assert.AreEqual(-1, result.Players.FirstOrDefault(x => x.Name == testAccNickname).ArmyReplayId);
            Assert.AreEqual(-1, result.Players.FirstOrDefault(x => x.Name == testAccNickname).Team);
            Assert.IsFalse(result.Players.FirstOrDefault(x => x.Name == testAccNickname).IsObserver);
            Assert.IsFalse(result.Players.FirstOrDefault(x => x.Name == testAccNickname).IsLooser);
            Assert.IsFalse(result.Players.FirstOrDefault(x => x.Name == testAccNickname).IsGameHost);
        }

        [Test]
        public async Task Given_FFAGame_ThrowsException
            ()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();

            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            const string stratTestNickname = "StrayTest1";
            const string testAccNickname = "Testacc";
            var userNicknames = new List<Player>()
            {
                new Player() { Id = 1, Name = stratTestNickname, UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = testAccNickname, UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 }
            };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(userNicknames.ToArray());
            var sut = new GameReportServiceOfTools(dbWrapper._DbContext, null);

            //Act & Assert
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", "FFA.BfME2Replay");
            using (var inputStream = File.OpenRead(filePath))
            {
                var ex = Assert.Throws<InvalidOperationException>(() => sut.GetApiGame(inputStream, userOne.Email));
                Assert.AreEqual("You're unable to report FFA games.", ex.Message);
            }

            
        }

    }
}
