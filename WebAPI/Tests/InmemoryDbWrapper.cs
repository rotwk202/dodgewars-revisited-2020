﻿using DALMySql;
using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Tests
{
    public class InmemoryDbWrapper : IDisposable
    {
        public LadderContext _DbContext { get; set; }
        protected InmemoryDbWrapper()
        {
            DbContextOptions<LadderContext> options = BuildConnectionOptions();
            _DbContext = new LadderContext(options);
            _DbContext.Database.EnsureCreated();
        }
        public static InmemoryDbWrapper GetWrapper()
        {
            var wrapper = new InmemoryDbWrapper();

            return wrapper;
        }

        private static DbContextOptions<LadderContext> BuildConnectionOptions()
        {
            var csBuilder = new SqliteConnectionStringBuilder
            {
                DataSource = ":memory:"
            };
            var connectionString = csBuilder.ToString();
            var connection = new SqliteConnection(connectionString);
            connection.Open();
            var builder = new DbContextOptionsBuilder<LadderContext>();
            builder.UseSqlite(connection);

            var options = builder.Options;
            return options;
        }

        public async Task PopulateDbWithDataAsync(object[] testData)
        {

            await _DbContext.AddRangeAsync(testData);
            await _DbContext.SaveChangesAsync();
        }

        public void MigrateUp()
        {
            
            _DbContext.Database.Migrate();
        }

        public string GetConnString()
        {
            using var conn = _DbContext.Database.GetDbConnection();
            conn.Open();
            var res = conn.Query("select * [User]");


            return null;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _DbContext.Dispose();
                }

                disposedValue = true;
            }
        }


        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
