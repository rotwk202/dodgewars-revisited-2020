﻿using BusinessLogic.PointsSystem;
using BusinessLogic.Services;
using DALMySql.POCO;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tests.BusinessLogicTests.ServicesTests
{
    [TestFixture]
    public class PlayerHistoryServiceTests
    {
        private async Task PopulateTestData(InmemoryDbWrapper dbWrapper)
        {
            #region BasicData
            var map = new Map() { Id = 1, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "One", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "Two", UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };
            #endregion

            #region Games
            var game1 = new Game() { Id = 1, Identifier = "C4CA4238A0B923820DCC509A6F75849B", PlayedOn = new DateTime(2018, 01, 01), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
            var GameDetails1Game1 = new GameDetail() { Id = 1, GameId = 1, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Angmar };
            var GameDetails2Game1 = new GameDetail() { Id = 2, GameId = 1, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Angmar };
            var rating1Game1 = new Rating() { Id = 1, GameId = 1, Delta = -15, PlayerId = 1 };
            var rating2Game1 = new Rating() { Id = 2, GameId = 1, Delta = 15, PlayerId = 2 };

            var game2 = new Game() { Id = 2, Identifier = "C81E728D9D4C2F636F067F89CC14862C", PlayedOn = new DateTime(2018, 01, 02), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
            var GameDetails1Game2 = new GameDetail() { Id = 3, GameId = 2, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Angmar };
            var GameDetails2Game2 = new GameDetail() { Id = 4, GameId = 2, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Angmar };
            var rating1Game2 = new Rating() { Id = 3, GameId = 2, Delta = -15, PlayerId = 1 };
            var rating2Game2 = new Rating() { Id = 4, GameId = 2, Delta = 15, PlayerId = 2 };

            var game3 = new Game() { Id = 3, Identifier = "ECCBC87E4B5CE2FE28308FD9F2A7BAF3", PlayedOn = new DateTime(2018, 01, 03), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
            var GameDetails1Game3 = new GameDetail() { Id = 5, GameId = 3, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Angmar };
            var GameDetails2Game3 = new GameDetail() { Id = 6, GameId = 3, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Angmar };
            var rating1Game3 = new Rating() { Id = 5, GameId = 3, Delta = -15, PlayerId = 1 };
            var rating2Game3 = new Rating() { Id = 6, GameId = 3, Delta = 15, PlayerId = 2 };

            var game4 = new Game() { Id = 4, Identifier = "A87FF679A2F3E71D9181A67B7542122C", PlayedOn = new DateTime(2018, 01, 04), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
            var GameDetails1Game4 = new GameDetail() { Id = 7, GameId = 4, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Angmar };
            var GameDetails2Game4 = new GameDetail() { Id = 8, GameId = 4, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Angmar };
            var rating1Game4 = new Rating() { Id = 7, GameId = 4, Delta = -15, PlayerId = 1 };
            var rating2Game4 = new Rating() { Id = 8, GameId = 4, Delta = 15, PlayerId = 2 };

            var game5 = new Game() { Id = 5, Identifier = "E4DA3B7FBBCE2345D7772B0674A318D5", PlayedOn = new DateTime(2018, 01, 05), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
            var GameDetails1Game5 = new GameDetail() { Id = 9, GameId = 5, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Angmar };
            var GameDetails2Game5 = new GameDetail() { Id = 10, GameId = 5, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Angmar };
            var rating1Game5 = new Rating() { Id = 9, GameId = 5, Delta = -15, PlayerId = 1 };
            var rating2Game5 = new Rating() { Id = 10, GameId = 5, Delta = 15, PlayerId = 2 };

            var game6 = new Game() { Id = 6, Identifier = "1679091C5A880FAF6FB5E6087EB1B2DC", PlayedOn = new DateTime(2018, 01, 06), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
            var GameDetails1Game6 = new GameDetail() { Id = 11, GameId = 6, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Angmar };
            var GameDetails2Game6 = new GameDetail() { Id = 12, GameId = 6, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Angmar };
            var rating1Game6 = new Rating() { Id = 11, GameId = 6, Delta = -15, PlayerId = 1 };
            var rating2Game6 = new Rating() { Id = 12, GameId = 6, Delta = 15, PlayerId = 2 };

            var game7 = new Game() { Id = 7, Identifier = "8F14E45FCEEA167A5A36DEDD4BEA2543", PlayedOn = new DateTime(2018, 01, 07), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
            var GameDetails1Game7 = new GameDetail() { Id = 13, GameId = 7, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Angmar };
            var GameDetails2Game7 = new GameDetail() { Id = 14, GameId = 7, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Angmar };
            var rating1Game7 = new Rating() { Id = 13, GameId = 7, Delta = -15, PlayerId = 1 };
            var rating2Game7 = new Rating() { Id = 14, GameId = 7, Delta = 15, PlayerId = 2 };

            var game8 = new Game() { Id = 8, Identifier = "C9F0F895FB98AB9159F51FD0297E236D", PlayedOn = new DateTime(2018, 01, 08), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
            var GameDetails1Game8 = new GameDetail() { Id = 15, GameId = 8, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Angmar };
            var GameDetails2Game8 = new GameDetail() { Id = 16, GameId = 8, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Angmar };
            var rating1Game8 = new Rating() { Id = 15, GameId = 8, Delta = -15, PlayerId = 1 };
            var rating2Game8 = new Rating() { Id = 16, GameId = 8, Delta = 15, PlayerId = 2 };

            var game9 = new Game() { Id = 9, Identifier = "45C48CCE2E2D7FBDEA1AFC51C7C6AD26", PlayedOn = new DateTime(2018, 01, 09), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
            var GameDetails1Game9 = new GameDetail() { Id = 17, GameId = 9, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Angmar };
            var GameDetails2Game9 = new GameDetail() { Id = 18, GameId = 9, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Angmar };
            var rating1Game9 = new Rating() { Id = 17, GameId = 9, Delta = -15, PlayerId = 1 };
            var rating2Game9 = new Rating() { Id = 18, GameId = 9, Delta = 15, PlayerId = 2 };

            var game10 = new Game() { Id = 10, Identifier = "D3D9446802A44259755D38E6D163E820", PlayedOn = new DateTime(2018, 01, 10), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
            var GameDetails1Game10 = new GameDetail() { Id = 19, GameId = 10, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Angmar };
            var GameDetails2Game10 = new GameDetail() { Id = 20, GameId = 10, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Angmar };
            var rating1Game10 = new Rating() { Id = 19, GameId = 10, Delta = -15, PlayerId = 1 };
            var rating2Game10 = new Rating() { Id = 20, GameId = 10, Delta = 15, PlayerId = 2 };

            var maps = new List<Map>() { map };
            var games = new List<Game>() { game1, game2, game3, game4, game5, game6, game7, game8, game9, game10 };
            var gameDetails = new List<GameDetail>()
            {
                GameDetails1Game1, GameDetails2Game1,
                GameDetails1Game2, GameDetails2Game2,
                GameDetails1Game3, GameDetails2Game3,
                GameDetails1Game4, GameDetails2Game4,
                GameDetails1Game5, GameDetails2Game5,
                GameDetails1Game6, GameDetails2Game6,
                GameDetails1Game7, GameDetails2Game7,
                GameDetails1Game8, GameDetails2Game8,
                GameDetails1Game9, GameDetails2Game9,
                GameDetails1Game10, GameDetails2Game10,
            };

            var ratings = new List<Rating>()
            {
                rating1Game1, rating2Game1,
                rating1Game2, rating2Game2,
                rating1Game3, rating2Game3,
                rating1Game4, rating2Game4,
                rating1Game5, rating2Game5,
                rating1Game6, rating2Game6,
                rating1Game7, rating2Game7,
                rating1Game8, rating2Game8,
                rating1Game9, rating2Game9,
                rating1Game10, rating2Game10,
            };

            #endregion

            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(maps.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(games.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(gameDetails.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(ratings.ToArray());
        }

        private static IEnumerable<TestCaseData> TestCases
        {
            get
            {
                yield return new TestCaseData(5, "One", 0, Constants.ParmanentLadderType, 5, 10, 6);
                yield return new TestCaseData(5, "One", 1, Constants.ParmanentLadderType, 5, 5, 1);
                yield return new TestCaseData(3, "One", 0, Constants.ParmanentLadderType, 3, 10, 8);
                yield return new TestCaseData(10, "One", 2, Constants.ParmanentLadderType, 0, null, null);
            }
        }

        [Test, TestCaseSource(nameof(TestCases))]

        public async Task GetPlayerHistoryTests(int gameCount,
            string playerName,
            int pageNumber,
            string ladderType,
            int expectedGamesCount,
            int? expectedFirstGameIdOnList,
            int? expectedLastGameIdOnList)
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            await PopulateTestData(dbWrapper);


            var sut = new PlayerHistoryService(dbWrapper._DbContext);

            //Act
            var result = await sut.GetHistory(gameCount, playerName, pageNumber, ladderType);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedGamesCount, result.Count);
            Assert.AreEqual(expectedFirstGameIdOnList, result.FirstOrDefault()?.GamePrimaryKeyId);
            Assert.AreEqual(expectedLastGameIdOnList, result.LastOrDefault()?.GamePrimaryKeyId);
        }
    }
}
