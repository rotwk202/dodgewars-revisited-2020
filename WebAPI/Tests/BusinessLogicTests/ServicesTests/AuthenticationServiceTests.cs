﻿using BusinessLogic.Services;
using Common;
using Common.Model;
using DALMySql.POCO;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace Tests.BusinessLogicTests.ServicesTests
{
    [TestFixture]
    public class AuthenticationServiceTests
    {
        private static string ExistingLogin = "playerOne@gmail.com";
        private static string AnotherExistingLogin = "playerTwo@gmail.com";

        private static IEnumerable<TestCaseData> TestCases
        {
            get
            {
                yield return new TestCaseData(null, null, Constants.WebClientName, "Error trying to authenticate user. No login was provided.");
                yield return new TestCaseData("", null, Constants.WebClientName, "Error trying to authenticate user. No login was provided.");
                yield return new TestCaseData("login", "secret", null, $"Client name was not provided. Valid values: {Constants.ParserClientName} or {Constants.WebClientName}.");
                yield return new TestCaseData("NotFoundLogin", "aaa", Constants.WebClientName, "User with given login was not found in system.");
                yield return new TestCaseData(AnotherExistingLogin, "aaa", Constants.ParserClientName, "Token provided does not exist.");
                yield return new TestCaseData(ExistingLogin, "64E8F571253F7436A09256B409C81E6F", Constants.ParserClientName, $"Given token 64E8F571253F7436A09256B409C81E6F does not belong to user with login {ExistingLogin}.");
                yield return new TestCaseData(ExistingLogin, "562DD385E2B2BB79051B7B7BD29393C0", Constants.ParserClientName, "Given token has expired. Please log in again to renew it.");
            }
        }

        [Test, TestCaseSource(nameof(TestCases))]
        public async Task AuthenticateUserTests(string login, string token, string clientName, string expectedMessage)
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();

            var userOne = new User()
            { 
                Id = 1, 
                Email = ExistingLogin, 
                Password = "47BCE5C74F589F4867DBD57E9CA9F808", 
                CreatedOn = DateTime.Now ,
                TokenConsole = "562DD385E2B2BB79051B7B7BD29393C0",
                TokenConsoleExpiresOn = new DateTime(2010, 1, 1),
                TokenWeb = "7ABCB19C37B91F8B86D8D41B9F075E4F",
                TokenWebExpiresOn = DateTime.Now.AddDays(14),
            };
            var userTwo = new User()
            {
                Id = 2,
                Email = AnotherExistingLogin,
                Password = "47BCE5C74F589F4867DBD57E9CA9F808",
                CreatedOn = DateTime.Now
            };
            var users = new List<User>() { userOne, userTwo };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AuthenticationService(dbWrapper._DbContext);

            //Act & Assert
            var ex = Assert.ThrowsAsync<AuthenticationException>(() => sut.AuthenticateUser(login, token, clientName));
            Assert.AreEqual(expectedMessage, ex.Message);
        }

        [Test]
        public async Task GetAdminToken_ThrowsException_When_AdminUserDoesNotExist()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var secretPassword = "SecretPassword";

            var apiUser = new ApiUser() { Password = secretPassword };
            var sut = new AuthenticationService(dbWrapper._DbContext);

            //Act & Assert
            var ex = Assert.ThrowsAsync<KeyNotFoundException>(() => sut.GetAdminToken(apiUser));
            Assert.AreEqual($"Admin token not defined. Please create user {AuthenticationService.SecretAdmin} in the database.", ex.Message);
        }

        [Test]
        public async Task GetAdminToken_ThrowsException_When_ProvidedAdminPassword_Is_Incorrect()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var userAdminName = AuthenticationService.SecretAdmin;
            var secretPassword = "SecretPassword";

            var userOne = new User() { Id = 1, Email = userAdminName, Password = secretPassword, CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };

            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());

            var apiUser = new ApiUser() { Password = "WrongPassword" };
            var sut = new AuthenticationService(dbWrapper._DbContext);

            //Act & Assert
            var ex = Assert.ThrowsAsync<Exception>(() => sut.GetAdminToken(apiUser));
            Assert.AreEqual($"Login or password provided are incorrect.", ex.Message);
        }

        [Test]
        public async Task GetAdminToken_GivenCorrectCredentials_ReturnsHashedPassword()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var userAdminName = AuthenticationService.SecretAdmin;
            var secretPassword = "SecretPassword";
            var secretPasswordHash = "58D613129C5E71DE57EE3F44C5CE16BC";

            var userOne = new User() { Id = 1, Email = userAdminName, Password = secretPasswordHash, CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };

            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());

            var apiUser = new ApiUser() { Password = secretPassword };
            var sut = new AuthenticationService(dbWrapper._DbContext);

            //Act
            var result = await sut.GetAdminToken(apiUser);

            //Assert
            Assert.AreEqual(secretPasswordHash, result);
        }

        [Test]
        public async Task GetAdminToken_GivenCorrectCredentials_DoesNotUpdatePassword()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var userAdminName = AuthenticationService.SecretAdmin;
            var secretPassword = "SecretPassword";
            var secretPasswordHash = "58D613129C5E71DE57EE3F44C5CE16BC";
            var tokenConsole = "tokenConsole";
            var tokenWeb = "tokenWeb";

            var userOne = new User() { Id = 1, Email = userAdminName, Password = secretPasswordHash, TokenConsole = tokenConsole, TokenWeb = tokenWeb, CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };

            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());

            var apiUser = new ApiUser() { Password = secretPassword };
            var sut = new AuthenticationService(dbWrapper._DbContext);
            var dbUser = await dbWrapper._DbContext.User.AsQueryable().FirstOrDefaultAsync(x => x.Email == userAdminName);

            //Act
            var result = await sut.GetAdminToken(apiUser);

            //Assert
            Assert.AreEqual(secretPasswordHash, dbUser.Password);
            Assert.AreEqual(tokenConsole, dbUser.TokenConsole);
            Assert.AreEqual(tokenWeb, dbUser.TokenWeb);
        }

        [Test]
        public async Task GetToken_GivenCorrectAdminCredentials_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var userAdminName = AuthenticationService.SecretAdmin;
            var secretPassword = "SecretPassword";
            var secretPasswordHash = "58D613129C5E71DE57EE3F44C5CE16BC";

            var userOne = new User() { Id = 1, Email = userAdminName, Password = secretPasswordHash, CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };

            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());

            var apiUser = new ApiUser() { Email = userAdminName, Password = secretPassword };
            var sut = new AuthenticationService(dbWrapper._DbContext);

            //Act & Assert
            var ex = Assert.ThrowsAsync<InvalidOperationException>(() => sut.GetToken(apiUser, Constants.WebClientName));
            Assert.AreEqual($"You're unable to log in as admin.", ex.Message);
        }

    }
}
