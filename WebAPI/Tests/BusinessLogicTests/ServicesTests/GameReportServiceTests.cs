﻿using BusinessLogic.Services;
using BusinessLogic.Validators;
using Common.Interfaces;
using Common.Model;
using Common.Services;
using DALMySql.POCO;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tests.BusinessLogicTests.ServicesTests
{
    [TestFixture]
    public class GameReportServiceTests
    {
        [Test]
        public async Task Given_CorrectApiGameData_ReportsLoss()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();

            #region DbArrange
            var dbMapId = 1;
            var map = new Map() { Id = dbMapId, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "One", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "Two", UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };

            var mapsList = new List<Map>() { map };
            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());

            #endregion

            const string uniqueIdentifier = "74B87337454200D4D33F80C4663DC5E5";
            var apiGame = new ApiGame()
            {
                Identifier = uniqueIdentifier,
                PlayedOn = DateTime.Now,
                ReplayFile = new byte[256],
                ReplayMapTextId = "map mp fords of isen",
                LastReplayFileLength = 256,
                Players = new List<ApiPlayer>()
                {
                    new ApiPlayer() { Name = "One", Army = Common.Constants.Angmar, IsGameHost = true, IsLooser = false, IsObserver = false },
                    new ApiPlayer() { Name = "Two", Army = Common.Constants.Elves, IsGameHost = false, IsLooser = true, IsObserver = false },
                }
            };

            var configMock = new Mock<IConfigProvider>();
            var providerMock = new Mock<ISqlConnectionProvider>();
            configMock.Setup(x => x.GetAllowUnknownMapsSetting()).Returns(true);
            var validator = new GameValidator(configMock.Object);
            var sut = new GameReportService(dbWrapper._DbContext, validator, null, null, providerMock.Object);

            //Act
            sut.ReportLoss(apiGame);

            //Assert
            var gameEntityQueryable = dbWrapper._DbContext.Game.AsQueryable();
            var gameAdded = gameEntityQueryable.FirstOrDefault(x => x.Identifier == uniqueIdentifier);
            Assert.AreEqual(1, gameEntityQueryable.Count());
            Assert.IsNotNull(gameAdded);
            Assert.AreEqual(dbMapId, gameAdded.MapId);
            Assert.AreEqual(apiGame.Identifier, gameAdded.Identifier);
            Assert.AreEqual(apiGame.PlayedOn, gameAdded.PlayedOn);

        }

        [Test]
        public async Task Given_NonExistingPlayer_ThrowsException_DoesNotAddGame_AddsFailedReport()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();

            #region DbArrange
            var dbMapId = 1;
            var map = new Map() { Id = dbMapId, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "One", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "Two", UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };

            var mapsList = new List<Map>() { map };
            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());

            #endregion

            var apiGame = new ApiGame()
            {
                Identifier = 1.ToString(),
                PlayedOn = DateTime.Now,
                ReplayFile = new byte[256],
                ReplayMapTextId = "map mp fords of isen",
                LastReplayFileLength = 256,
                Players = new List<ApiPlayer>()
                {
                    new ApiPlayer() { Name = "One", Army = Common.Constants.Angmar, IsGameHost = true, IsLooser = false, IsObserver = false },
                    new ApiPlayer() { Name = "NonExistingDude", Army = Common.Constants.Elves, IsGameHost = false, IsLooser = true, IsObserver = false },
                }
            };

            var configMock = new Mock<IConfigProvider>();
            var providerMock = new Mock<ISqlConnectionProvider>();
            configMock.Setup(x => x.GetAllowUnknownMapsSetting()).Returns(true);
            var validator = new GameValidator(configMock.Object);
            var frsMock = new Mock<FailedReportService>(dbWrapper._DbContext);
            var sut = new GameReportService(dbWrapper._DbContext, validator, null, frsMock.Object, providerMock.Object);

            //Act
            var ex = Assert.Throws<Exception>(() => sut.ReportLoss(apiGame));
            Assert.AreEqual(1, dbWrapper._DbContext.FailedReport.Count());
            Assert.AreEqual(0, dbWrapper._DbContext.Game.Count());
            Assert.AreEqual(0, dbWrapper._DbContext.GameDetail.Count());
        }

    }
}
