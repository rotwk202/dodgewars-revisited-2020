﻿using BusinessLogic.Services;
using Common;
using Common.Interfaces;
using Common.Model;
using DALMySql.POCO;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tests.BusinessLogicTests.ServicesTests
{
    [TestFixture]
    public class StatisticsServiceTests
    {
        private static IEnumerable<TestCaseData> GetOverallArmiesStatsTestCases
        {
            get
            {
                var game1 = new Game() { Id = 1, Identifier = 1.ToString(), PlayedOn = new DateTime(2018, 01, 01), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game1 = new GameDetail() { Id = 1, GameId = 1, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game1 = new GameDetail() { Id = 2, GameId = 1, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Elves };
                var game2 = new Game() { Id = 2, Identifier = 2.ToString(), PlayedOn = new DateTime(2018, 01, 02), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game2 = new GameDetail() { Id = 3, GameId = 2, PlayerId = 1, IsLooser = false, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game2 = new GameDetail() { Id = 4, GameId = 2, PlayerId = 2, IsLooser = true, Team = -1, Army = Common.Constants.Elves };
                var game3 = new Game() { Id = 3, Identifier = 3.ToString(), PlayedOn = new DateTime(2018, 01, 03), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game3 = new GameDetail() { Id = 5, GameId = 3, PlayerId = 1, IsLooser = false, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game3 = new GameDetail() { Id = 6, GameId = 3, PlayerId = 2, IsLooser = true, Team = -1, Army = Common.Constants.Elves };

                var gamesCaseOne = new List<Game>() { game1, game2, game3 };
                var gameDetailsCaseOne = new List<GameDetail>() { GameDetails1Game1, GameDetails2Game1, GameDetails1Game2, GameDetails2Game2, GameDetails1Game3, GameDetails2Game3 };

                yield return new TestCaseData(gamesCaseOne, gameDetailsCaseOne, 33.0d);

                var game1CaseTwo = new Game() { Id = 1, Identifier = 1.ToString(), PlayedOn = new DateTime(2018, 01, 01), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game1CaseTwo = new GameDetail() { Id = 1, GameId = 1, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game1CaseTwo = new GameDetail() { Id = 2, GameId = 1, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Elves };
                var game2CaseTwo = new Game() { Id = 2, Identifier = 2.ToString(), PlayedOn = new DateTime(2018, 01, 02), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game2CaseTwo = new GameDetail() { Id = 3, GameId = 2, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game2CaseTwo = new GameDetail() { Id = 4, GameId = 2, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Elves };
                var game3CaseTwo = new Game() { Id = 3, Identifier = 3.ToString(), PlayedOn = new DateTime(2018, 01, 03), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game3CaseTwo = new GameDetail() { Id = 5, GameId = 3, PlayerId = 1, IsLooser = false, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game3CaseTwo = new GameDetail() { Id = 6, GameId = 3, PlayerId = 2, IsLooser = true, Team = -1, Army = Common.Constants.Elves };

                var gamesCaseTwo = new List<Game>() { game1CaseTwo, game2CaseTwo, game3CaseTwo };
                var gameDetailsCaseTwo = new List<GameDetail>() { GameDetails1Game1CaseTwo, GameDetails2Game1CaseTwo, GameDetails1Game2CaseTwo, GameDetails2Game2CaseTwo, GameDetails1Game3CaseTwo, GameDetails2Game3CaseTwo };

                yield return new TestCaseData(gamesCaseTwo, gameDetailsCaseTwo, 67.0d);

                var game1CaseThree = new Game() { Id = 1, Identifier = 1.ToString(), PlayedOn = new DateTime(2018, 01, 01), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game1CaseThree = new GameDetail() { Id = 1, GameId = 1, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game1CaseThree = new GameDetail() { Id = 2, GameId = 1, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Elves };
                var game2CaseThree = new Game() { Id = 2, Identifier = 2.ToString(), PlayedOn = new DateTime(2018, 01, 02), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game2CaseThree = new GameDetail() { Id = 3, GameId = 2, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game2CaseThree = new GameDetail() { Id = 4, GameId = 2, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Elves };
                var game3CaseThree = new Game() { Id = 3, Identifier = 3.ToString(), PlayedOn = new DateTime(2018, 01, 03), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game3CaseThree = new GameDetail() { Id = 5, GameId = 3, PlayerId = 1, IsLooser = false, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game3CaseThree = new GameDetail() { Id = 6, GameId = 3, PlayerId = 2, IsLooser = true, Team = -1, Army = Common.Constants.Elves };
                var game4CaseThree = new Game() { Id = 4, Identifier = 4.ToString(), PlayedOn = new DateTime(2018, 01, 03), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game4CaseThree = new GameDetail() { Id = 7, GameId = 4, PlayerId = 1, IsLooser = false, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game4CaseThree = new GameDetail() { Id = 8, GameId = 4, PlayerId = 2, IsLooser = true, Team = -1, Army = Common.Constants.Elves };


                var gamesCaseThree = new List<Game>() { game1CaseThree, game2CaseThree, game3CaseThree, game4CaseThree };
                var gameDetailsCaseThree = new List<GameDetail>() { GameDetails1Game1CaseThree, GameDetails2Game1CaseThree, GameDetails1Game2CaseThree, GameDetails2Game2CaseThree, GameDetails1Game3CaseThree, GameDetails2Game3CaseThree, GameDetails1Game4CaseThree, GameDetails2Game4CaseThree };

                yield return new TestCaseData(gamesCaseThree, gameDetailsCaseThree, 50d);


            }
        }

        private static IEnumerable<TestCaseData> PlayerCardTestData
        {
            get
            {
                var game1 = new Game() { Id = 1, Identifier = 1.ToString(), PlayedOn = new DateTime(2018, 01, 01), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game1 = new GameDetail() { Id = 1, GameId = 1, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game1 = new GameDetail() { Id = 2, GameId = 1, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Elves };
                var game2 = new Game() { Id = 2, Identifier = 2.ToString(), PlayedOn = new DateTime(2018, 01, 02), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game2 = new GameDetail() { Id = 3, GameId = 2, PlayerId = 1, IsLooser = true, Team = -1, Army = Common.Constants.Mordor };
                var GameDetails2Game2 = new GameDetail() { Id = 4, GameId = 2, PlayerId = 2, IsLooser = false, Team = -1, Army = Common.Constants.Elves };
                var game3 = new Game() { Id = 3, Identifier = 3.ToString(), PlayedOn = new DateTime(2018, 01, 03), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game3 = new GameDetail() { Id = 5, GameId = 3, PlayerId = 1, IsLooser = false, Team = -1, Army = Common.Constants.Goblins };
                var GameDetails2Game3 = new GameDetail() { Id = 6, GameId = 3, PlayerId = 3, IsLooser = true, Team = -1, Army = Common.Constants.Goblins };
                var game4 = new Game() { Id = 4, Identifier = 4.ToString(), PlayedOn = new DateTime(2018, 01, 03), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null };
                var GameDetails1Game4 = new GameDetail() { Id = 7, GameId = 4, PlayerId = 1, IsLooser = true, Team = 1, Army = Common.Constants.Goblins };
                var GameDetails2Game4 = new GameDetail() { Id = 8, GameId = 4, PlayerId = 2, IsLooser = true, Team = 1, Army = Common.Constants.Goblins };
                var GameDetails3Game4 = new GameDetail() { Id = 9, GameId = 4, PlayerId = 3, IsLooser = false, Team = 2, Army = Common.Constants.Goblins };
                var GameDetails4Game4 = new GameDetail() { Id = 10, GameId = 4, PlayerId = 4, IsLooser = false, Team = 2, Army = Common.Constants.Goblins };


                var gamesCaseOne = new List<Game>() { game1, game2, game3, game4 };
                var gameDetailsCaseOne = new List<GameDetail>() { GameDetails1Game1, GameDetails2Game1, GameDetails1Game2, GameDetails2Game2, GameDetails1Game3, GameDetails2Game3,
                    GameDetails1Game4, GameDetails2Game4, GameDetails3Game4, GameDetails4Game4};

                yield return new TestCaseData(gamesCaseOne, gameDetailsCaseOne);
            }
        }

        [Test, TestCaseSource(nameof(GetOverallArmiesStatsTestCases))]
        public async Task GetOverallArmiesStatsTests(IList<Game> games, IList<GameDetail> gameDetails, double expectedWinRate)
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();

            #region DbArrange
            var dbMapId = 1;
            var map = new Map() { Id = dbMapId, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };

            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "One", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "Two", UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };

            var mapsList = new List<Map>() { map };
            var users = new List<User>() { userOne, userTwo };

            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(games.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(gameDetails.ToArray());

            #endregion

            var configMock = new Mock<IConfigProvider>();
            configMock.Setup(x => x.GetRotwkPatchesSetting()).Returns(new List<RotwkPatch>()
            {
                new RotwkPatch(){ StartDate = new DateTime(2020, 1, 1), Version = "8.3" },
                new RotwkPatch(){ StartDate = new DateTime(2020, 8, 1), Version = "8.4" }
            });

            var sut = new StatisticsService(dbWrapper._DbContext, configMock.Object);

            //Act
            var result = sut.GetArmiesStatsInIndividualGames();

            //Assert
            Assert.AreEqual(expectedWinRate, result.First(x => x.Army == Constants.Elves).WinPercentage);
        }

        

        [Test, TestCaseSource(nameof(PlayerCardTestData))]
        public async Task PlayerCardTests_OnCorrectData(IList<Game> games, IList<GameDetail> gameDetails)
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            const string statsPlayerName = "One";
            const string twoPlayerName = "Two";
            const string threePlayerName = "Three";

            #region DbArrange
            var dbMapId = 1;
            var map = new Map() { Id = dbMapId, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var userThree = new User() { Id = 3, Email = "playerThree@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var userFour = new User() { Id = 4, Email = "playerFour@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };

            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = statsPlayerName, UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = twoPlayerName, UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 3, Name = threePlayerName, UserId = userThree.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 4, Name = "Four", UserId = userFour.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };

            var mapsList = new List<Map>() { map };
            var users = new List<User>() { userOne, userTwo, userThree, userFour };

            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(games.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(gameDetails.ToArray());

            #endregion

            var configMock = new Mock<IConfigProvider>();
            configMock.Setup(x => x.GetRotwkPatchesSetting()).Returns(new List<RotwkPatch>()
            {
                new RotwkPatch(){ StartDate = new DateTime(2020, 1, 1), Version = "8.3" },
                new RotwkPatch(){ StartDate = new DateTime(2020, 8, 1), Version = "8.4" }
            });

            var sut = new StatisticsService(dbWrapper._DbContext, configMock.Object);

            //Act
            var result = await sut.GetPlayerCard(statsPlayerName);

            //Assert
            Assert.AreEqual(1, result.PermaIndividualWinsCount);
            Assert.AreEqual(2, result.PermaIndividualLosesCount);
            Assert.AreEqual(0, result.PermaTeamWinsCount);
            Assert.AreEqual(1, result.PermaTeamLosesCount);
            Assert.AreEqual(1, result.PlayerOpponents.Where(x => x.PlayerName == twoPlayerName).Count());
            Assert.AreEqual(1, result.PlayerOpponents.Where(x => x.PlayerName == threePlayerName).Count());
            Assert.AreEqual(2, result.PlayerOpponents.FirstOrDefault(x => x.PlayerName == twoPlayerName).LostTo);
            Assert.AreEqual(1, result.PlayerOpponents.FirstOrDefault(x => x.PlayerName == threePlayerName).WonAgainst);
        }

        [Test]
        public async Task PlayerCardTest_Given_NullPlayerNameParam_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            const string statsPlayerName = "One";
            const string twoPlayerName = "Two";
            const string threePlayerName = "Three";

            #region DbArrange
            var dbMapId = 1;
            var map = new Map() { Id = dbMapId, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var userThree = new User() { Id = 3, Email = "playerThree@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var userFour = new User() { Id = 4, Email = "playerFour@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };

            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = statsPlayerName, UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = twoPlayerName, UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 3, Name = threePlayerName, UserId = userThree.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 4, Name = "Four", UserId = userFour.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };

            var mapsList = new List<Map>() { map };
            var users = new List<User>() { userOne, userTwo, userThree, userFour };

            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());

            #endregion

            var configMock = new Mock<IConfigProvider>();
            configMock.Setup(x => x.GetRotwkPatchesSetting()).Returns(new List<RotwkPatch>()
            {
                new RotwkPatch(){ StartDate = new DateTime(2020, 1, 1), Version = "8.3" },
                new RotwkPatch(){ StartDate = new DateTime(2020, 8, 1), Version = "8.4" }
            });

            var sut = new StatisticsService(dbWrapper._DbContext, configMock.Object);

            //Act & Assert
            var ex = Assert.ThrowsAsync<ArgumentException>(() => sut.GetPlayerCard(null));
            Assert.AreEqual($"Player name param was not provided.", ex.Message);
        }

        [Test]
        public async Task PlayerCardTest_Given_NullDisplayName_Returns_InGameName()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            const string statsPlayerName = "One";
            const string nameParam = statsPlayerName;

            #region DbArrange
            var dbMapId = 1;
            var map = new Map() { Id = dbMapId, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };

            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = statsPlayerName, DisplayName = null, UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };

            var mapsList = new List<Map>() { map };
            var users = new List<User>() { userOne };

            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());

            #endregion

            var configMock = new Mock<IConfigProvider>();
            configMock.Setup(x => x.GetRotwkPatchesSetting()).Returns(new List<RotwkPatch>()
            {
                new RotwkPatch(){ StartDate = new DateTime(2020, 1, 1), Version = "8.3" },
                new RotwkPatch(){ StartDate = new DateTime(2020, 8, 1), Version = "8.4" }
            });

            var sut = new StatisticsService(dbWrapper._DbContext, configMock.Object);

            //Act & Assert
            var result =  await sut.GetPlayerCard(nameParam);
            
            Assert.IsNotNull(result);
            Assert.AreEqual(nameParam, result.PlayerName);
        }

        [Test]
        public async Task PlayerCardTest_Given_DisplayName_Returns_DisplayName()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            const string statsPlayerName = "One";
            const string nameParam = "DisplayNameLong454454545445";

            #region DbArrange
            var dbMapId = 1;
            var map = new Map() { Id = dbMapId, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };

            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = statsPlayerName, DisplayName = nameParam, UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };

            var mapsList = new List<Map>() { map };
            var users = new List<User>() { userOne };

            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());

            #endregion

            var configMock = new Mock<IConfigProvider>();
            configMock.Setup(x => x.GetRotwkPatchesSetting()).Returns(new List<RotwkPatch>()
            {
                new RotwkPatch(){ StartDate = new DateTime(2020, 1, 1), Version = "8.3" },
                new RotwkPatch(){ StartDate = new DateTime(2020, 8, 1), Version = "8.4" }
            });

            var sut = new StatisticsService(dbWrapper._DbContext, configMock.Object);

            //Act & Assert
            var result = await sut.GetPlayerCard(nameParam);

            Assert.IsNotNull(result);
            Assert.AreEqual(nameParam, result.PlayerName);
        }

        [Test]
        public async Task PlayerCardTest_Given_NonExistingPlayerNameParam_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            const string statsPlayerName = "One";
            const string twoPlayerName = "Two";
            const string threePlayerName = "Three";

            #region DbArrange
            var dbMapId = 1;
            var map = new Map() { Id = dbMapId, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var userThree = new User() { Id = 3, Email = "playerThree@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var userFour = new User() { Id = 4, Email = "playerFour@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };

            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = statsPlayerName, UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = twoPlayerName, UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 3, Name = threePlayerName, UserId = userThree.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 4, Name = "Four", UserId = userFour.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };

            var mapsList = new List<Map>() { map };
            var users = new List<User>() { userOne, userTwo, userThree, userFour };

            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());

            #endregion

            var configMock = new Mock<IConfigProvider>();
            configMock.Setup(x => x.GetRotwkPatchesSetting()).Returns(new List<RotwkPatch>()
            {
                new RotwkPatch(){ StartDate = new DateTime(2020, 1, 1), Version = "8.3" },
                new RotwkPatch(){ StartDate = new DateTime(2020, 8, 1), Version = "8.4" }
            });

            var paramPlayer = "NonExistingPlayer";
            var sut = new StatisticsService(dbWrapper._DbContext, configMock.Object);

            //Act & Assert
            var ex = Assert.ThrowsAsync<KeyNotFoundException>(() => sut.GetPlayerCard(paramPlayer));
            Assert.AreEqual($"Player {paramPlayer} does not exist.", ex.Message);
        }






        //[Test, TestCaseSource(nameof(TestCases))]
        //public async Task One(IList<Game> games, IList<GameDetail> gameDetails, double expectedWinRate)
        //{
        //    //Arrange
        //    using var dbWrapper = InmemoryDbWrapper.GetWrapper();

        //    #region DbArrange
        //    var dbMapId = 1;
        //    var map = new Map() { Id = dbMapId, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
        //    var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
        //    var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };

        //    var players = new List<Player>()
        //    {
        //        new Player() { Id = 1, Name = "One", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
        //        new Player() { Id = 2, Name = "Two", UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
        //    };

        //    var mapsList = new List<Map>() { map };
        //    var users = new List<User>() { userOne, userTwo };

        //    await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
        //    await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
        //    await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
        //    await dbWrapper.PopulateDbWithDataAsync(games.ToArray());
        //    await dbWrapper.PopulateDbWithDataAsync(gameDetails.ToArray());

        //    #endregion

        //    var configMock = new Mock<IConfigProvider>();
        //    configMock.Setup(x => x.GetRotwkPatchesSetting()).Returns(new List<RotwkPatch>()
        //    {
        //        new RotwkPatch(){ StartDate = new DateTime(2020, 1, 1), Version = "8.3" },
        //        new RotwkPatch(){ StartDate = new DateTime(2020, 8, 1), Version = "8.4" }
        //    });

        //    var sut = new StatisticsService(dbWrapper.DbContext, configMock.Object);

        //    //Act
        //    var date = new DateTime(2018, 1, 1);
        //    //var result = await sut.GetMonthActivityStats(date);

        //}
    }
}
