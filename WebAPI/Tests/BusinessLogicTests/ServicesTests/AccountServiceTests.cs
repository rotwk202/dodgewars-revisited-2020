﻿using BusinessLogic.PointsSystem;
using BusinessLogic.Services;
using Common.Model;
using Common.Services;
using Common.Tools;
using DALMySql.POCO;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Tests.BusinessLogicTests.ServicesTests
{
    [TestFixture]
    public class AccountServiceTests
    {
        [Test]
        public async Task Given_ExistingLoginExistingNickname_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var login = "playerOne@gmail.com";
            var nicknameRequest = "One";

            var userOne = new User() { Id = 1, Email = login, Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = nicknameRequest, UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "One1", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.ThrowsAsync<InvalidOperationException>(() => sut.CreatePlayer(login, nicknameRequest, ""));
            Assert.AreEqual($"In game name {nicknameRequest} is already in use", ex.Message);
        }

        [Test]
        public async Task Given_NonExistingLoginNonExistingNickname_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var login = "playerOne@gmail.com";
            var nicknameRequest = "One";

            var userOne = new User() { Id = 1, Email = "OtherLogin", Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "Other1", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "Other2", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.ThrowsAsync<InvalidOperationException>(() => sut.CreatePlayer(login, nicknameRequest, ""));
            Assert.AreEqual($"User of given email does not exist.", ex.Message);
        }

        [Test]
        public async Task Given_ExistingLoginAndTooLongNickname_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var login = "playerOne@gmail.com";
            var nicknameRequest = "OverTenCharactersNickname";
            var maximumPlayerNameLength = 10;

            var userOne = new User() { Id = 1, Email = login, Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "Another", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "One1", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.ThrowsAsync<InvalidOperationException>(() => sut.CreatePlayer(login, nicknameRequest, ""));
            Assert.AreEqual($"Given in game name {nicknameRequest} is too long. Maximum in game name length is {maximumPlayerNameLength}", ex.Message);
        }

        [Test]
        public async Task Given_CorrectInputData_CreatesPlayerCorrectly()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var login = "playerOne@gmail.com";
            var nicknameRequest = "Two";

            var userOne = new User() { Id = 1, Email = login, Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "One", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "One1", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act
            var result = await sut.CreatePlayer(login, nicknameRequest, "");
            var nicknameCreated = dbWrapper._DbContext.Player.AsQueryable().FirstOrDefault(x => x.Name == nicknameRequest);

            //Assert
            Assert.IsTrue(result);
            Assert.IsNotNull(nicknameCreated);
            Assert.AreEqual(nicknameRequest, nicknameCreated.Name);
            Assert.AreEqual(Constants.InitialRatingValue, nicknameCreated.Rating);
            Assert.AreEqual(Constants.InitialRatingValue, nicknameCreated.TeamRating);
            Assert.AreEqual(Constants.InitialMonthlyRatingValue, nicknameCreated.MonthlyRating);
            Assert.AreEqual(Constants.InitialMonthlyRatingValue, nicknameCreated.MonthlyTeamRating);
        }

        [Test]
        public async Task Given_ExistingLoginOnRegister_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";
            var clientName = "web";
            var apiUser = new ApiUser() { Email = existingLogin, Password = "aaa", InitialNickname = "nickname" };

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "AnotherLogin", Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.ThrowsAsync<InvalidOperationException>(() => sut.RegisterUser(apiUser, clientName));
            Assert.AreEqual($"User with such e-mail already exists.", ex.Message);
        }

        [Test]
        public async Task Given_EmptyPasswordOnRegister_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";
            var clientName = "web";
            var apiUser = new ApiUser() { Email = "abc", Password = "", InitialNickname = "nickname" };

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "AnotherLogin", Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.ThrowsAsync<ArgumentException>(() => sut.RegisterUser(apiUser, clientName));
            Assert.AreEqual($"Provided password either didn't exist or was empty.", ex.Message);
        }

        [Test]
        public async Task Given_NullPasswordOnRegister_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";
            var clientName = "web";
            var apiUser = new ApiUser() { Email = "abc", Password = null, InitialNickname = "nickname" };

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "AnotherLogin", Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.ThrowsAsync<ArgumentException>(() => sut.RegisterUser(apiUser, clientName));
            Assert.AreEqual($"Provided password either didn't exist or was empty.", ex.Message);
        }

        [Test]
        public async Task Given_EmptyUserNameOnGetUserName_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";
            var testPlayerName = string.Empty;

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "AnotherLogin", Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            var players = new List<Player>()
            {
                new Player(){ Id = 1, UserId = 1, Name = "One", Rating = 1000, MonthlyRating = 1000, TeamRating = 1000, MonthlyTeamRating = 1000 },
                new Player(){ Id = 2, UserId = 1, Name = "Two", Rating = 1000, MonthlyRating = 1000, TeamRating = 1000, MonthlyTeamRating = 1000 },
            };

            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.Throws<ArgumentException>(() => sut.GetUserName(testPlayerName));
            Assert.AreEqual($"Provided user name was incorrect.", ex.Message);
        }

        [Test]
        public async Task Given_NullUserNameOnGetUserName_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";
            string testPlayerName = null;

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "AnotherLogin", Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            var players = new List<Player>()
            {
                new Player(){ Id = 1, UserId = 1, Name = "One", Rating = 1000, MonthlyRating = 1000, TeamRating = 1000, MonthlyTeamRating = 1000 },
                new Player(){ Id = 2, UserId = 1, Name = "Two", Rating = 1000, MonthlyRating = 1000, TeamRating = 1000, MonthlyTeamRating = 1000 },
            };

            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.Throws<ArgumentException>(() => sut.GetUserName(testPlayerName));
            Assert.AreEqual($"Provided user name was incorrect.", ex.Message);
        }

        [Test]
        public async Task Given_NonExistingPlayerName_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";
            string testPlayerName = "Three";

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "AnotherLogin", Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            var players = new List<Player>()
            {
                new Player(){ Id = 1, UserId = 1, Name = "One", Rating = 1000, MonthlyRating = 1000, TeamRating = 1000, MonthlyTeamRating = 1000 },
                new Player(){ Id = 2, UserId = 1, Name = "Two", Rating = 1000, MonthlyRating = 1000, TeamRating = 1000, MonthlyTeamRating = 1000 },
            };

            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());

            var frsMock = new Mock<FailedReportService>(dbWrapper._DbContext);
            var sut = new AccountService(dbWrapper._DbContext, frsMock.Object);

            //Act & Assert
            var ex = Assert.Throws<KeyNotFoundException>(() => sut.GetUserName(testPlayerName));
            Assert.AreEqual($"Player of given name {testPlayerName} was not found.", ex.Message);
        }

        [Test]
        public async Task Given_CorrectPlayerName_Returns_Login()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";
            string testPlayerName = "One";

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "AnotherLogin", Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            var players = new List<Player>()
            {
                new Player(){ Id = 1, UserId = 1, Name = "One", Rating = 1000, MonthlyRating = 1000, TeamRating = 1000, MonthlyTeamRating = 1000 },
                new Player(){ Id = 2, UserId = 1, Name = "Two", Rating = 1000, MonthlyRating = 1000, TeamRating = 1000, MonthlyTeamRating = 1000 },
            };

            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var result = sut.GetUserName(testPlayerName);
            Assert.AreEqual(existingLogin, result);
        }

        [Test]
        public async Task Given_NullUserOnResetPassword_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "47BCE5C74F589F4867DBD57E9CA9F808", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.ThrowsAsync<ArgumentException>(() => sut.ResetPassword(null));
            Assert.AreEqual("User to update was no provided.", ex.Message);

        }

        [Test]
        public async Task Given_AdminUserOnResetPassword_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "47BCE5C74F589F4867DBD57E9CA9F808", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);
            var apiUser = new ApiUser() { Email = AccountService.SecretAdmin, Password = "aaa" };

            //Act & Assert
            var ex = Assert.ThrowsAsync<InvalidOperationException>(() => sut.ResetPassword(apiUser));
            Assert.AreEqual("You're not allowed to change the password of this user.", ex.Message);
        }

        [Test]
        public async Task Given_NullUserLoginOnResetPassword_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";
            var apiUser = new ApiUser() { Email = null, Password = "aaa" };

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "47BCE5C74F589F4867DBD57E9CA9F808", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.ThrowsAsync<ArgumentException>(() => sut.ResetPassword(apiUser));
            Assert.AreEqual("User login was not provided.", ex.Message);
        }

        [Test]
        public async Task Given_NullPasswordOnResetPassword_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";
            var apiUser = new ApiUser() { Email = existingLogin, Password = null };

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "47BCE5C74F589F4867DBD57E9CA9F808", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.ThrowsAsync<ArgumentException>(() => sut.ResetPassword(apiUser));
            Assert.AreEqual("Reset password was not provided.", ex.Message);
        }

        [Test]
        public async Task Given_NonExistingLoginOnResetPassword_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";
            var apiUser = new ApiUser() { Email = "OtherLogin", Password = "aaa" };

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "47BCE5C74F589F4867DBD57E9CA9F808", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.ThrowsAsync<KeyNotFoundException>(() => sut.ResetPassword(apiUser));
            Assert.AreEqual("User with given user name does not exist.", ex.Message);
        }

        [Test]
        public async Task Given_CorrectDataOnResetPassword_UpdatesUserCorrectly()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingLogin = "playerOne@gmail.com";
            var apiUser = new ApiUser() { Email = existingLogin, Password = "bbb" };

            var userOne = new User() { Id = 1, Email = existingLogin, Password = "47BCE5C74F589F4867DBD57E9CA9F808", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var result = sut.ResetPassword(apiUser);
            Assert.AreEqual(PasswordEncryptor.CreateMD5(apiUser.Password),
                dbWrapper._DbContext.User.AsQueryable().FirstOrDefault(x => x.Email == apiUser.Email).Password);
        }

        [Test]
        public async Task Given_CorrectAdminPassword_ReturnsTrue()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var userAdminName = AccountService.SecretAdmin;
            var secretPassword = "SecretPassword";

            var userOne = new User() { Id = 1, Email = userAdminName, Password = secretPassword, CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act
            var result = await sut.IsValidAdminPwd(secretPassword);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public async Task IsValidAdminPwd_DoesNotUpdate_AdminUserPassword()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var userAdminName = AccountService.SecretAdmin;
            var secretPassword = "SecretPassword";

            var userOne = new User() { Id = 1, Email = userAdminName, Password = secretPassword, CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };

            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act
            var result = await sut.IsValidAdminPwd(secretPassword);
            var dbUser = await dbWrapper._DbContext.User.AsQueryable().FirstOrDefaultAsync(x => x.Email == userAdminName);

            //Assert
            Assert.IsNotNull(dbUser);
            Assert.AreEqual(userAdminName, dbUser.Email);
            Assert.AreEqual(secretPassword, dbUser.Password);
        }

        [Test]
        public async Task IsValidAdminPwd_ThrowsException_When_SecretAdminUserDoesNotExist()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var secretPassword = "SecretPassword";

            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act & Assert
            var ex = Assert.ThrowsAsync<KeyNotFoundException>(() => sut.IsValidAdminPwd(secretPassword));
            Assert.AreEqual($"Admin token not defined. Please create user {AccountService.SecretAdmin} in the database.", ex.Message);
        }

        [Test]
        public async Task Given_NonInitializedDisplayName_Sets_InReplayName()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var login = "playerOne@gmail.com";

            var userOne = new User() { Id = 1, Email = login, Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "Abc", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "Def", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act
            var result = await sut.GetPlayerNamesDisplayNames(login);

            //Assert
            Assert.AreEqual(players.Count, result.Count);
            Assert.AreEqual("Abc", result[0].DisplayName);
            Assert.AreEqual("Def", result[1].DisplayName);
        }

        [Test]
        public async Task Given_InitializedDisplayName_Returns_DisplayName()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var login = "playerOne@gmail.com";

            var userOne = new User() { Id = 1, Email = login, Password = "aaa", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne };
            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "Abc", DisplayName = "qwerty", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "Def", DisplayName = "ytrewq", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            var sut = new AccountService(dbWrapper._DbContext, null);

            //Act
            var result = await sut.GetPlayerNamesDisplayNames(login);

            //Assert
            Assert.AreEqual(players.Count, result.Count);
            Assert.AreEqual("qwerty", result[0].DisplayName);
            Assert.AreEqual("ytrewq", result[1].DisplayName);
        }

    }
}
