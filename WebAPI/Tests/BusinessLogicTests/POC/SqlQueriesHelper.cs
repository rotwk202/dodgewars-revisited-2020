﻿

using Microsoft.Data.SqlClient;

namespace Tests.BusinessLogicTests.POC
{
    public static class SqlQueriesHelper
    {
        public static void CreateEmptyDb(string connectionString)
        {
            var connStringBuilder = new SqlConnectionStringBuilder(connectionString);
            var dbName = connStringBuilder.InitialCatalog;
            connStringBuilder.InitialCatalog = "master";
            using SqlConnection myConn = new SqlConnection(connStringBuilder.ConnectionString);

            var sql = $@"CREATE DATABASE {dbName} ON PRIMARY 
             (NAME = {dbName}_Data,
             FILENAME = 'C:\\{dbName}Data.mdf', 
             SIZE = 20MB, MAXSIZE = 100000MB, FILEGROWTH = 10%)
             LOG ON (NAME = {dbName}_Log,
             FILENAME = 'C:\\{dbName}Log.ldf',
             SIZE = 100MB,
             MAXSIZE = 100000MB,
             FILEGROWTH = 10%)";

            using SqlCommand myCommand = new SqlCommand(sql, myConn);

            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
            }
            catch (System.Exception ex)
            {

            }
        }
    }
}
