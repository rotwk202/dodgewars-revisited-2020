﻿using BusinessLogic.POC;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using BusinessLogic.Services;
using BusinessLogic.Validators;
using Common.Interfaces;
using Common.Model;
using Common.Services;
using DALMySql.POCO;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using Microsoft.Data.SqlClient;
using Dapper;
using System.Reflection;
using System.IO;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace Tests.BusinessLogicTests.POC
{
    [TestFixture]
    public class POCServiceTests : IntegrationTestBase
    {
        [Test]
        public async Task SimulateNGames()
        {
            //Arrange
            var sut = new POCService();
            using var dbWrapper = DbWrapper.GetWrapper();

            #region DbArrange
            //run local test server (api) - base class
            //run migrations - base class
            //insert users, players and maps into database

            //using var conn = new SqlConnection("Data Source=localhost,7878;Initial Catalog=ABCDEMyDatabase;User ID=SA;Password=yourStrong(!)Password;Encrypt=false;");
            using var conn = new MySqlConnection("server=192.168.0.105,3306;database=RotwkLeague_6000games;user id=root;password=my-secret-pw");
            var execAssemblyPath = Assembly.GetExecutingAssembly().Location;
            DirectoryInfo testDirInfo = new DirectoryInfo(execAssemblyPath);
            DirectoryInfo scriptsDirectoryInfo = testDirInfo.Parent.Parent.Parent.Parent;
            //var scriptsDirectory = Path.Combine(scriptsDirectoryInfo.FullName, "BusinessLogicTests", "POC", "DummyInitialData");
            var scriptsDirectory = Path.Combine(scriptsDirectoryInfo.FullName, "BusinessLogicTests", "POC", "DummyInitialDataMySql");
            var filePaths = Directory.GetFiles(scriptsDirectory, "*.sql");
            var dbName = conn.Database;

            foreach (var filePath in filePaths)
            {
                try
                {
                    var sql = File.ReadAllText(filePath);
                    sql = sql.Replace("USE_PLACEHOLDER", $"USE {dbName};");
                    conn.Execute(sql);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
            #endregion

            //var dbPlayers = conn.Query<Player>("select * from [Player]").ToList();
            var dbPlayers = conn.Query<Player>($"use {dbName}; select * from Player").ToList();
            var configMock = new Mock<IConfigProvider>();
            var providerMock = new Mock<ISqlConnectionProvider>();
            configMock.Setup(x => x.GetAllowUnknownMapsSetting()).Returns(true);
            var validator = new GameValidator(configMock.Object);
            var reportService = new GameReportService(dbWrapper.DbContext, validator, null, null, providerMock.Object);

            //Act
            Console.WriteLine("season 1 start simulation");
            sut.SimulateNGames(dbPlayers, reportService, 6000, new DateTime(2022, 1, 1), new DateTime(2022, 3, 31), 100);
            Console.WriteLine("season 1 simulation complete");
            Console.WriteLine("season 2 start simulation");
            sut.SimulateNGames(dbPlayers, reportService, 6000, new DateTime(2022, 4, 1), new DateTime(2022, 6, 30), 100);
            Console.WriteLine("season 2 simulation complete");
            Console.WriteLine("season 3 start simulation");
            sut.SimulateNGames(dbPlayers, reportService, 6000, new DateTime(2022, 7, 1), new DateTime(2022, 9, 30), 100);
            Console.WriteLine("season 3 simulation complete");
            Console.WriteLine("season 4 start simulation");
            sut.SimulateNGames(dbPlayers, reportService, 6000, new DateTime(2022, 10, 1), new DateTime(2022, 12, 31), 100);
            Console.WriteLine("season 4 simulation complete");
        }

        public List<User> GetAutofixture100Users()
        {
            Console.WriteLine("console test");
            var userFixture = new Fixture();
            int howMany = 3;
            userFixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            userFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            int startId = 1;
            userFixture.Customize<User>(ob => ob
                //.Without(t => t.TransactionId)
                .Do(t => t.Id = startId++)
                .With(t => t.Password, "12111")
                //.OmitAutoProperties()

                );

            var myFixture = new Fixture();
            myFixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            myFixture.Behaviors.Add(new OmitOnRecursionBehavior());
            
            var resultUsers = myFixture
                .Build<User>()
                .Without(user => user.Players)
                .Without(user => user.Id)
                .Do(user =>
                {
                    var playerFixture = new Fixture();
                    playerFixture.Behaviors.Remove(new ThrowingRecursionBehavior());
                    playerFixture.Behaviors.Add(new OmitOnRecursionBehavior());
                    var players = playerFixture
                        .Build<Player>()
                        .Without(p => p.MonthlyRatings)
                        .Without(p => p.Ratings)
                        .Without(p => p.GameDetails)
                        .With(p => p.Rating, 1600)
                        .With(p => p.TeamRating, 1600)
                        .With(p => p.MonthlyRating, 1600)
                        .With(p => p.MonthlyTeamRating, 1600)
                        .With(p => p.UserId, user.Id)
                        .CreateMany<Player>(2).ToList();

                    user.Id = startId++;
                    user.Players = players;
                })
                .CreateMany<User>(howMany).ToList();

            return resultUsers;

            //return null;
        }

        //public Player GetRandomPlayer()
        //{
        //    return new Player() { Id = 2, Name = "Two", UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
        //}

    }
}
