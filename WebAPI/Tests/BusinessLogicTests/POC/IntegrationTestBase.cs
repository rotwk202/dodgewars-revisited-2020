﻿using Api;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Tests.BusinessLogicTests.POC
{
    public class IntegrationTestBase
    {
        protected TestServer _testServer;
        //protected HttpClient _testClient;
        private static Random random = new Random();
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        //private readonly string _databaseName = "A" + new string(Enumerable.Repeat(chars, 10).Select(s => s[random.Next(s.Length)]).ToArray());
        private readonly string _databaseName = "ABCDEMyDatabase";
        protected string _connectionString;
        private string _dockerContainerId;
        private string _dockerSqlPort;
        public const string DATABASE_NAME_PLACEHOLDER = "@@databaseName@@";
        protected IConfiguration _testServerConfiguration;

        [OneTimeSetUp]
        public async Task InitializeAsync()
        {
            //create container with sql server
            //await InitializeDockerAsync();
            //_connectionString = GetSqlConnectionString().Replace(DATABASE_NAME_PLACEHOLDER, _databaseName);

            /////create empty database using new conn string
            //SqlQueriesHelper.CreateEmptyDb(_connectionString);

            //initialize test api
            await InitializeTestServer();
        }

        private async Task InitializeTestServer()
        {
            if (_testServer != null)
            {
                return;
            }

            //AddOrUpdateAppSetting("ConnectionStrings:Default", _connectionString, "appsettings.gamesSimulation.json");

            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.gamesSimulation.json", true, reloadOnChange: true);
            _testServerConfiguration = builder.Build();
            _testServerConfiguration["ConnectionStrings:Default"] = _connectionString;
            _testServerConfiguration["IsSimulation"] = "5555";
            

            var application = new WebApplicationFactory<Api.Program>()
               .WithWebHostBuilder(builder =>
               {
                   builder
                    .UseStartup<Startup>()
                    .UseEnvironment("gamesSimulation")
                    .UseConfiguration(_testServerConfiguration);
               });

            _testServer = application.Server;
        }

        public static void AddOrUpdateAppSetting<T>(string key, T value, string settingsFileName)
        {
            try
            {
                var filePath = Path.Combine(AppContext.BaseDirectory, settingsFileName);
                string json = File.ReadAllText(filePath);

                var enviroment = System.Environment.CurrentDirectory;
                string projectDirectory = Directory.GetParent(enviroment).Parent.FullName;

                dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);

                var sectionPath = key.Split(":")[0];

                if (!string.IsNullOrEmpty(sectionPath))
                {
                    var keyPath = key.Split(":")[1];
                    jsonObj[sectionPath][keyPath] = value;
                }
                else
                {
                    jsonObj[sectionPath] = value; // if no sectionpath just set the value
                }

                string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(filePath, output);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
        }

        private string GetSqlConnectionString()
        {
            return $"Data Source=localhost,{_dockerSqlPort};" +
                $"Initial Catalog={DATABASE_NAME_PLACEHOLDER};" +
                "Integrated Security=False;" +
                "User ID=SA;" +
                $"Password={DockerSqlDatabaseUtilities.SQLSERVER_SA_PASSWORD};Encrypt=false;";
        }

        private async Task InitializeDockerAsync()
        {
            (_dockerContainerId, _dockerSqlPort) = await DockerSqlDatabaseUtilities.EnsureDockerStartedAndGetContainerIdAndPortAsync();
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await DockerSqlDatabaseUtilities.EnsureDockerStoppedAndRemovedAsync(_dockerContainerId);
        }


        public void Dispose()
        {
            _testServer?.Dispose();
        }
    }
}
