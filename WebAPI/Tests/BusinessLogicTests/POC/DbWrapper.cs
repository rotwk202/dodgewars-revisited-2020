﻿using DALMySql;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Threading.Tasks;

namespace Tests
{
    public class DbWrapper : IDisposable
    {
        public LadderContext DbContext { get; set; }
        protected DbWrapper()
        {
            DbContextOptions<LadderContext> options = BuildConnectionOptions();
            DbContext = new LadderContext(options);
            DbContext.Database.EnsureCreated();
        }
        public static DbWrapper GetWrapper()
        {
            var wrapper = new DbWrapper();

            return wrapper;
        }

        private static DbContextOptions<LadderContext> BuildConnectionOptions()
        {
            //var connectionString = "Data Source=localhost,7878;Initial Catalog=ABCDEMyDatabase;User ID=SA;Password=yourStrong(!)Password;Encrypt=false;";
            var connectionString = "server=192.168.0.105,3306;database=RotwkLeague_6000games;user id=root;password=my-secret-pw";

            //var connection = new SqliteConnection(connectionString);
            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var builder = new DbContextOptionsBuilder<LadderContext>();
            //builder.UseSqlServer(connectionString);
            builder.UseMySQL(connectionString);

            var options = builder.Options;
            return options;
        }

        public async Task PopulateDbWithDataAsync(object[] testData)
        {

            await DbContext.AddRangeAsync(testData);
            await DbContext.SaveChangesAsync();
        }

        public void MigrateUp()
        {
            
            DbContext.Database.Migrate();
        }

        public string GetConnString()
        {
            using var conn = DbContext.Database.GetDbConnection();
            conn.Open();
            var res = conn.Query("select * [User]");


            return null;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    DbContext.Dispose();
                }

                disposedValue = true;
            }
        }


        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
