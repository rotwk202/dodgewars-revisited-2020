﻿using BusinessLogic.POC;
using DALMySql.POCO;
using Dapper;
using Microsoft.Data.SqlClient;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Z.Dapper.Plus;

namespace Tests.BusinessLogicTests.POC
{
    [TestFixture]
    public class RunCode
    {
        [Test]
        public void ReplaceEmails()
        {
            using var conn = new SqlConnection("Data Source=.;Initial Catalog=rotwkladderadmin_RotwkLeague;User Id=sa;Password=lokalny123");

            var users = conn.Query<User>("select * from [User]");
            foreach (var user in users)
            {
                user.Email = Faker.Name.FullName().Replace(" ", "");
                user.Password = "";
            }

            var players = conn.Query<Player>("select * from [Player]");
            foreach (var player in players)
            {
                var newName = Faker.Name.FullName().Replace(" ", "");
                player.Name = newName.Length > 10 ? newName.Substring(0, 10) : newName;
                player.DisplayName = newName;
            }

            conn.BulkUpdate(users);
            conn.BulkUpdate(players);
        }

        [Test]
        public void Test1()
        {
            //Trace.WriteLine("test trace");
            //Debug.WriteLine("test debug");
            //TestContext.WriteLine("test context");
            //Console.WriteLine("test console");

            Assert.Pass();
        }

    }
}
