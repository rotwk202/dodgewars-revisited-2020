﻿using BusinessLogic.PointsSystem;
using Common.Model;
using DALMySql.POCO;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.BusinessLogicTests.PointsSystemTests
{
    [TestFixture]
    public class TeamRatingCalculatorTests
    {
        [TestCase(
            1400, 1450, 1100, 1120, 4, 4, -4, -4,  //inside lenience range, +5, -0
            1400, 1450, 1100, 1120, 5, 5, 0, 0)]

        [TestCase(
            1100, 1150, 1005, 1000, 11, 11, -11, -11,  //inside lenience range, +14, -0
            1100, 1150, 1005, 1000, 14, 14, 0, 0)]

        [TestCase(
            1500, 1500, 1500, 1500, 16, 16, -16, -16,  //outside out bonus range +16, -16
            1500, 1500, 1500, 1500, 16, 16, -16, -16)]

        [TestCase(
            1450, 1450, 1450, 1450, 16, 16, -16, -16,  //inside out bonus range +20, -16
            1450, 1450, 1450, 1450, 20, 20, -16, -16)]

        public void GetTeamRatingDeltasTests(
            int winner1Rating, int winner2Rating,
            int loser1Rating, int loser2Rating,
            int winner1ExpectedDelta, int winner2ExpectedDelta,
            int loser1ExpectedDelta, int loser2ExpectedDelta,
            int winner1RatingMonthly, int winner2RatingMonthly,
            int loser1RatingMonthly, int loser2RatingMonthly,
            int winner1ExpectedDeltaMonthly, int winner2ExpectedDeltaMonthly,
            int loser1ExpectedDeltaMonthly, int loser2ExpectedDeltaMonthly)
        {
            //Arrange
            var dbPlayers = new List<Player>
            {
                new Player() { Name = "Winner1", Id = 1, TeamRating = winner1Rating, MonthlyTeamRating =  winner1RatingMonthly },
                new Player() { Name = "Winner2", Id = 2, TeamRating = winner2Rating, MonthlyTeamRating = winner2RatingMonthly },
                new Player() { Name = "Loser1", Id = 3, TeamRating = loser1Rating, MonthlyTeamRating = loser1RatingMonthly },
                new Player() { Name = "Loser2", Id = 4, TeamRating = loser2Rating, MonthlyTeamRating = loser2RatingMonthly }
            };

            var apiPlayers = new List<ApiPlayer>
            {
                new ApiPlayer() { Name = "Winner1", IsLooser = false },
                new ApiPlayer() { Name = "Winner2", IsLooser = false },
                new ApiPlayer() { Name = "Loser1", IsLooser = true },
                new ApiPlayer() { Name = "Loser2", IsLooser = true }
            };

            //Act
            var calculationResults = TeamRatingCalculator.GetTeamRatingsDeltas(dbPlayers, apiPlayers);
            var calculationResultsMonthly = TeamRatingCalculator.GetMonthlyTeamRatingsDeltas(dbPlayers, apiPlayers);

            //Assert
            Assert.AreEqual(winner1ExpectedDelta, calculationResults.Find(x => x.PlayerId == 1).Delta);
            Assert.AreEqual(winner2ExpectedDelta, calculationResults.Find(x => x.PlayerId == 2).Delta);
            Assert.AreEqual(loser1ExpectedDelta, calculationResults.Find(x => x.PlayerId == 3).Delta);
            Assert.AreEqual(loser2ExpectedDelta, calculationResults.Find(x => x.PlayerId == 4).Delta);

            Assert.AreEqual(winner1ExpectedDelta, calculationResults.Find(x => x.PlayerId == 1).Delta);
            Assert.AreEqual(winner2ExpectedDelta, calculationResults.Find(x => x.PlayerId == 2).Delta);
            Assert.AreEqual(loser1ExpectedDelta, calculationResults.Find(x => x.PlayerId == 3).Delta);
            Assert.AreEqual(loser2ExpectedDelta, calculationResults.Find(x => x.PlayerId == 4).Delta);

            //Assert
            Assert.AreEqual(winner1ExpectedDeltaMonthly, calculationResultsMonthly.Find(x => x.PlayerId == 1).Delta);
            Assert.AreEqual(winner2ExpectedDeltaMonthly, calculationResultsMonthly.Find(x => x.PlayerId == 2).Delta);
            Assert.AreEqual(loser1ExpectedDeltaMonthly, calculationResultsMonthly.Find(x => x.PlayerId == 3).Delta);
            Assert.AreEqual(loser2ExpectedDeltaMonthly, calculationResultsMonthly.Find(x => x.PlayerId == 4).Delta);

            Assert.AreEqual(winner1ExpectedDeltaMonthly, calculationResultsMonthly.Find(x => x.PlayerId == 1).Delta);
            Assert.AreEqual(winner2ExpectedDeltaMonthly, calculationResultsMonthly.Find(x => x.PlayerId == 2).Delta);
            Assert.AreEqual(loser1ExpectedDeltaMonthly, calculationResultsMonthly.Find(x => x.PlayerId == 3).Delta);
            Assert.AreEqual(loser2ExpectedDeltaMonthly, calculationResultsMonthly.Find(x => x.PlayerId == 4).Delta);
        }
    }
}
