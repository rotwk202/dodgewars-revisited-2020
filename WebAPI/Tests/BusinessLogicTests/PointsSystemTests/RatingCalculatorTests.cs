﻿using Common.Model;
using DALMySql.POCO;
using DodgeWars.BusinessLogic.Services;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests.BusinessLogicTests.PointsSystemTests
{
    [TestFixture]
    public class RatingCalculatorTests
    {
        [TestCase(              //+2, -0
            1500, 1020, 2, -2,
            1500, 1020, 2, 0)]

        [TestCase(              //+17, -0
            1035, 1000, 14, -14,
            1035, 1000, 17, 0)]

        [TestCase(              //+38, -31
            1010, 1800, 31, -31,
            1010, 1800, 38, -31)]

        [TestCase(              //+1, -1
            1800, 1201, 1, -1,
            1800, 1201, 1, -1)]

        [TestCase(              //+20, -3 -> can't go lower than 1200
            1200, 1203, 16, -16,
            1200, 1203, 20, -3)]

        //[TestCase(0, 0, 20, 0)]

        public void UpdateNicknamesIndividualRatingsTests(
            int winnerRating,
            int looserRating,
            int winnerExpectedDelta,
            int loserExpectedDelta,
            int winnerRatingMonthly,
            int looserRatingMonthly,
            int winnerExpectedDeltaMonthly,
            int loserExpectedDeltaMonthly)
        {
            //Arrange
            var dbNicknames = new List<Player>
            {
                new Player() { Name = "Loser", Id = 1, Rating = looserRating, MonthlyRating = looserRatingMonthly },
                new Player() { Name = "Winner", Id = 2, Rating = winnerRating, MonthlyRating = winnerRatingMonthly }
            };

            var apiPlayers = new List<ApiPlayer>
            {
                new ApiPlayer() { Name = "Loser", IsLooser = true },
                new ApiPlayer() { Name = "Winner", IsLooser = false }
            };

            //Act
            var calculationResults = RatingCalculator.GetIndividualRatingsDeltas(dbNicknames, apiPlayers);
            var calculationResultsMonthly = RatingCalculator.GetMonthlyIndividualRatingsDeltas(dbNicknames, apiPlayers);

            //Assert
            Assert.AreEqual(winnerExpectedDelta, calculationResults.Find(x => x.PlayerId == 2).Delta);
            Assert.AreEqual(loserExpectedDelta, calculationResults.Find(x => x.PlayerId == 1).Delta);

            //Assert for monthly
            Assert.AreEqual(winnerExpectedDeltaMonthly, calculationResultsMonthly.Find(x => x.PlayerId == 2).Delta);
            Assert.AreEqual(loserExpectedDeltaMonthly, calculationResultsMonthly.Find(x => x.PlayerId == 1).Delta);
        }
    }
}
