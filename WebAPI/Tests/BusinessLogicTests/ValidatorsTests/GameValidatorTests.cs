﻿using BusinessLogic.Validators;
using Common;
using Common.Interfaces;
using Common.Model;
using DALMySql.POCO;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Tests.BusinessLogicTests.ValidatorsTests
{
    [TestFixture]
    public class GameValidatorTests
    {

        [Test]
        public async Task Given_UnknownMapAndAllowUnknownMapsConfig_AllowsReportingGame()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var map = new Map() { Id = 1, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var game = new Game() { Id = 1, MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null, Identifier = "" };
            var mapsList = new List<Map>() { map };
            var gamesList = new List<Game>() { game };
            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(gamesList.ToArray());

            var apiGame = new ApiGame();
            apiGame.Players = new List<ApiPlayer>() { new ApiPlayer() };
            apiGame.ReplayMapTextId = "map mp argonath";
            apiGame.PlayedOn = DateTime.Now;

            var configMock = new Mock<IConfigProvider>();
            configMock.Setup(x => x.GetAllowUnknownMapsSetting()).Returns(true);
            var sut = new GameValidator(configMock.Object);

            //Act
            var isGameValid = sut.IsGameValid(dbWrapper._DbContext, apiGame);

            //Assert
            Assert.IsTrue(isGameValid);
            Assert.AreEqual(Constants.UnkownMapReplayMapTextId, apiGame.ReplayMapTextId);
        }

        [Test]
        public async Task Given_UnknownMapAndAllowUnknownMapsConfiSetToFalseg_RefusesGame()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var map = new Map() { Id = 1, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var game = new Game() { Id = 1, MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null, Identifier = "" };
            var mapsList = new List<Map>() { map };
            var gamesList = new List<Game>() { game };
            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(gamesList.ToArray());

            var apiGame = new ApiGame();
            apiGame.Players = new List<ApiPlayer>() { new ApiPlayer() };
            apiGame.ReplayMapTextId = "map mp argonath";
            apiGame.PlayedOn = DateTime.Now;

            var configMock = new Mock<IConfigProvider>();
            configMock.Setup(x => x.GetAllowUnknownMapsSetting()).Returns(false);
            var sut = new GameValidator(configMock.Object);

            //Act & Assert
            var ex = Assert.Throws<NullReferenceException>(() => sut.IsGameValid(dbWrapper._DbContext, apiGame));
            Assert.AreEqual($"Map with given ID {apiGame.ReplayMapTextId} was not found. You are unable to play games on this map.", ex.Message);
        }

        [Test]
        public async Task Given_InvalidGamePlayedOnProperty_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var map = new Map() { Id = 1, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var game = new Game() { Id = 1, MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null, Identifier = "" };
            var mapsList = new List<Map>() { map };
            var gamesList = new List<Game>() { game };
            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(gamesList.ToArray());

            var apiGame = new ApiGame();
            apiGame.Players = new List<ApiPlayer>() { new ApiPlayer() };
            apiGame.ReplayMapTextId = "map mp argonath";
            apiGame.PlayedOn = DateTime.MinValue;

            var configMock = new Mock<IConfigProvider>();
            configMock.Setup(x => x.GetAllowUnknownMapsSetting()).Returns(true);
            var sut = new GameValidator(configMock.Object);

            //Act & Assert
            var ex = Assert.Throws<ArgumentException>(() => sut.IsGameValid(dbWrapper._DbContext, apiGame));
            Assert.AreEqual($"PlayedOn property of game was not initialized.", ex.Message);
        }

        [Test]
        public async Task Given_GameThatWasAlreadyReported_ThrowsException()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            const string uniqeHash = "uniqeHash";
            var map = new Map() { Id = 1, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var game = new Game() { Id = 1, MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null, Identifier = uniqeHash };
            var mapsList = new List<Map>() { map };
            var gamesList = new List<Game>() { game };
            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(gamesList.ToArray());

            var apiGame = new ApiGame();
            apiGame.Identifier = uniqeHash;
            apiGame.Players = new List<ApiPlayer>() { new ApiPlayer() };
            apiGame.ReplayMapTextId = "map mp argonath";
            apiGame.PlayedOn = DateTime.MinValue;

            var configMock = new Mock<IConfigProvider>();
            configMock.Setup(x => x.GetAllowUnknownMapsSetting()).Returns(true);
            var sut = new GameValidator(configMock.Object);

            //Act & Assert
            var ex = Assert.Throws<InvalidOperationException>(() => sut.IsGameValid(dbWrapper._DbContext, apiGame));
            Assert.AreEqual($"Such a game has already been reported in the RotWK League system.", ex.Message);
        }

    }
}
