﻿using Common.Services;
using DALMySql.POCO;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tests.CommonTests
{
    [TestFixture]
    public class FailedReportServiceTests
    {
        [Test]
        public async Task Given_ExistingGameReport_DoesNot_AddNewEntry()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingGameIdentifier = 1;

            var map = new Map() { Id = 1, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "One", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "Two", UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };

            var game1 = new Game() { Id = 1, PlayedOn = new DateTime(2018, 01, 01), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null, Identifier = existingGameIdentifier.ToString() };
            var GameDetails1Game1 = new GameDetail() { Id = 1, GameId = 1, PlayerId = 1, IsLooser = true, Team = -1 };
            var GameDetails2Game1 = new GameDetail() { Id = 2, GameId = 1, PlayerId = 2, IsLooser = false, Team = -1 };

            var GameDetailss = new List<GameDetail>()
            {
                GameDetails1Game1,
                GameDetails2Game1
            };

            var failedReport = new FailedReport() { GameId = existingGameIdentifier.ToString() };

            var mapsList = new List<Map>() { map };
            var gamesList = new List<Game>() { game1 };
            //var replayList = new List<Replay>() { replay1 };
            var reportsList = new List<FailedReport>() { failedReport };
            
            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(gamesList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(GameDetailss.ToArray());
            //await dbWrapper.PopulateDbWithDataAsync(replayList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(reportsList.ToArray());

            var sut = new FailedReportService(dbWrapper._DbContext);

            //Act
            sut.AddEntry(existingGameIdentifier.ToString(), "some msg");

            //Assert
            Assert.AreEqual(reportsList.Count, dbWrapper._DbContext.FailedReport.Count());

        }

        [Test]
        public async Task Given_NullGameId_Adds_NonSpecifiedGameId()
        {
            //Arrange
            using var dbWrapper = InmemoryDbWrapper.GetWrapper();
            var existingGameIdentifier = 1;

            var map = new Map() { Id = 1, CodeName = "FordsOfIsen", DisplayName = "Fords Of Isen", ReplayMapTextId = "map mp fords of isen", Image = new byte[] { 0x20 } };
            var userOne = new User() { Id = 1, Email = "playerOne@gmail.com", Password = "aaa", CreatedOn = DateTime.Now };
            var userTwo = new User() { Id = 2, Email = "playerTwo@gmail.com", Password = "bbb", CreatedOn = DateTime.Now };
            var users = new List<User>() { userOne, userTwo };
            var players = new List<Player>()
            {
                new Player() { Id = 1, Name = "One", UserId = userOne.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
                new Player() { Id = 2, Name = "Two", UserId = userTwo.Id, Rating = 1600, TeamRating = 1600, MonthlyRating = 1600, MonthlyTeamRating = 1600 },
            };

            var game1 = new Game() { Id = 1, PlayedOn = new DateTime(2018, 01, 01), MapId = 1, GameDetails = null, MonthlyRatings = null, Ratings = null, Identifier = existingGameIdentifier.ToString() };
            //var replay1 = new Replay() { Id = 1, File = new byte[8], GameId = 1, ReplayFileSize = 1024 };
            var GameDetails1Game1 = new GameDetail() { Id = 1, GameId = 1, PlayerId = 1, IsLooser = true, Team = -1 };
            var GameDetails2Game1 = new GameDetail() { Id = 2, GameId = 1, PlayerId = 2, IsLooser = false, Team = -1 };

            var GameDetailss = new List<GameDetail>()
            {
                GameDetails1Game1,
                GameDetails2Game1
            };

            var failedReport = new FailedReport() { GameId = existingGameIdentifier.ToString() };

            var mapsList = new List<Map>() { map };
            var gamesList = new List<Game>() { game1 };
            //var replayList = new List<Replay>() { replay1 };
            var reportsList = new List<FailedReport>() { failedReport };

            await dbWrapper.PopulateDbWithDataAsync(mapsList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(users.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(players.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(gamesList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(GameDetailss.ToArray());
            //await dbWrapper.PopulateDbWithDataAsync(replayList.ToArray());
            await dbWrapper.PopulateDbWithDataAsync(reportsList.ToArray());

            var sut = new FailedReportService(dbWrapper._DbContext);

            //Act
            sut.AddEntry(null, "some msg");

            //Assert
            Assert.AreEqual(reportsList.Count + 1, dbWrapper._DbContext.FailedReport.Count());
            Assert.AreEqual("Not specified", dbWrapper._DbContext.FailedReport.AsEnumerable().Last().GameId);
        }

    }
}
