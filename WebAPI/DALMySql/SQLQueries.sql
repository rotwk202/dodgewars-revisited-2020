﻿select p.[Name], (1600 + sum(r.Delta)) as 'IndividualRating'
from Rating r
join Player p on r.PlayerId = p.Id
where r.GameId in
(
	select g.Id
	from Game g
	join GameDetail gd on gd.GameId = g.Id
	group by g.Id
	having count(gd.PlayerId) = 2
)
group by p.[Name]

--drop db
USE master;
ALTER DATABASE [PointsSystem] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
DROP DATABASE [PointsSystem];

--rename db
ALTER DATABASE [old_name]
 
	SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO
ALTER DATABASE [old_name]
 
	MODIFY NAME = [new_name]
GO
ALTER DATABASE [new_name]
	SET MULTI_USER
GO

--delete all games
begin tran

delete from [dbo].[Replay]
delete from [dbo].[Rating]
delete from [dbo].[MonthlyRating]
delete from [dbo].[GameDetail]
delete from [dbo].[Game]

DBCC CHECKIDENT (Replay, RESEED, 0)
DBCC CHECKIDENT (Rating, RESEED, 0)
DBCC CHECKIDENT (MonthlyRating, RESEED, 0)
DBCC CHECKIDENT (GameDetail, RESEED, 0)
DBCC CHECKIDENT (Game, RESEED, 0)

update [dbo].[Player]
set Rating = 1600
update [dbo].[Player]
set TeamRating = 1600
update [dbo].[Player]
set MonthlyRating = 1000
update [dbo].[Player]
set MonthlyTeamRating = 1000

commit

--clean up test games
DECLARE @GameIdsToRemove TABLE (
    idx int identity(1,1),
    GameId int )

insert into @GameIdsToRemove(GameId)
	select distinct g.Id
	from Game g
	join GameDetail gd on gd.GameId = g.Id
	join Player p on p.Id = gd.PlayerId
	where p.[Name] like '%test%';

DECLARE @counter int
SET @counter = 1

WHILE(@counter <= (SELECT MAX(idx) FROM @GameIdsToRemove) + 1)
BEGIN
    DECLARE @gameId INT
	SELECT @gameId = GameId FROM @GameIdsToRemove WHERE idx = @counter

	delete from [dbo].[Replay] where GameId = @gameId
	delete from [dbo].[Rating] where GameId = @gameId
	delete from [dbo].[MonthlyRating] where GameId = @gameId
	delete from [dbo].[GameDetail] where GameId = @gameId
	delete from [dbo].[Game] where Id = @gameId

	--increment counter
    SET @counter = @counter + 1
END

--delete game of given Identifier
declare @identifier int;
declare @gameId int;
set @identifier = [Identifier_HERE];
set @gameId = (select Id from Game where Identifier = @identifier)

delete from Replay where GameId = @gameId;
delete from Rating where GameId = @gameId;
delete from MonthlyRating where GameId = @gameId;
delete from GameDetail where GameId = @gameId;
delete from Game where Id = @gameId;
