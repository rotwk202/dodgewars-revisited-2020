﻿using DALMySql.POCO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DALMySql.Configurations
{
    public class GameDetailConfiguration : IEntityTypeConfiguration<GameDetail>
    {
        public void Configure(EntityTypeBuilder<GameDetail> builder)
        {
            builder.HasKey(p => p.Id);

            //builder.Property(p => p.Id).UseIdentityColumn();
            builder.Property(p => p.Id).IsRequired();

            builder.HasOne(m => m.Game).WithMany(a => a.GameDetails).HasForeignKey(m => m.GameId);
            builder.HasOne(m => m.Player).WithMany(a => a.GameDetails).HasForeignKey(m => m.PlayerId);

            //builder.ToTable("GameDetail");
        }
    }
}
