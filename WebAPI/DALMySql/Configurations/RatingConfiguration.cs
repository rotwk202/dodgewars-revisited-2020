﻿using DALMySql.POCO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DALMySql.Configurations
{
    public class RatingConfiguration : IEntityTypeConfiguration<Rating>
    {
        public void Configure(EntityTypeBuilder<Rating> builder)
        {
            builder.HasKey(p => p.Id);

            //builder.Property(p => p.Id).UseIdentityColumn();
            builder.Property(p => p.Id).IsRequired();
            builder.Property(p => p.Delta).IsRequired();

            builder.HasOne(m => m.Game).WithMany(a => a.Ratings).HasForeignKey(m => m.GameId);
            builder.HasOne(m => m.Player).WithMany(a => a.Ratings).HasForeignKey(m => m.PlayerId);

            //builder.ToTable("Rating");
        }
    }
}
