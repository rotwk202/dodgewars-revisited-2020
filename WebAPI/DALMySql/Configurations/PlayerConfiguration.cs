﻿using DALMySql.POCO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DALMySql.Configurations
{
    public class PlayerConfiguration : IEntityTypeConfiguration<Player>
    {
        public void Configure(EntityTypeBuilder<Player> builder)
        {
            builder.HasKey(p => p.Id);

            //builder.Property(p => p.Id).UseIdentityColumn();
            builder.Property(p => p.Id).IsRequired();
            builder.Property(p => p.Name).HasMaxLength(10);

            builder.HasOne(m => m.User).WithMany(a => a.Players).HasForeignKey(m => m.UserId);

            //builder.ToTable("Player");
        }
    }
}
