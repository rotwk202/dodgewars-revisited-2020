﻿using DALMySql.POCO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DALMySql.Configurations
{
    public class GameConfiguration : IEntityTypeConfiguration<Game>
    {
        public void Configure(EntityTypeBuilder<Game> builder)
        {
            builder.HasKey(p => p.Id);

            //builder.Property(p => p.Id).UseIdentityColumn();
            builder.Property(p => p.Id).IsRequired();
            builder.Property(p => p.PlayedOn).IsRequired();
            builder.Property(p => p.Identifier).IsRequired();

            builder.HasOne(m => m.Map).WithMany(a => a.Games).HasForeignKey(m => m.MapId);

            //builder.ToTable("Game");
        }
    }
}
