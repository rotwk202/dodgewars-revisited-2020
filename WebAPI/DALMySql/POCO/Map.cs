﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DALMySql.POCO
{
    public class Map
    {
        public Map()
        {
            Games = new Collection<Game>();
        }

        public int Id { get; set; }
        public string CodeName { get; set; }
        public string DisplayName { get; set; }
        public byte[] Image { get; set; }
        public string ReplayMapTextId { get; set; }

        public virtual ICollection<Game> Games { get; set; }
    }
}
