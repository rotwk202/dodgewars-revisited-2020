﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DALMySql.POCO
{
    public class Game
    {
        public Game()
        {
            GameDetails = new Collection<GameDetail>();
            MonthlyRatings = new Collection<MonthlyRating>();
            Ratings = new Collection<Rating>();
        }

        public int Id { get; set; }
        public int MapId { get; set; }
        public DateTime PlayedOn { get; set; }
        public string Identifier { get; set; }

        public Map Map { get; set; }
        public virtual ICollection<GameDetail> GameDetails { get; set; }
        public virtual ICollection<MonthlyRating> MonthlyRatings { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}
