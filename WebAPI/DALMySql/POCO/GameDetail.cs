﻿namespace DALMySql.POCO
{
    public class GameDetail
    {
        public int Id { get; set; }
        public int PlayerId { get; set; }
        public int GameId { get; set; }
        public int Team { get; set; }
        public bool IsLooser { get; set; }
        public bool? IsGameHost { get; set; }
        public string Army { get; set; }

        public Game Game { get; set; }
        public Player Player { get; set; }
    }
}
