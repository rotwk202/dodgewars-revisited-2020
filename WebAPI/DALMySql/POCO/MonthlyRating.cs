﻿namespace DALMySql.POCO
{
    public class MonthlyRating
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public double Delta { get; set; }
        public int PlayerId { get; set; }

        public Game Game { get; set; }
        public Player Player { get; set; }
    }
}
