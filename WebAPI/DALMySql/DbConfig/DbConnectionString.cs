﻿namespace DALMySql.DbConfig
{
    public static class DbConnectionString
    {
        private static string Default = "Default";

        static DbConnectionString()
        {
            Configured = Default;
        }

        public static string Configured { get; private set; }
    }
}
