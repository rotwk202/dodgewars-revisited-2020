﻿using BusinessLogic.Services;
using Common;
using Common.Model;
using Common.Model.Statistics;
using DALMySql;
using DALMySql.POCO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.POC
{
    public class POCService
    {
        private readonly Random _random = new Random(Guid.NewGuid().GetHashCode());
        private readonly LadderContext _dbContext;

        public POCService() { }

        public POCService(LadderContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void SimulateNGames(List<Player> dbPlayers,
            GameReportService gameReportService,
            int gamesCount,
            DateTime startDate,
            DateTime endDate,
            [Optional] int takeNFirstPlayers)
        {
            for (int i = 0; i < gamesCount; i++)
            {
                //generate random game-pairs
                var pairs = Get1v1Pair(takeNFirstPlayers > 0 ? dbPlayers.Take(takeNFirstPlayers).ToList() : dbPlayers);
                var uniqueIdentifier = Guid.NewGuid().ToString();
                var apiGame = new ApiGame()
                {
                    Identifier = uniqueIdentifier,
                    //PlayedOn = DateTime.Now,
                    PlayedOn = GetRandomDateTime(startDate, endDate),
                    ReplayFile = new byte[256],
                    ReplayMapTextId = "map mp fords of isen",
                    LastReplayFileLength = 256,
                    Players = pairs
                };

                //simulate games without replays
                gameReportService.ReportLoss(apiGame);
            }
        }

        public List<ApiPlayer> Get1v1Pair(List<Player> players)
        {
            //two random players
            var firstOpponentIndex = _random.Next(0, players.Count - 1);
            var secondOpponentIndex = GetRandomOpponentId(firstOpponentIndex, players.Count);
            var firstPlayer = players[firstOpponentIndex];
            var secondPlayer = players[secondOpponentIndex];

            //randomize who won
            var halfChanceTrue = _random.Next(1, 100) > 50 ? true : false;

            var apiPlayers = new List<ApiPlayer>();
            apiPlayers.Add(new ApiPlayer()
            {
                Name = firstPlayer.Name,
                Army = Constants.AllArmies[GetRandomArmyIndex()],
                IsGameHost = halfChanceTrue,
                IsLooser = halfChanceTrue,
                IsObserver = false
            });
            apiPlayers.Add(new ApiPlayer()
            {
                Name = secondPlayer.Name,
                Army = Constants.AllArmies[GetRandomArmyIndex()],
                IsGameHost = !halfChanceTrue,
                IsLooser = !halfChanceTrue,
                IsObserver = false
            });

            return apiPlayers;
        }

        public async Task<(MedalStage medalStage, double progressPerc)> GetTotalPlayerWinsMedal(LadderContext dbContext, string season, string playerName)
        {
            var seasonBoundries = GetSeasonBoundries(season);

            var playerGames = await(from g in dbContext.Game
                                      join gd in dbContext.GameDetail on g.Id equals gd.GameId
                                      join p in dbContext.Player on gd.PlayerId equals p.Id
                                      select new { g.Id, g.PlayedOn, Name = p.Name, DisplayName = p.DisplayName, gd.IsLooser })
            .AsQueryable()
            .Distinct()
            .Where(x => x.Name == playerName || x.DisplayName == playerName)
            .Where(x => x.PlayedOn.Date > seasonBoundries.startDate.Date && x.PlayedOn.Date < seasonBoundries.endDate.Date)
            .OrderByDescending(x => x.PlayedOn)
            .ToListAsync();

            var winsCount = playerGames.Count(x => x.IsLooser == false);
            var medal = MedalStages.GetTotalWinsMedal(winsCount);

            return (medal.medalStage, medal.progressPerc);
        }

        public async Task<PlayerMedalDetails> GetTotalPlayerWinsMedal(string season, string playerName)
        {
            var seasonBoundries = GetSeasonBoundries(season);

            var playerGames = await (from g in _dbContext.Game
                                     join gd in _dbContext.GameDetail on g.Id equals gd.GameId
                                     join p in _dbContext.Player on gd.PlayerId equals p.Id
                                     select new { g.Id, g.PlayedOn, Name = p.Name, DisplayName = p.DisplayName, gd.IsLooser })
            .AsQueryable()
            .Distinct()
            .Where(x => x.Name == playerName || x.DisplayName == playerName)
            .Where(x => x.PlayedOn.Date > seasonBoundries.startDate.Date && x.PlayedOn.Date < seasonBoundries.endDate.Date)
            .OrderByDescending(x => x.PlayedOn)
            .ToListAsync();

            var winsCount = playerGames.Count(x => x.IsLooser == false);
            
            var totalPlayerWinsMedal = MedalStages.GetTotalWinsMedal(winsCount);
            var activityMedal = MedalStages.GetActivityMedal(playerGames.Count);

            var result = new PlayerMedalDetails();
            result.PlayerName = playerName;
            result.Season = season;

            result.TotalWins = new MedalModel((int)totalPlayerWinsMedal.medalStage + 1,
                totalPlayerWinsMedal.progressPerc,
                totalPlayerWinsMedal.medalStage);

            result.Activity = new MedalModel((int)activityMedal.medalStage + 1,
                activityMedal.progressPerc,
                activityMedal.medalStage);

            var ambitionMedal = (medalStage: MedalStage.Silver_2, progressPerc: 50);
            result.Ambition = new MedalModel((int)ambitionMedal.medalStage + 1,
                ambitionMedal.progressPerc,
                ambitionMedal.medalStage);

            result.TotalPlayersWinsMedalStagesDict = MedalStages.TotalPlayersWinsMedalStagesDict;

            return result;
        }

        private (DateTime startDate, DateTime endDate) GetSeasonBoundries(string season)
        {
            var year = Convert.ToInt32(season.Split(".")[0]);
            var quarter = Convert.ToInt32(season.Split(".")[1]);
            
            //TODO: blabla walidacja na steing ktory przyjdzie, bo inaczej sie wywali

            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;

            switch (quarter)
            {
                case 1:
                    startDate = new DateTime(year, 1, 1);
                    endDate = new DateTime(year, 3, 31);
                    break;
                case 2:
                    startDate = new DateTime(year, 4, 1);
                    endDate = new DateTime(year, 6, 30);
                    break;
                case 3:
                    startDate = new DateTime(year, 7, 1);
                    endDate = new DateTime(year, 9, 30);
                    break;
                case 4:
                    startDate = new DateTime(year, 10, 1);
                    endDate = new DateTime(year, 12, 31);
                    break;
                default:
                    break;
            }

            return (startDate: startDate, endDate: endDate);
        }

        private int GetRandomOpponentId(int firstOpponentIndex, int playersCount)
        {
            int resultIndex = firstOpponentIndex;
            while (resultIndex == firstOpponentIndex)
            {
                resultIndex = _random.Next(0, playersCount - 1);
            }

            return resultIndex;
        }

        public int GetRandomArmyIndex()
        {
            return _random.Next(0, Constants.AllArmies.Length - 1);
        }

        private DateTime GetRandomDateTime(DateTime startDate,DateTime endDate)
        {
            TimeSpan timeSpan = endDate - startDate;
            TimeSpan newSpan = new TimeSpan(0, _random.Next(0, (int)timeSpan.TotalMinutes), 0);
            DateTime newDate = startDate + newSpan;

            return newDate;
        }

    }
}
