﻿using Common.Model.Statistics;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.POC
{
    public static class MedalStages
    {
        public static readonly double StartScoreTotalWinsBronze_1 = 16;
        public static readonly double StartScoreTotalWinsBronze_2 = 36;
        public static readonly double StartScoreTotalWinsBronze_3 = 68;
        public static readonly double StartScoreTotalWinsSilver_1 = 95;
        public static readonly double StartScoreTotalWinsSilver_2 = 142;
        public static readonly double StartScoreTotalWinsSilver_3 = 189;
        public static readonly double StartScoreTotalWinsGold_1 = 300;
        public static readonly double StartScoreTotalWinsGold_2 = 450;
        public static readonly double StartScoreTotalWinsGold_3 = 530;

        public static readonly double StartScoreActivityBronze_1 = 16;
        public static readonly double StartScoreActivityBronze_2 = 36;
        public static readonly double StartScoreActivityBronze_3 = 72;
        public static readonly double StartScoreActivitySilver_1 = 124;
        public static readonly double StartScoreActivitySilver_2 = 170;
        public static readonly double StartScoreActivitySilver_3 = 230;
        public static readonly double StartScoreActivityGold_1 = 320;
        public static readonly double StartScoreActivityGold_2 = 450;
        public static readonly double StartScoreActivityGold_3 = 530;

        public static readonly Dictionary<MedalStage, double> TotalPlayersWinsMedalStagesDict = new Dictionary<MedalStage, double>()
        {
            { MedalStage.Bronze_1, StartScoreTotalWinsBronze_1 },
            { MedalStage.Bronze_2, StartScoreTotalWinsBronze_2 },
            { MedalStage.Bronze_3, StartScoreTotalWinsBronze_3 },
            { MedalStage.Silver_1, StartScoreTotalWinsSilver_1 },
            { MedalStage.Silver_2, StartScoreTotalWinsSilver_2 },
            { MedalStage.Silver_3, StartScoreTotalWinsSilver_3 },
            { MedalStage.Gold_1, StartScoreTotalWinsGold_1 },
            { MedalStage.Gold_2, StartScoreTotalWinsGold_2 },
            { MedalStage.Gold_3, StartScoreTotalWinsGold_3 },
        };

        public static (MedalStage medalStage, double progressPerc) GetTotalWinsMedal(double winsCount)
        {
            int number = 50;
            switch (number)
            {
                case int n when (winsCount >= StartScoreTotalWinsBronze_1 && winsCount < StartScoreTotalWinsBronze_2):
                    var progressPercValue = Math.Floor((double)(winsCount / StartScoreTotalWinsBronze_2 * 100));
                    return (medalStage: MedalStage.Bronze_1, progressPerc: progressPercValue);

                case int n when (winsCount >= StartScoreTotalWinsBronze_2 && winsCount < StartScoreTotalWinsBronze_3):
                    progressPercValue = Math.Floor((double)(winsCount / StartScoreTotalWinsBronze_3 * 100));
                    return (medalStage: MedalStage.Bronze_2, progressPerc: progressPercValue);

                case int n when (winsCount >= StartScoreTotalWinsBronze_3 && winsCount < StartScoreTotalWinsSilver_1):
                    progressPercValue = Math.Floor((double)(winsCount / StartScoreTotalWinsSilver_1 * 100));
                    return (medalStage: MedalStage.Bronze_3, progressPerc: progressPercValue);

                case int n when (winsCount >= StartScoreTotalWinsSilver_1 && winsCount < StartScoreTotalWinsSilver_2):
                    progressPercValue = Math.Floor((double)(winsCount / StartScoreTotalWinsSilver_2 * 100));
                    return (medalStage: MedalStage.Silver_1, progressPerc: progressPercValue);

                case int n when (winsCount >= StartScoreTotalWinsSilver_2 && winsCount < StartScoreTotalWinsSilver_3):
                    progressPercValue = Math.Floor((double)(winsCount / StartScoreTotalWinsSilver_3 * 100));
                    return (medalStage: MedalStage.Silver_2, progressPerc: progressPercValue);

                case int n when (winsCount >= StartScoreTotalWinsSilver_3 && winsCount < StartScoreTotalWinsGold_1):
                    progressPercValue = Math.Floor((double)(winsCount / StartScoreTotalWinsGold_1 * 100));
                    return (medalStage: MedalStage.Silver_3, progressPerc: progressPercValue);

                case int n when (winsCount >= StartScoreTotalWinsGold_1 && winsCount < StartScoreTotalWinsGold_2):
                    progressPercValue = Math.Floor((double)(winsCount / StartScoreTotalWinsGold_2 * 100));
                    return (medalStage: MedalStage.Gold_1, progressPerc: progressPercValue);

                case int n when (winsCount >= StartScoreTotalWinsGold_2 && winsCount < StartScoreTotalWinsGold_3):
                    progressPercValue = Math.Floor((double)(winsCount / StartScoreTotalWinsGold_3 * 100));
                    return (medalStage: MedalStage.Gold_2, progressPerc: progressPercValue);

                case int n when (winsCount >= StartScoreTotalWinsGold_3):
                    progressPercValue = Math.Floor((double)(winsCount / StartScoreTotalWinsGold_3 * 100));
                    return (medalStage: MedalStage.Gold_3, progressPerc: progressPercValue);
                default:
                    return (MedalStage.Bronze_1, 0);
            }
        }

        public static (MedalStage medalStage, double progressPerc) GetActivityMedal(double gamesCount)
        {
            int number = 50;
            switch (number)
            {
                case int n when (gamesCount >= StartScoreActivityBronze_1 && gamesCount < StartScoreActivityBronze_2):
                    var progressPercValue = Math.Floor((double)(gamesCount / StartScoreActivityBronze_2 * 100));
                    return (medalStage: MedalStage.Bronze_1, progressPerc: progressPercValue);

                case int n when (gamesCount >= StartScoreActivityBronze_2 && gamesCount < StartScoreActivityBronze_3):
                    progressPercValue = Math.Floor((double)(gamesCount / StartScoreActivityBronze_3 * 100));
                    return (medalStage: MedalStage.Bronze_2, progressPerc: progressPercValue);

                case int n when (gamesCount >= StartScoreActivityBronze_3 && gamesCount < StartScoreActivitySilver_1):
                    progressPercValue = Math.Floor((double)(gamesCount / StartScoreActivitySilver_1 * 100));
                    return (medalStage: MedalStage.Bronze_3, progressPerc: progressPercValue);

                case int n when (gamesCount >= StartScoreActivitySilver_1 && gamesCount < StartScoreActivitySilver_2):
                    progressPercValue = Math.Floor((double)(gamesCount / StartScoreActivitySilver_2 * 100));
                    return (medalStage: MedalStage.Silver_1, progressPerc: progressPercValue);

                case int n when (gamesCount >= StartScoreActivitySilver_2 && gamesCount < StartScoreActivitySilver_3):
                    progressPercValue = Math.Floor((double)(gamesCount / StartScoreActivitySilver_3 * 100));
                    return (medalStage: MedalStage.Silver_2, progressPerc: progressPercValue);

                case int n when (gamesCount >= StartScoreActivitySilver_3 && gamesCount < StartScoreActivityGold_1):
                    progressPercValue = Math.Floor((double)(gamesCount / StartScoreActivityGold_1 * 100));
                    return (medalStage: MedalStage.Silver_3, progressPerc: progressPercValue);

                case int n when (gamesCount >= StartScoreActivityGold_1 && gamesCount < StartScoreActivityGold_2):
                    progressPercValue = Math.Floor((double)(gamesCount / StartScoreActivityGold_2 * 100));
                    return (medalStage: MedalStage.Gold_1, progressPerc: progressPercValue);

                case int n when (gamesCount >= StartScoreActivityGold_2 && gamesCount < StartScoreActivityGold_3):
                    progressPercValue = Math.Floor((double)(gamesCount / StartScoreActivityGold_3 * 100));
                    return (medalStage: MedalStage.Gold_2, progressPerc: progressPercValue);

                case int n when (gamesCount >= StartScoreActivityGold_3):
                    progressPercValue = Math.Floor((double)(gamesCount / StartScoreActivityGold_3 * 100));
                    return (medalStage: MedalStage.Gold_3, progressPerc: progressPercValue);
                default:
                    return (MedalStage.Bronze_1, 0);
            }
        }
    }
}
