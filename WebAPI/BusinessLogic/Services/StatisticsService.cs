﻿using BusinessLogic.POC;
using Common;
using Common.Interfaces;
using Common.Model.Statistics;
using DALMySql;
using DALMySql.POCO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class StatisticsService 
    {
        private readonly LadderContext _dbContext;
        private readonly IConfigProvider _configProvider;
        private readonly POCService _pocService;

        public StatisticsService(LadderContext dbContext, IConfigProvider configProvider)
        {
            _dbContext = dbContext;
            _configProvider = configProvider;
            var allRotwkPatches = _configProvider.GetRotwkPatchesSetting();
        }

        public StatisticsService(LadderContext dbContext, IConfigProvider configProvider, POCService pocService)
        {
            _dbContext = dbContext;
            _configProvider = configProvider;
            _pocService = pocService;
            var allRotwkPatches = _configProvider.GetRotwkPatchesSetting();
        }

        public IList<OverallArmyStat> GetArmiesStatsInIndividualGames()
        {
            var result = new List<OverallArmyStat>();

            var indiviualGameDetails = _dbContext.GameDetail
                .AsQueryable()
                .ToList()
                .GroupBy(x => x.GameId)
                .Where(grp => grp.Count() == 2)
                .SelectMany(values => values)
                .ToList();
            
            FilterMirrorMatchups(indiviualGameDetails);

            foreach (var army in Constants.AllArmies)
            {
                var armyWinsCount = (double) indiviualGameDetails.Where(x => x.Army == army && x.IsLooser == false).Count();
                var armyGamesCount = (double) indiviualGameDetails.Where(x => x.Army == army).Count();

                var newStat = new OverallArmyStat()
                {
                    Army = army,
                    WinPercentage = Math.Round(armyWinsCount / armyGamesCount, 2)  * 100
                };

                result.Add(newStat);
            };

            return result;
        }

        private void FilterMirrorMatchups(List<GameDetail> indiviualGameDetails)
        {
            var gameIds = indiviualGameDetails.Select(x => x.GameId).ToList();
            foreach (var gameId in gameIds)
            {
                var details = indiviualGameDetails.Where(x => x.GameId == gameId).ToList();
                var areDistinctArmies = details.Select(x => x.Army).Distinct().Count() == 2;

                if (!areDistinctArmies)
                {
                    indiviualGameDetails.RemoveAll(x => x.GameId == gameId);
                }
            }
        }

        public async Task<IList<ActivityStat>> GetMonthActivityStats(string dateString)
        {
            var result = new List<ActivityStat>();
            if (!DateTime.TryParse(dateString, out DateTime dateTime))
            {
                return result;
            }

            var games = await _dbContext.Game
                .AsQueryable()
                .Where(x => x.PlayedOn.Month == dateTime.Month && x.PlayedOn.Year == dateTime.Year)
                .ToListAsync();

            var startDate = new DateTime(dateTime.Year, dateTime.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            

            foreach (DateTime date in EachDay(startDate, endDate))
            {
                var detail = new ActivityStat()
                {
                    DateString = date.ToShortDateString(),
                    GamesCount = games.Where(x => x.PlayedOn.Year == date.Year
                                        && x.PlayedOn.Month == date.Month
                                        && x.PlayedOn.Day == date.Day)
                    .Count()
                };

                result.Add(detail);
            }

            return result;
        }

        public async Task<PlayerCard> GetPlayerCard(string playerName, [Optional] string season)
        {
            var dbPlayers = await _dbContext.Player.ToListAsync();

            #region Validation

            if (string.IsNullOrEmpty(playerName))
            {
                throw new ArgumentException($"Player name param was not provided.");
            }

            if (!(dbPlayers.Exists(x => x.Name == playerName) || dbPlayers.Exists(x => x.DisplayName == playerName)))
            {
                throw new KeyNotFoundException($"Player {playerName} does not exist.");
            }

            #endregion

            var gameDetails = await _dbContext.GameDetail.ToListAsync();
            
            var indiviualGameDetails = gameDetails
                .GroupBy(x => x.GameId)
                .Where(grp => grp.Count() == 2)
                .SelectMany(values => values)
                .ToList();

            var teamGameDetails = gameDetails
                .GroupBy(x => x.GameId)
                .Where(grp => grp.Count() > 2)
                .SelectMany(values => values)
                .ToList();

            var dbStatPlayer = dbPlayers.FirstOrDefault(x => x.Name == playerName || x.DisplayName == playerName);

            var permaIndividualWinsCount = indiviualGameDetails.Where(x => x.PlayerId == dbStatPlayer.Id && !x.IsLooser).Count();
            var permaIndividualLosesCount = indiviualGameDetails.Where(x => x.PlayerId == dbStatPlayer.Id && x.IsLooser).Count();
            var permaTeamWinsCount = teamGameDetails.Where(x => x.PlayerId == dbStatPlayer.Id && !x.IsLooser).Count();
            var permaTeamLosesCount = teamGameDetails.Where(x => x.PlayerId == dbStatPlayer.Id && x.IsLooser).Count();

            var statPlayerIndiviualGameIds = indiviualGameDetails
                .Where(x => x.PlayerId == dbStatPlayer.Id)
                .Select(x => x.GameId)
                .ToList();

            var playerOpponents = new List<PlayerOpponentInfo>();
            foreach (var gameId in statPlayerIndiviualGameIds)
            {
                var gameDetailOfOpponent = indiviualGameDetails
                    .Where(x => x.GameId == gameId && x.PlayerId != dbStatPlayer.Id)
                    .FirstOrDefault();

                var opponent = dbPlayers.FirstOrDefault(x => x.Id == gameDetailOfOpponent.PlayerId);
                string opponentName = null;
                if(opponent != null)
                {
                    opponentName = opponent.DisplayName ?? opponent.Name;
                }
                
                if(!playerOpponents.Any(x => x.PlayerName == opponentName))
                {
                    var newPlayerOpponent = new PlayerOpponentInfo();
                    newPlayerOpponent.PlayerName = opponentName;
                    playerOpponents.Add(newPlayerOpponent);
                }

                if (gameDetailOfOpponent.IsLooser)
                {
                    playerOpponents.First(x => x.PlayerName == opponentName).WonAgainst++;
                }
                else
                {
                    playerOpponents.First(x => x.PlayerName == opponentName).LostTo++;
                }
            }

            var totalPlayerWinsMedal = await _pocService.GetTotalPlayerWinsMedal(season, playerName);

            var result = new PlayerCard()
            {
                PlayerName = dbStatPlayer.DisplayName ?? dbStatPlayer.Name,
                PermaIndividualRating = dbStatPlayer.Rating,
                PermaTeamRating = dbStatPlayer.TeamRating,
                MonthlyIndividualRating = dbStatPlayer.MonthlyRating,
                MonthlyTeamRating = dbStatPlayer.MonthlyTeamRating,
                PermaIndividualWinsCount = permaIndividualWinsCount,
                PermaIndividualLosesCount = permaIndividualLosesCount,
                PermaTeamWinsCount = permaTeamWinsCount,
                PermaTeamLosesCount = permaTeamLosesCount,
                PermaIndividualWinRatio = GetWinRatio(permaIndividualWinsCount, permaIndividualLosesCount),
                PermaTeamWinRatio = GetWinRatio(permaTeamWinsCount, permaTeamLosesCount),
                PlayerOpponents = playerOpponents,
                PlayerMedalDetails = totalPlayerWinsMedal
            };

            return result;
        }

        private double GetWinRatio(int winsCount, int losesCount)
        {
            var top = (double)winsCount;
            var bottom = (double)(winsCount + losesCount);

            return Math.Round(top / bottom, 2) * 100;
        }
        
        private IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

    }
}
