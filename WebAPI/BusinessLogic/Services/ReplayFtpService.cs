﻿using DALMySql;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Net;

namespace BusinessLogic.Services
{
    public class ReplayFtpService
    {
        private readonly IConfiguration _config;
        private readonly LadderContext _dbContext;
        private readonly string _ftpLogin;
        private readonly string _ftpPassword;
        private readonly string _ftpReplaysDestinationUrl;

        public ReplayFtpService(IConfiguration config, LadderContext dbContext)
        {
            _config = config;
            _dbContext = dbContext;
            _ftpLogin = _config.GetValue<string>("FtpUserName");
            _ftpPassword = _config.GetValue<string>("FtpPassword");
            _ftpReplaysDestinationUrl = _config.GetValue<string>("FtpReplaysDestinationUrl");
        }

        public void UploadReplay(string identifier, Stream replayFileStream)
        {
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var filePath = Path.Combine(baseDirectory, "replayToUpload.bfme2replay");

            FileStream fileStream = null;
            using (fileStream = File.Create(filePath))
            using (replayFileStream)
            {
                replayFileStream.Seek(0, SeekOrigin.Begin);
                replayFileStream.CopyTo(fileStream);
            }

            using (var client = new WebClient())
            {
                client.Credentials = new NetworkCredential(_ftpLogin, _ftpPassword);
                var replayUrlDestination = Path.Combine(_ftpReplaysDestinationUrl, $"RotwkLeagueReplay_{identifier}.bfme2replay");
                client.UploadFile(replayUrlDestination, WebRequestMethods.Ftp.UploadFile, filePath);
            }
        }

        public byte[] DownloadReplay(string identifier)
        {
            

            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var filePath = Path.Combine(baseDirectory, "replayToDownload.bfme2replay");

            using (var client = new WebClient())
            {
                client.Credentials = new NetworkCredential(_ftpLogin, _ftpPassword);
                var replayUrlDestination = Path.Combine(_ftpReplaysDestinationUrl, $"RotwkLeagueReplay_{identifier}.bfme2replay");
                client.DownloadFile(replayUrlDestination, filePath);
            }

            if (new FileInfo(filePath).Length == 0 && !File.Exists(filePath))
            {
                throw new FileNotFoundException($"Unable to find replay file of identifier {identifier}");
            }

            return File.ReadAllBytes(filePath);
        }

        public void DeleteReplay(string identifier)
        {
            string fileNameToDelete = $"Game_{identifier}_Replay.BfME2Replay";
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(Path.Combine(_ftpReplaysDestinationUrl, fileNameToDelete));

            request.Credentials = new NetworkCredential(_ftpLogin, _ftpPassword);
            request.Method = WebRequestMethods.Ftp.DeleteFile;
            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            {
            }
        }

    }
}
