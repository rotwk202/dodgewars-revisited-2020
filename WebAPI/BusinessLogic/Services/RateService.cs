﻿using BusinessLogic.PointsSystem;
using Common.Model;
using Common.Tools;
using DALMySql;
using DAL.DbConfig;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Common.Interfaces;

namespace BusinessLogic.Services
{
    public class RateService
    {
        private readonly IConfiguration _config;
        private readonly LadderContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;
        private readonly Stopwatch _watch;

        public class GameIdQueryResult
        {
            public int GameId { get; set; }
        }

        public RateService(IConfiguration config, LadderContext dbContext, ISqlConnectionProvider sqlConnectionProvider)
        {
            _config = config;
            _dbContext = dbContext;
            _sqlConnectionProvider = sqlConnectionProvider;
            _watch = new Stopwatch();
        }

        public List<QueryResult> GetIndividualAllTimeRatings()
        {
            var result = new List<QueryResult>();

            var players = _dbContext.Player.AsQueryable().Select(x => new { Id = x.Id, Name = x.Name, DisplayName = x.DisplayName }).ToList();
            var oneVsOneGameIds = GetOneVsOneGameIds();

            var oneVsOneGameDetails = _dbContext.GameDetail.AsQueryable()
                .Where(x => oneVsOneGameIds.Contains(x.GameId))
                .ToList();

            var oneVsOneRatingDeltas = _dbContext.Rating.AsQueryable()
                    .Where(x => oneVsOneGameIds.Contains(x.GameId))
                    .ToList();

            foreach (var player in players)
            {
                var resultItem = new QueryResult();
                resultItem.Rating =
                    oneVsOneRatingDeltas
                    .Where(x => x.PlayerId == player.Id)
                    .Select(x => x.Delta)
                    .ToList()
                    .Sum() + Constants.InitialRatingValue;

                resultItem.Name = player.DisplayName ?? player.Name;

                resultItem.WinsCount = oneVsOneGameDetails
                    .Where(x => x.PlayerId == player.Id && !x.IsLooser).Count();
                resultItem.LosesCount = oneVsOneGameDetails
                    .Where(x => x.PlayerId == player.Id && x.IsLooser).Count();

                if(resultItem.WinsCount == 0 && resultItem.LosesCount == 0)
                {
                    continue;
                }

                result.Add(resultItem);
            }

            return result.OrderByDescending(x => x.Rating).ToList();
        }

        public List<QueryResult> GetIndividualMonthlyRatings(string dateString)
        {
            var result = new List<QueryResult>();
            DateTime dateTime;
            var parsed = DateTime.TryParse(dateString, out dateTime);

            if (!parsed)
            {
                throw new ArgumentException($"Unable to parse given dateString: {dateString}");
            }

            var players = _dbContext.Player.AsQueryable().Select(x => new { Id = x.Id, Name = x.Name, DisplayName = x.DisplayName }).ToList();
            var oneVsOneGameInMonthIds = GetOneVsOneGameIdsInMonth(dateTime.Month, dateTime.Year);

            var oneVsOneGameInMonthDetails = _dbContext.GameDetail.AsQueryable()
                .Where(x => oneVsOneGameInMonthIds.Contains(x.GameId))
                .ToList();

            var oneVsOneMonthlyRatingDeltas = _dbContext.MonthlyRating.AsQueryable()
                    .Where(x => oneVsOneGameInMonthIds.Contains(x.GameId))
                    .ToList();

            foreach (var player in players)
            {
                var resultItem = new QueryResult();
                resultItem.Rating =
                    oneVsOneMonthlyRatingDeltas
                    .Where(x => x.PlayerId == player.Id)
                    .Select(x => x.Delta)
                    .ToList()
                    .Sum() + Constants.InitialMonthlyRatingValue;

                resultItem.Name = player.DisplayName ?? player.Name;

                resultItem.WinsCount = oneVsOneGameInMonthDetails
                    .Where(x => x.PlayerId == player.Id && !x.IsLooser).Count();
                resultItem.LosesCount = oneVsOneGameInMonthDetails
                    .Where(x => x.PlayerId == player.Id && x.IsLooser).Count();

                if (resultItem.WinsCount == 0 && resultItem.LosesCount == 0)
                {
                    continue;
                }

                result.Add(resultItem);
            }

            return result.OrderByDescending(x => x.Rating).ToList();
        }

		public List<QueryResult> GetTeamAllTimeRatings()
		{
            var result = new List<QueryResult>();

            var players = _dbContext.Player.AsQueryable().Select(x => new { Id = x.Id, Name = x.Name, DisplayName = x.DisplayName }).ToList();
            var teamGameIds = GetTeamGameIds();

            var teamGameDetails = _dbContext.GameDetail.AsQueryable()
                .Where(x => teamGameIds.Contains(x.GameId))
                .ToList();

            var teamRatingDeltas = _dbContext.Rating.AsQueryable()
                    .Where(x => teamGameIds.Contains(x.GameId))
                    .ToList();

            foreach (var player in players)
            {
                var resultItem = new QueryResult();
                resultItem.Rating =
                    teamRatingDeltas
                    .Where(x => x.PlayerId == player.Id)
                    .Select(x => x.Delta)
                    .ToList()
                    .Sum() + Constants.InitialRatingValue;

                resultItem.Name = player.DisplayName ?? player.Name;

                resultItem.WinsCount = teamGameDetails
                    .Where(x => x.PlayerId == player.Id && !x.IsLooser).Count();
                resultItem.LosesCount = teamGameDetails
                    .Where(x => x.PlayerId == player.Id && x.IsLooser).Count();

                if (resultItem.WinsCount == 0 && resultItem.LosesCount == 0)
                {
                    continue;
                }

                result.Add(resultItem);
            }

            return result.OrderByDescending(x => x.Rating).ToList();
        }

		public List<QueryResult> GetTeamMonthlyRatings(string dateString)
		{
            var result = new List<QueryResult>();
            DateTime dateTime;
            var parsed = DateTime.TryParse(dateString, out dateTime);

            if (!parsed)
            {
                throw new ArgumentException($"Unable to parse given dateString: {dateString}");
            }

            var players = _dbContext.Player.AsQueryable().Select(x => new { Id = x.Id, Name = x.Name, DisplayName = x.DisplayName }).ToList();
            var teamGameInMonthIds = GetTeamGameIdsInMonth(dateTime.Month, dateTime.Year);

            var teamGameInMonthDetails = _dbContext.GameDetail.AsQueryable()
                .Where(x => teamGameInMonthIds.Contains(x.GameId))
                .ToList();

            var teamMonthlyRatingDeltas = _dbContext.MonthlyRating.AsQueryable()
                    .Where(x => teamGameInMonthIds.Contains(x.GameId))
                    .ToList();

            foreach (var player in players)
            {
                var resultItem = new QueryResult();
                resultItem.Rating =
                    teamMonthlyRatingDeltas
                    .Where(x => x.PlayerId == player.Id)
                    .Select(x => x.Delta)
                    .ToList()
                    .Sum() + Constants.InitialMonthlyRatingValue;

                resultItem.Name = player.DisplayName ?? player.Name;

                resultItem.WinsCount = teamGameInMonthDetails
                    .Where(x => x.PlayerId == player.Id && !x.IsLooser).Count();
                resultItem.LosesCount = teamGameInMonthDetails
                    .Where(x => x.PlayerId == player.Id && x.IsLooser).Count();

                if (resultItem.WinsCount == 0 && resultItem.LosesCount == 0)
                {
                    continue;
                }

                result.Add(resultItem);
            }

            return result.OrderByDescending(x => x.Rating).ToList();
        }

        public List<RecentGame> GetRecentGames(int gamesCount, int pageNumber)
        {
            var result = new List<RecentGame>();

            var gameIdsToTake = _dbContext.Game.AsQueryable()
                .OrderByDescending(x => x.PlayedOn)
                .Skip(pageNumber * gamesCount)
                .Take(gamesCount)
                .Select(x => x.Id)
                .ToList();

            using (IDbConnection db = _sqlConnectionProvider.GetConnection())
            {
                var parameters = new { GameIdsToTake = gameIdsToTake };
                result = db.Query<RecentGame>
                (@" select g.Id as DisplayGameId,
                        g.Identifier as GameId,
	                    m.CodeName as MapCodeName,
	                    m.DisplayName as MapDisplayName,
	                    g.PlayedOn as PlayedOn
                    from Game g
                    join Map m on g.MapId = m.Id
                    where g.Id in @GameIdsToTake", parameters)
                .ToList();
            }

            var dbGameDetails = _dbContext.GameDetail.AsQueryable().Where(x => gameIdsToTake.Contains(x.GameId)).ToList();
            var dbGames = _dbContext.Game.AsQueryable().Where(x => gameIdsToTake.Contains(x.Id)).ToList();

            foreach (var dbGame in dbGames)
            {
                var isNeutralHost = dbGameDetails.Where(x => x.GameId == dbGame.Id).All(x => x.IsGameHost == false);
                var apiGameToUpdate = result.FirstOrDefault(x => x.GameId == dbGame.Identifier);

                apiGameToUpdate.IsNeutralHost = isNeutralHost;
                apiGameToUpdate.GamePrimaryKeyId = dbGame.Id;
            }

            var playerIds = dbGameDetails.Select(x => x.PlayerId).ToList();
            var dbPlayers = _dbContext.Player.AsQueryable().Where(x => playerIds.Contains(x.Id)).ToList();

            foreach (var gameId in gameIdsToTake)
            {
                var recentGameTpUpdate = result.FirstOrDefault(x => x.GamePrimaryKeyId == gameId);
                var apiPlayers = new List<ApiPlayer>();

                var gameDetailsOfGivenGameId = dbGameDetails.Where(x => x.GameId == gameId).ToList();
                foreach (var gameDetail in gameDetailsOfGivenGameId)
                {
                    var apiPlayer = new ApiPlayer();
                    var player = dbPlayers.FirstOrDefault(x => x.Id == gameDetail.PlayerId);
                    apiPlayer.IsLooser = gameDetail.IsLooser;
                    apiPlayer.IsGameHost = gameDetail.IsGameHost;
                    apiPlayer.Name = player.DisplayName ?? player.Name;
                    apiPlayer.Army = gameDetail.Army;
                    apiPlayer.ArmyColorCode = ArmyColorCodes.GetArmyColorCode(gameDetail.Army);

                    apiPlayers.Add(apiPlayer);
                }

                recentGameTpUpdate.Players = apiPlayers;
            }

            return result.OrderByDescending(x => x.PlayedOn).ToList();
        }

        public ApiGamesCountData GetGamesCountData()
        {
            var gamesAllTimeCount = _dbContext.Game.AsQueryable().Count();
            var gamesCurrentMonthCount = _dbContext.Game
                .AsQueryable()
                .Where(x => x.PlayedOn.Month == DateTime.Now.Month
                    && x.PlayedOn.Year == DateTime.Now.Year)
                .Count();

            return new ApiGamesCountData() { GamesAllTimeCount = gamesAllTimeCount, GamesCurrentMonthCount = gamesCurrentMonthCount };
        }

        private List<int> GetOneVsOneGameIds()
        {
            var oneVsOneGameIds = new List<int>();

            using (IDbConnection db = _sqlConnectionProvider.GetConnection())
            {
                oneVsOneGameIds = db.Query<GameIdQueryResult>
                (@" select gd.GameId
                    from GameDetail gd
                    group by gd.GameId
                    having count(gd.PlayerId) = 2")
                .Select(x => x.GameId)
                .ToList();
            }

            return oneVsOneGameIds;
        }

        private List<int> GetTeamGameIds()
        {
            var teamGameIds = new List<int>();

            using (IDbConnection db = _sqlConnectionProvider.GetConnection())
            {
                teamGameIds = db.Query<GameIdQueryResult>
                (@" select gd.GameId
                    from GameDetail gd
                    group by gd.GameId
                    having count(gd.PlayerId) > 2")
                .Select(x => x.GameId)
                .ToList();
            }

            return teamGameIds;
        }

        private List<int> GetOneVsOneGameIdsInMonth(int monthNumber, int yearNumber)
        {
            var oneVsOneGamesInMonthIds = new List<int>();

            using (IDbConnection db = _sqlConnectionProvider.GetConnection())
            {
                var parameters = new { MonthNumber = monthNumber, YearNumber = yearNumber };

                oneVsOneGamesInMonthIds = db.Query<GameIdQueryResult>
                (@" select gd.GameId
                    from GameDetail gd
                    join Game g on g.Id = gd.GameId
                    where month(g.PlayedOn) = @MonthNumber and year(g.PlayedOn) = @YearNumber
                    group by gd.GameId
                    having count(gd.PlayerId) = 2", parameters)
                .Select(x => x.GameId)
                .ToList();
            }

            return oneVsOneGamesInMonthIds;
        }

        private List<int> GetTeamGameIdsInMonth(int monthNumber, int yearNumber)
        {
            var teamGamesInMonthIds = new List<int>();

            using (IDbConnection db = _sqlConnectionProvider.GetConnection())
            {
                var parameters = new { MonthNumber = monthNumber, YearNumber = yearNumber };

                teamGamesInMonthIds = db.Query<GameIdQueryResult>
                (@" select gd.GameId
                    from GameDetail gd
                    join Game g on g.Id = gd.GameId
                    where month(g.PlayedOn) = @MonthNumber and year(g.PlayedOn) = @YearNumber
                    group by gd.GameId
                    having count(gd.PlayerId) > 2", parameters)
                .Select(x => x.GameId)
                .ToList();
            }

            return teamGamesInMonthIds;
        }
    }
}
