﻿using Common;
using Common.Model;
using Common.Tools;
using DALMySql;
using DALMySql.POCO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class AuthenticationService
    {
        private readonly LadderContext _dbContext;
        private const string _secretAdmin = Constants.SecretAdmin;
        public static string SecretAdmin { get { return _secretAdmin; } }

        public AuthenticationService(LadderContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<string> GetToken(ApiUser apiUser, string clientName)
        {
            if (!_dbContext.User.AsQueryable().Any(x => x.Email == apiUser.Email))
            {
                throw new InvalidOperationException("There is no user with such e-mail address.");
            }

            if (string.IsNullOrEmpty(apiUser.Password))
            {
                throw new ArgumentException("Provided password either didn't exist or was empty.");
            }

            if (apiUser.Email == Constants.SecretAdmin)
            {
                throw new InvalidOperationException("You're unable to log in as admin.");
            }

            var dBUser = _dbContext.User.AsQueryable().FirstOrDefault(x => x.Email == apiUser.Email);
            var computedPasswordHash = PasswordEncryptor.CreateMD5(apiUser.Password);
            if (computedPasswordHash != dBUser.Password)
            {
                throw new ArgumentException("There is no user with such e-mail address or password provided was wrong.");
            }

            var now = DateTime.Now;
            var tokenSourceString = $"{now}{apiUser.Email}{apiUser.Password}";
            var token = PasswordEncryptor.CreateMD5(tokenSourceString);
            var tokenExpiresOn = now.AddMonths(1);

            if (clientName == Constants.ParserClientName)
            {
                dBUser.TokenConsole = token;
                dBUser.TokenConsoleExpiresOn = tokenExpiresOn;
                _dbContext.Set<User>().Attach(dBUser);
                _dbContext.Entry(dBUser).State = EntityState.Modified;
            }
            if (clientName == Constants.WebClientName)
            {
                dBUser.TokenWeb = token;
                dBUser.TokenWebExpiresOn = tokenExpiresOn;
                _dbContext.Set<User>().Attach(dBUser);
                _dbContext.Entry(dBUser).State = EntityState.Modified;
            }

            await _dbContext.SaveChangesAsync();
            return token;
        }

        public async Task<string> GetAdminToken(ApiUser apiUser)
        {
            if (string.IsNullOrEmpty(apiUser.Password))
            {
                throw new ArgumentException("Provided admin token was null or empty.");
            }

            var secretUser = await _dbContext.User
            .AsQueryable()
            .FirstOrDefaultAsync(x => x.Email == _secretAdmin);

            if (secretUser is null)
            {
                throw new KeyNotFoundException($"Admin token not defined. Please create user {_secretAdmin} in the database.");
            }

            if(secretUser.Password != PasswordEncryptor.CreateMD5(apiUser.Password))
            {
                throw new Exception("Login or password provided are incorrect.");
            }

            return secretUser.Password;
        }


        public async Task<bool> IsTokenOutOfDate(string token, string clientName, DateTime requestTime)
        {
            DateTime? expirationDate = null;

            if (clientName == Constants.ParserClientName)
            {
                if (!_dbContext.User.AsQueryable().Any(x => x.TokenConsole == token))
                {
                    throw new ArgumentException("No such token was found");
                }
                expirationDate = _dbContext.User.AsQueryable().First(x => x.TokenConsole == token).TokenConsoleExpiresOn;
            }

            if (clientName == Constants.WebClientName)
            {
                if (!_dbContext.User.AsQueryable().Any(x => x.TokenWeb == token))
                {
                    throw new ArgumentException("No such token was found");
                }
                expirationDate = _dbContext.User.AsQueryable().First(x => x.TokenWeb == token).TokenWebExpiresOn;
            }

            return !(expirationDate > requestTime);
        }

        public async Task<bool> AuthenticateUser(string login, string token, string clientName)
        {
            if (string.IsNullOrEmpty(login))
            {
                throw new AuthenticationException("Error trying to authenticate user. No login was provided.");
            }

            if (string.IsNullOrEmpty(token))
            {
                throw new AuthenticationException("Error trying to authenticate user. No token was provided.");
            }

            if (string.IsNullOrEmpty(clientName))
            {
                throw new AuthenticationException($"Client name was not provided. Valid values: {Constants.ParserClientName} or {Constants.WebClientName}.");
            }

            var dbLogin = await _dbContext.User.AsQueryable().FirstOrDefaultAsync(x => x.Email == login);
            if(dbLogin is null)
            {
                throw new AuthenticationException("User with given login was not found in system.");
            }

            if(clientName == Constants.ParserClientName)
            {
                if (dbLogin.TokenConsole is null || string.IsNullOrEmpty(dbLogin.TokenConsole))
                {
                    throw new AuthenticationException("Token provided does not exist.");
                }

                if (dbLogin.TokenConsole != token)
                {
                    throw new AuthenticationException($"Given token {token} does not belong to user with login {login}.");
                }

                if (dbLogin.TokenConsoleExpiresOn < DateTime.Now)
                {
                    throw new AuthenticationException($"Given token has expired. Please log in again to renew it.");
                }

                return true;
            }

            return false;
        }
    }
}
