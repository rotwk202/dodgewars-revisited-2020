﻿using BusinessLogic.PointsSystem;
using Common.Model;
using Common.Services;
using Common.Tools;
using DALMySql;
using DALMySql.POCO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class AccountService
    {
        private readonly LadderContext _dbContext;
        private readonly FailedReportService _failedReportService;
        private const int _maximumPlayerNameLength = 10;
        private const string _secretAdmin = Common.Constants.SecretAdmin;
        public static string SecretAdmin { get { return _secretAdmin; } }

        public AccountService(LadderContext dbContext, FailedReportService failedReportService)
        {
            _dbContext = dbContext;
            _failedReportService = failedReportService;
        }

        public async Task<bool> CreatePlayer(string userEmail, string nickname, string nicknameDisplay)
        {
            var user = _dbContext.User.AsQueryable().FirstOrDefault(x => x.Email.Equals(userEmail));
            if (user == null)
            {
                throw new InvalidOperationException($"User of given email does not exist.");
            }

            if (!ValidatePlayerName(nickname))
            {
                return false;
            }

            if (!ValidatePlayerDisplayName(nicknameDisplay))
            {
                return false;
            }

            var newPlayer = new Player
            {
                UserId = user.Id,
                Name = nickname,
                DisplayName = nicknameDisplay,
                Rating = Constants.InitialRatingValue,
                TeamRating = Constants.InitialRatingValue,
                MonthlyRating = Constants.InitialMonthlyRatingValue,
                MonthlyTeamRating = Constants.InitialMonthlyRatingValue,
            };

            _dbContext.Set<Player>().Add(newPlayer);
            var nicknameAddedCount = await _dbContext.SaveChangesAsync();

            if(nicknameAddedCount == 1)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> UpdatePlayerName(string userEmail, string playerName, string playerDisplayName)
        {
            var user = _dbContext.User.AsQueryable().FirstOrDefault(x => x.Email.Equals(userEmail));
            if (user == null)
            {
                throw new InvalidOperationException($"User of given email does not exist.");
            }

            if (!ValidatePlayerDisplayName(playerDisplayName))
            {
                return false;
            }

            var playerToUpdate = _dbContext.Player.AsQueryable().FirstOrDefault(x => x.Name == playerName);

            if(playerToUpdate is null)
            {
                throw new KeyNotFoundException($"Player of name {playerName} does not exist.");
            }

            playerToUpdate.DisplayName = playerDisplayName;

            _dbContext.Set<Player>().Attach(playerToUpdate);
            _dbContext.Entry(playerToUpdate).State = EntityState.Modified;
            var playerUpdatedCount = await _dbContext.SaveChangesAsync();

            if (playerUpdatedCount == 1)
            {
                return true;
            }

            return false;
        }


        public bool ValidatePlayerName(string playerName)
        {
            var isNicknameReserved = _dbContext.Player.AsQueryable().Any(x => x.Name == playerName);
            if (isNicknameReserved)
            {
                throw new InvalidOperationException($"In game name {playerName} is already in use");
            }

            if (playerName.Length > _maximumPlayerNameLength)
            {
                throw new InvalidOperationException(
                    $"Given in game name {playerName} is too long. Maximum in game name length is {_maximumPlayerNameLength.ToString()}");
            }

            return true;
        }

        public bool ValidatePlayerDisplayName(string playerDisplayName)
        {
            var isNicknameReserved = _dbContext.Player.AsQueryable().Any(x => x.DisplayName == playerDisplayName);
            if (isNicknameReserved)
            {
                throw new InvalidOperationException($"League display name {playerDisplayName} is already in use");
            }

            return true;
        }

        public async Task<bool> RegisterUser(ApiUser apiUser, string clientName)
        {
            if (_dbContext.User.AsQueryable().Any(x => x.Email == apiUser.Email))
            {
                throw new InvalidOperationException("User with such e-mail already exists.");
            }

            if (string.IsNullOrEmpty(apiUser.Password))
            {
                throw new ArgumentException("Provided password either didn't exist or was empty.");
            }

            if (clientName == "web")
            {
                var registrationDate = DateTime.Now;
                var newUser = new User
                {
                    Email = apiUser.Email,
                    Password = PasswordEncryptor.CreateMD5(apiUser.Password),
                    CreatedOn = registrationDate
                };

                if (!ValidatePlayerName(apiUser.InitialNickname))
                {
                    return false;
                }
                if (!ValidatePlayerDisplayName(apiUser.InitialDisplayName))
                {
                    return false;
                }

                _dbContext.Set<User>().Add(newUser);
                var userAddedCount = await _dbContext.SaveChangesAsync();
                await CreatePlayer(newUser.Email, apiUser.InitialNickname, apiUser.InitialDisplayName);

                if (userAddedCount == 1)
                {
                    return true;
                }
            }

            return false;
        }

        public string GetUserName(string playerName)
        {
            if (string.IsNullOrEmpty(playerName))
            {
                throw new ArgumentException("Provided user name was incorrect.");
            }

            var player = _dbContext.Player.AsQueryable().FirstOrDefault(x => x.Name == playerName);
            if(player is null)
            {
                var message = $"Player of given name {playerName} was not found.";
                _failedReportService.AddEntry(null, message);
                throw new KeyNotFoundException($"Player of given name {playerName} was not found.");
            }

            return _dbContext.User.AsQueryable().FirstOrDefault(x => x.Id == player.UserId).Email;
        }

        public async Task<IEnumerable<string>> GetPlayerNames(string login)
        {
            if (string.IsNullOrEmpty(login))
            {
                throw new ArgumentException("Provided login was incorrect.");
            }

            var dbLogin = await _dbContext.User.AsQueryable().FirstOrDefaultAsync(x => x.Email == login);
            if (dbLogin is null)
            {
                throw new KeyNotFoundException("Provided login was not found in system.");
            }

            return await _dbContext.Player.AsQueryable().Where(x => x.UserId == dbLogin.Id).Select(x => x.Name).ToListAsync();
        }

        public async Task<IList<ApiNameDisplayName>> GetPlayerNamesDisplayNames(string login)
        {
            if (string.IsNullOrEmpty(login))
            {
                throw new ArgumentException("Provided login was incorrect.");
            }

            var dbLogin = await _dbContext.User.AsQueryable().FirstOrDefaultAsync(x => x.Email == login);
            if (dbLogin is null)
            {
                throw new KeyNotFoundException("Provided login was not found in system.");
            }

            var dbNameDetails = _dbContext.Player.AsQueryable()
                .Where(x => x.UserId == dbLogin.Id)
                .Select(x => new { InReplayName = x.Name, DisplayName = x.DisplayName });

            var result = new List<ApiNameDisplayName>();
            foreach (var nameDetail in dbNameDetails)
            {
                result.Add(new ApiNameDisplayName() { InReplayName = nameDetail.InReplayName, DisplayName = nameDetail.DisplayName ?? nameDetail.InReplayName });
            }

            return result;
        }


        public async Task<bool> IsValidAdminPwd(string adminToken)
        {
            if (string.IsNullOrEmpty(adminToken))
            {
                throw new ArgumentException("Provided admin token was null or empty.");
            }

            var secretUser = await _dbContext.User
            .AsQueryable()
            .FirstOrDefaultAsync(x => x.Email == _secretAdmin);

            if (secretUser is null)
            {
                throw new KeyNotFoundException($"Admin token not defined. Please create user {_secretAdmin} in the database.");
            }

            return secretUser.Password == adminToken;
        }

        public async Task<IEnumerable<ApiUserPublic>> GetAllAccountNames()
        {
            return await _dbContext.User
                .AsQueryable()
                .Select(x => new ApiUserPublic
                {
                    Id = x.Id,
                    AccountName = x.Email,
                    CreationDate = x.CreatedOn
                })
                .ToListAsync();
        }

        public async Task<IEnumerable<ApiNicknamePublic>> GetAllNickNames()
        {
            return await _dbContext.Player
                .AsQueryable()
                .Select(x => new ApiNicknamePublic
                {
                    AccountID = x.UserId,
                    Nickname = x.Name
                })
                .ToListAsync();
        }

        public async Task<IEnumerable<ApiNicknamePublic>> GetAllNickNamesPublic()
        {
            return await _dbContext.Player
                .AsQueryable()
                .OrderBy(x => x.Name)
                .Select(x => new ApiNicknamePublic
                {
                    Nickname = x.Name
                })
                .ToListAsync();
        }

        public async Task ResetPassword(ApiUser user)
        {
            if(user == null)
            {
                throw new ArgumentException("User to update was no provided.");
            }
            if (user.Email == _secretAdmin)
            {
                throw new InvalidOperationException("You're not allowed to change the password of this user.");
            }
            if (string.IsNullOrEmpty(user.Email))
            {
                throw new ArgumentException("User login was not provided.");
            }
            if (string.IsNullOrEmpty(user.Password))
            {
                throw new ArgumentException("Reset password was not provided.");
            }

            var dbUser = _dbContext.User.AsQueryable().FirstOrDefault(x => x.Email == user.Email);
            if (dbUser == null)
            {
                throw new KeyNotFoundException("User with given user name does not exist.");
            }

            dbUser.Password = PasswordEncryptor.CreateMD5(user.Password);

            _dbContext.User.Attach(dbUser);
            _dbContext.Entry(dbUser).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

    }
}
