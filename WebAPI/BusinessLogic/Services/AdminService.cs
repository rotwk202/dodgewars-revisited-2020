﻿using Common.Model;
using DALMySql;
using DALMySql.POCO;
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class AdminService
    {
        private readonly LadderContext _dbContext;
        private readonly ReplayFtpService _replayFtpService;

        public List<string> EmailListForUnitTests { get; private set; }
        public List<int> GameIdsListForUnitTests { get; private set; }
 
        public AdminService(LadderContext dbContext, ReplayFtpService replayFtpService)
        {
            _dbContext = dbContext;
            _replayFtpService = replayFtpService;
        }

        public AdminService()
        {

        }

        //public byte[] GetRecalcReplayAndLogins()
        //{
        //    var replays = _dbContext.Replay.AsQueryable()
        //        .OrderBy(x => x.Game.PlayedOn)
        //        .ToList();

        //    var chronologicReportsLoginList = new List<string>();
        //    GameIdsListForUnitTests = new List<int>();

        //    foreach (var replay in replays)
        //    {
        //        var email = (from r in _dbContext.Replay
        //                     join g in _dbContext.Game on r.GameId equals g.Id
        //                     join gd in _dbContext.GameDetail on g.Id equals gd.GameId
        //                     join p in _dbContext.Player on gd.PlayerId equals p.Id
        //                     join u in _dbContext.User on p.UserId equals u.Id
        //                     where g.Id == replay.GameId && gd.IsLooser
        //                     select new
        //                     {
        //                         u.Email
        //                     }).AsQueryable().First().Email;

        //        chronologicReportsLoginList.Add(email);
        //        GameIdsListForUnitTests.Add(replay.GameId);
        //    }

        //    EmailListForUnitTests = chronologicReportsLoginList;
        //    Directory.CreateDirectory("result");

        //    using (ZipFile zip = new ZipFile())
        //    using (var stream = new MemoryStream())
        //    {
        //        for (int i = 0; i < replays.Count; i++)
        //        {
        //            File.WriteAllBytes($"result/{i},{chronologicReportsLoginList[i]}.bfme2replay", replays[i].File);
        //            zip.AddFile($"result/{i},{chronologicReportsLoginList[i]}.bfme2replay");
        //        }

        //        zip.Save(stream);
        //        Directory.Delete("result", recursive: true);
        //        return stream.ToArray();
        //    }

        //}

        //public byte[] DownloadRepsWithUniqueIds()
        //{
        //    var replays = _dbContext.Replay.AsQueryable()
        //        .OrderBy(x => x.Game.PlayedOn)
        //        .ToList();

        //    var games = _dbContext.Game.AsQueryable()
        //        .ToList();

        //    Directory.CreateDirectory("result");

        //    using (ZipFile zip = new ZipFile())
        //    using (var stream = new MemoryStream())
        //    {
        //        for (int i = 0; i < replays.Count; i++)
        //        {
        //            var gameHashIdentifier = games.FirstOrDefault(x => x.Id == replays[i].GameId).Identifier;
        //            File.WriteAllBytes($"result/LeagueGameOfHashIdentifider_{gameHashIdentifier}.bfme2replay", replays[i].File);
        //            zip.AddFile($"result/LeagueGameOfHashIdentifider_{gameHashIdentifier}.bfme2replay");
        //        }

        //        zip.Save(stream);
        //        Directory.Delete("result", recursive: true);
        //        return stream.ToArray();
        //    }

        //}

        public async Task<bool> UploadAllMaps(List<ApiMap> mapModels)
        {
            var newMaps = new List<Map>();
            var dbMapsTextIds = await _dbContext.Map.AsQueryable().Select(x => x.ReplayMapTextId).ToListAsync();

            foreach (var mapModel in mapModels)
            {
                if(dbMapsTextIds.Exists(x => x == mapModel.ReplayMapTextId))
                {
                    continue;
                }

                newMaps.Add(new Map()
                {
                    CodeName = mapModel.CodeName,
                    DisplayName = mapModel.DisplayName,
                    ReplayMapTextId = mapModel.ReplayMapTextId,
                    Image = mapModel.Image
                });
            }

            await _dbContext.Map.AddRangeAsync(newMaps);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task MigrateReplays()
        {
            var a = AppDomain.CurrentDomain.BaseDirectory;

            var someText = "some text here";
            var filePath = Path.Combine(a, "example.txt");
            var fileBytes = Encoding.ASCII.GetBytes(someText);

            using (var fileStream = File.Create(filePath))
            using (var sourceStream = new MemoryStream(fileBytes))
            {
                sourceStream.Seek(0, SeekOrigin.Begin);
                sourceStream.CopyTo(fileStream);
            }

            using (var client = new WebClient())
            {
                client.Credentials = new NetworkCredential("secretLogin", "secretPass");
                client.UploadFile("ftp://ftp.rotwk-league.net/abc/temp.txt", WebRequestMethods.Ftp.UploadFile, filePath);
            }

        }

        public async Task DownloadReplay()
        {
            using (var client = new WebClient())
            {
                client.Credentials = new NetworkCredential("secretLogin", "secretPass");
                client.DownloadFile("ftp://ftp.rotwk-league.net/abc/temp.txt", @"c:\temp\edit\downloaded.txt");
            }

        }

        public async Task<IList<int>> GetGamePrimaryKeyIds()
        {
            var res = await _dbContext.Game.AsQueryable().Select(x => x.Id).ToListAsync();
            return res;
        }

        public async Task DeleteGame(int gamePrimaryKeyId)
        {
            var dbGame = await _dbContext.Game.AsQueryable().FirstOrDefaultAsync(x => x.Id == gamePrimaryKeyId);

            if(dbGame is null)
            {
                throw new KeyNotFoundException($"Game of display ID {gamePrimaryKeyId} was not found.");
            }

            var ratingsToRemove = _dbContext.Rating.AsQueryable().Where(x => x.GameId == dbGame.Id);
            var monthlyRatingsToRemove = _dbContext.MonthlyRating.AsQueryable().Where(x => x.GameId == dbGame.Id);
            var gameDetailsToRemove = _dbContext.GameDetail.AsQueryable().Where(x => x.GameId == dbGame.Id);

            _dbContext.Rating.RemoveRange(ratingsToRemove);
            _dbContext.MonthlyRating.RemoveRange(monthlyRatingsToRemove);
            _dbContext.GameDetail.RemoveRange(gameDetailsToRemove);
            _dbContext.Game.Remove(dbGame);

            _replayFtpService.DeleteReplay(dbGame.Identifier);

            await _dbContext.SaveChangesAsync();
        }

    }
}
