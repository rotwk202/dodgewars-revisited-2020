﻿using DALMySql;
using System.Linq;

namespace BusinessLogic.Services
{
    public class ImageService
    {
        private readonly LadderContext _dbContext;

        public ImageService(LadderContext dbContext)
        {
            _dbContext = dbContext;
        }

        public byte[] GetMapImage(string mapCodeName)
        {
            var map = _dbContext.Map.AsQueryable().FirstOrDefault(x => x.CodeName == mapCodeName);

            return map?.Image;
        }
    }
}
