﻿using BusinessLogic.PointsSystem;
using Common.Model;
using Common.Tools;
using DALMySql;
using DALMySql.POCO;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class PlayerHistoryService
    {
        private readonly LadderContext _dbContext;

        public PlayerHistoryService(LadderContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<RecentGame>> GetHistory(int gamesCount, string playerName, int pageNumber, string ladderType)
        {
            var result = new List<RecentGame>();

            var gameIdsToTake = await (from g in _dbContext.Game
                                    join gd in _dbContext.GameDetail on g.Id equals gd.GameId
                                    join p in _dbContext.Player on gd.PlayerId equals p.Id
                                    select new { g.Id, g.PlayedOn, Name = p.Name, DisplayName = p.DisplayName})
                        .AsQueryable()
                        .Distinct()
                        .Where(x => x.Name == playerName || x.DisplayName == playerName)
                        .OrderByDescending(x => x.PlayedOn)
                        .Skip(pageNumber * gamesCount)
                        .Take(gamesCount)
                        .Select(x => x.Id)
                        .ToListAsync();

            result = await (from g in _dbContext.Game
                            join m in _dbContext.Map on g.MapId equals m.Id
                            select new RecentGame()
                            {
                                DisplayGameId = g.Id,
                                GameId = g.Identifier,
                                GamePrimaryKeyId = g.Id,
                                MapCodeName = m.CodeName,
                                MapDisplayName = m.DisplayName,
                                PlayedOn = g.PlayedOn
                            })
                        .AsQueryable()
                        .Where(x => gameIdsToTake.Contains(x.GamePrimaryKeyId))
                        .ToListAsync();

            var dbGameDetails = _dbContext.GameDetail.AsQueryable().Where(x => gameIdsToTake.Contains(x.GameId)).ToList();
            var dbGames = _dbContext.Game.AsQueryable().Where(x => gameIdsToTake.Contains(x.Id)).ToList();

            foreach (var dbGame in dbGames)
            {
                var isNeutralHost = dbGameDetails.Where(x => x.GameId == dbGame.Id).All(x => x.IsGameHost == false);
                var apiGameToUpdate = result.FirstOrDefault(x => x.GameId == dbGame.Identifier);

                apiGameToUpdate.IsNeutralHost = isNeutralHost;
                apiGameToUpdate.GamePrimaryKeyId = dbGame.Id;
            }

            var playerIds = dbGameDetails.Select(x => x.PlayerId).ToList();
            var dbPlayers = _dbContext.Player.AsQueryable().Where(x => playerIds.Contains(x.Id)).ToList();

            foreach (var gameId in gameIdsToTake)
            {
                var recentGameTpUpdate = result.FirstOrDefault(x => x.GamePrimaryKeyId == gameId);
                var apiPlayers = new List<ApiPlayer>();

                var gameDetailsOfGivenGameId = dbGameDetails.Where(x => x.GameId == gameId).ToList();
                foreach (var gameDetail in gameDetailsOfGivenGameId)
                {
                    var apiPlayer = new ApiPlayer();
                    var player = dbPlayers.FirstOrDefault(x => x.Id == gameDetail.PlayerId);
                    apiPlayer.IsLooser = gameDetail.IsLooser;
                    apiPlayer.IsGameHost = gameDetail.IsGameHost;
                    apiPlayer.Name = player.Name;
                    apiPlayer.DisplayName = player.DisplayName;
                    apiPlayer.Army = gameDetail.Army;
                    apiPlayer.ArmyColorCode = ArmyColorCodes.GetArmyColorCode(gameDetail.Army);

                    apiPlayers.Add(apiPlayer);
                }

                recentGameTpUpdate.Players = apiPlayers;
            }

            var gameIds = result.Select(x => x.GamePrimaryKeyId).ToList();

            List<Rating> ratingChanges = null;
            List<MonthlyRating> monthlyRatingChanges = null;
            if(ladderType == Constants.ParmanentLadderType)
            {
                ratingChanges = await _dbContext.Rating.AsQueryable().Where(x => gameIds.Contains(x.GameId)).ToListAsync();

                foreach (var resultToUpdate in result)
                {
                    foreach (var player in resultToUpdate.Players)
                    {
                        player.Delta = ratingChanges
                            .Where(x => x.GameId == resultToUpdate.GamePrimaryKeyId)
                            .FirstOrDefault(x => x.Player.Name == player.Name)
                            .Delta;
                    }
                }
            }
            if (ladderType == Constants.MonthlyLadderType)
            {
                monthlyRatingChanges = await _dbContext.MonthlyRating.AsQueryable().Where(x => gameIds.Contains(x.GameId)).ToListAsync();

                foreach (var resultToUpdate in result)
                {
                    foreach (var player in resultToUpdate.Players)
                    {
                        player.Delta = monthlyRatingChanges
                            .Where(x => x.GameId == resultToUpdate.GamePrimaryKeyId)
                            .FirstOrDefault(x => x.Player.Name == player.Name)
                            .Delta;
                    }
                }
            }

            return result.OrderByDescending(x => x.PlayedOn).ToList();
        }

    }
}
