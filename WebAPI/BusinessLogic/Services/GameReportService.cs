﻿using BusinessLogic.PointsSystem;
using BusinessLogic.Validators;
using Common.Interfaces;
using Common.Model;
using Common.Services;
using DALMySql;
using DALMySql.POCO;
using Dapper;
using DodgeWars.BusinessLogic.Services;
using log4net;
using log4net.Repository;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BusinessLogic.Services
{
    public class GameReportService
    {
        private readonly LadderContext _dbContext;
        private readonly GameValidator _gameValidator;
        private readonly IConfiguration _config;
        private readonly FailedReportService _failedReportService;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;
        private readonly ILog _logger;
        private readonly ILog _errorLogger;
        private const string repoName = "GameReportServiceRepo";

        public GameReportService(LadderContext dbContext,
            GameValidator gameValidator,
            IConfiguration config,
            FailedReportService failedReportService,
            ISqlConnectionProvider sqlConnectionProvider)
        {
            _dbContext = dbContext;
            _gameValidator = gameValidator;
            _config = config;
            _failedReportService = failedReportService;
            _sqlConnectionProvider = sqlConnectionProvider;
            ILoggerRepository repo = null;
            if (!LogManager.GetAllRepositories().Select(x => x.Name).ToList().Any(x => x == repoName))
            {
                repo = LogManager.CreateRepository(repoName);
            }
            _logger = LogManager.GetLogger(repoName, "InfoLogger");
            _errorLogger = LogManager.GetLogger(repoName, "ExceptionLogger");
        }

        public void ResetMonthlyRatings(ApiGame apiGame)
        {
            var lastGame = _dbContext.Game
                .AsQueryable()
                .OrderByDescending(x => x.PlayedOn)
                .FirstOrDefault();

            if (lastGame != null && apiGame.PlayedOn.Month > lastGame.PlayedOn.Month
                && apiGame.PlayedOn.Year > lastGame.PlayedOn.Year)
            {
                using (var connection = _sqlConnectionProvider.GetConnection())
                {
                    var result = connection
                        .Query(@"UPDATE [dbo].[Player]
                            SET MonthlyRating = @initialRatingValue
                            UPDATE [dbo].[Player]
                            SET MonthlyTeamRating = @initialRatingValue", Constants.InitialMonthlyRatingValue);
                }
            }
        }

        public string ReportLoss(ApiGame apiGame)
        {
            _logger.Info($"Going to report loss. The ApiGame model as follows: {apiGame.ToString()}");

            try
            {
                if (!_gameValidator.IsGameValid(_dbContext, apiGame))
                {
                    throw new InvalidOperationException("Game validation failed.");
                }

                apiGame.Players.RemoveAll(x => x.IsObserver);
                var dbGamePlayers = _dbContext
                    .Player
                    .AsQueryable()
                    .Where(x => apiGame.Players.Select(x => x.Name).ToList().Contains(x.Name))
                    .ToList();

                foreach (var gamePlayer in apiGame.Players)
                {
                    var player = dbGamePlayers.FirstOrDefault(x => x.Name == gamePlayer.Name);

                    if (player is null)
                    {
                        var message = $"Player of nickname {gamePlayer.Name} was not found in RotWK League system.";
                        _failedReportService.AddEntry(apiGame.Identifier.ToString(), message);
                        throw new Exception(message);
                    }
                }

                var newGame = new Game
                {
                    MapId = _dbContext.Map.FirstOrDefault(x => x.ReplayMapTextId.Equals(apiGame.ReplayMapTextId)).Id,
                    PlayedOn = apiGame.PlayedOn,
                    Identifier = apiGame.Identifier,
                };
                _dbContext.Game.Add(newGame);

                GameDetail newGameDetail;

                foreach (var gamePlayer in apiGame.Players)
                {
                    var player = dbGamePlayers.FirstOrDefault(x => x.Name == gamePlayer.Name);
                    newGameDetail = new GameDetail()
                    {
                        Game = newGame,
                        Player = player,
                        Team = gamePlayer.Team,
                        IsLooser = gamePlayer.IsLooser,
                        IsGameHost = gamePlayer.IsGameHost,
                        Army = gamePlayer.Army
                    };

                    _dbContext.GameDetail.Add(newGameDetail);
                }

                var isOneVsOneGame = apiGame.Players.Count == 2;

                List<CalculationResult> globalCalcResults;
                List<CalculationResult> monthlyCalcResults;
                if (isOneVsOneGame)
                {
                    globalCalcResults = RatingCalculator.GetIndividualRatingsDeltas(dbGamePlayers, apiGame.Players);
                    monthlyCalcResults = RatingCalculator.GetMonthlyIndividualRatingsDeltas(dbGamePlayers, apiGame.Players);
                }
                else
                {
                    globalCalcResults = TeamRatingCalculator.GetTeamRatingsDeltas(dbGamePlayers, apiGame.Players);
                    monthlyCalcResults = TeamRatingCalculator.GetMonthlyTeamRatingsDeltas(dbGamePlayers, apiGame.Players);
                }

                foreach (var item in globalCalcResults)
                {
                    var newRating = new Rating()
                    {
                        Game = newGame,
                        Delta = item.Delta,
                        PlayerId = item.PlayerId
                    };
                    _dbContext.Rating.Add(newRating);

                    var dbGamePlayerToEdit = dbGamePlayers.First(x => x.Id == item.PlayerId);
                    if (isOneVsOneGame)
                    {
                        dbGamePlayerToEdit.Rating += item.Delta;
                    }
                    else
                    {
                        dbGamePlayerToEdit.TeamRating += item.Delta;
                    }

                    _dbContext.Set<Player>().Attach(dbGamePlayerToEdit);
                    _dbContext.Entry(dbGamePlayerToEdit).State = EntityState.Modified;
                }

                foreach (var item in monthlyCalcResults)
                {
                    var newRating = new MonthlyRating()
                    {
                        Game = newGame,
                        Delta = item.Delta,
                        PlayerId = item.PlayerId
                    };
                    _dbContext.MonthlyRating.Add(newRating);

                    var dbGamePlayerToEdit = dbGamePlayers.First(x => x.Id == item.PlayerId);
                    if (isOneVsOneGame)
                    {
                        dbGamePlayerToEdit.MonthlyRating += item.Delta;
                    }
                    else
                    {
                        dbGamePlayerToEdit.MonthlyTeamRating += item.Delta;
                    }

                    _dbContext.Set<Player>().Attach(dbGamePlayerToEdit);
                    _dbContext.Entry(dbGamePlayerToEdit).State = EntityState.Modified;
                }

                _dbContext.SaveChanges();
                return newGame.Identifier;
            }
            catch (Exception ex)
            {
                _errorLogger.Error("Report loss went wrong", ex);
                throw ex;
            }
        }
    }
}
