﻿using Common;
using Common.Interfaces;
using Common.Model;
using DALMySql;
using System;
using System.Linq;

namespace BusinessLogic.Validators
{
    public class GameValidator
    {
        private readonly IConfigProvider _configProvider;

        public GameValidator(IConfigProvider configProvider)
        {
            _configProvider = configProvider;
        }

        public bool IsGameValid(LadderContext dbContext, ApiGame gameModel)
        {
            if (gameModel.Players == null || gameModel.Players.Count == 0)
            {
                throw new ArgumentNullException("No players were found.");
            }

            if (GameWasReported(dbContext, gameModel.Identifier))
            {
                throw new InvalidOperationException("Such a game has already been reported in the RotWK League system.");
            }

            var allowUnknownMaps = _configProvider.GetAllowUnknownMapsSetting();
            var isKnownMap = dbContext.Map.AsQueryable().Any(x => x.ReplayMapTextId == gameModel.ReplayMapTextId);

            if (allowUnknownMaps && !isKnownMap)
            {
                gameModel.ReplayMapTextId = Constants.UnkownMapReplayMapTextId;
            }
            if (!allowUnknownMaps && !isKnownMap)
            {
                throw new NullReferenceException($"Map with given ID {gameModel.ReplayMapTextId} was not found. You are unable to play games on this map.");
            }

            if (gameModel.PlayedOn == DateTime.MinValue)
            {
                throw new ArgumentException("PlayedOn property of game was not initialized.");
            }

            return true;
        }

        private bool GameWasReported(LadderContext context, string gameIdentifier)
        {
            return context.Game.AsQueryable().Any(x => x.Identifier == gameIdentifier);
        }
    }
}
