﻿using Common.Model;
using DALMySql.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.PointsSystem
{
    public static class TeamRatingCalculator
    {
        private const int powerBasisElo = 10;
        private const double powerExponentDividerElo = 400;
        private const double factorElo = 32;
        private const int _maxPointsAllowedElo = 31;
        private const int _minPointsAllowedElo = 1;

        public static List<CalculationResult> GetTeamRatingsDeltas(List<Player> dbPlayers, List<ApiPlayer> players)
        {
            return ExtractWinnersAndLosers(dbPlayers, players, "TeamRating");
        }
        public static List<CalculationResult> GetMonthlyTeamRatingsDeltas(List<Player> dbPlayers, List<ApiPlayer> players)
        {
            return ExtractWinnersAndLosers(dbPlayers, players, "MonthlyTeamRating",
                Constants.LenienceRatingCap,
                Constants.BonusMultiplierCap,
                Constants.BonusDivisor);
        }

        private static List<CalculationResult> ExtractWinnersAndLosers(
            List<Player> dbPlayers,
            List<ApiPlayer> players,
            string ratingType,
            int LenienceRatingCap = 0,
            int BonusMultiplierCap = 0,
            int BonusDivisor = 1)
        {
            var winnerNicknames = players
                .Where(y => !y.IsLooser)
                .Select(x => x.Name)
                .ToList();
            var dbWinners = dbPlayers.Where(x => winnerNicknames.Contains(x.Name)).ToList();
            var sumOfWinnersTeamRating = dbWinners.Select(x => (double)GetPropValue(x, ratingType)).Sum();

            var loosersNicknames = players
                .Where(y => y.IsLooser)
                .Select(x => x.Name)
                .ToList();
            var dbLoosers = dbPlayers.Where(x => loosersNicknames.Contains(x.Name)).ToList();
            var sumOfLoosersTeamRating = dbLoosers.Select(x => (double)GetPropValue(x, ratingType)).Sum();

            return CalculateMonthlyTeamPointsDelta(
                dbLoosers,
                dbWinners,
                sumOfWinnersTeamRating / dbWinners.Count,
                sumOfLoosersTeamRating / dbLoosers.Count,
                ratingType,
                LenienceRatingCap,
                BonusMultiplierCap,
                BonusDivisor);
        }
        private static List<CalculationResult> CalculateMonthlyTeamPointsDelta(
            List<Player> dbLoosers,
            List<Player> dbWinners,
            double averageWinnersRating,
            double averageLoosersRating,
            string ratingType,
            int LenienceRatingCap,
            int BonusMultiplierCap,
            int BonusDivisor)
        {
            var winnersTeamPrice = Math.Pow(powerBasisElo, averageWinnersRating / powerExponentDividerElo);
            var loosersTeamPrice = Math.Pow(powerBasisElo, averageLoosersRating / powerExponentDividerElo);

            var delta = Math.Abs(factorElo * (1 - (winnersTeamPrice / (winnersTeamPrice + loosersTeamPrice))));
            var deltaRoundedUp = Math.Round(delta, 0);

            if (deltaRoundedUp > _maxPointsAllowedElo)
            {
                deltaRoundedUp = _maxPointsAllowedElo;
            }
            if (deltaRoundedUp < _minPointsAllowedElo)
            {
                deltaRoundedUp = _minPointsAllowedElo;
            }

            var results = new List<CalculationResult>();

            foreach (var loser in dbLoosers)
            {
                SetLosersTeamCalculationResults(
                    (double)GetPropValue(loser, ratingType),
                    loser.Id,
                    deltaRoundedUp,
                    results,
                    LenienceRatingCap);
            }

            foreach (var winner in dbWinners)
            {
                SetWinnersTeamCalculationResults(
                    (double)GetPropValue(winner, ratingType),
                    winner.Id,
                    deltaRoundedUp,
                    results,
                    BonusDivisor,
                    BonusMultiplierCap);
            }

            return results;
        }

        private static void SetLosersTeamCalculationResults(double loserRating, int loserId, double deltaRoundedUp, List<CalculationResult> calculationResults, int LenienceRatingCap)
        {
            deltaRoundedUp = Math.Floor(deltaRoundedUp);

            var loserUpdatedTeamRating = loserRating;
            if (loserRating > LenienceRatingCap)
            {
                if (loserRating - deltaRoundedUp < LenienceRatingCap)
                    loserUpdatedTeamRating = LenienceRatingCap;
                else
                    loserUpdatedTeamRating -= deltaRoundedUp;
            }

            var delta = loserRating - loserUpdatedTeamRating;
            calculationResults.Add(new CalculationResult() { PlayerId = loserId, Delta = -delta, IsLoser = true });

        }

        private static void SetWinnersTeamCalculationResults(double winnerRating, int winnerId, double deltaRoundedUp, List<CalculationResult> calculationResults, int BonusDivisor, int BonusMultiplierCap)
        {
            var winnerDelta = GetTeamWinnerDelta(winnerRating, deltaRoundedUp, BonusDivisor, BonusMultiplierCap);
            calculationResults.Add(new CalculationResult() { PlayerId = winnerId, Delta = (int)winnerDelta });
        }

        private static double GetTeamWinnerDelta(double winnerTeamRating, double deltaRoundedUp, int BonusDivisor, int BonusMultiplierCap)
        {
            return Math.Ceiling(
                deltaRoundedUp
                    + (deltaRoundedUp / BonusDivisor * Convert.ToInt32(winnerTeamRating < BonusMultiplierCap)));
        }
        /** 
         * Get an object's property by string. 
         * E.g. instead of going obj.name() you can go: GetPropValue(obj, "name")
         */
        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

    }
}
