﻿using BusinessLogic.PointsSystem;
using Common.Model;
using DALMySql.POCO;
using System;
using System.Collections.Generic;

namespace DodgeWars.BusinessLogic.Services
{
    public static class RatingCalculator
    {
        private const int powerBasisElo = 10;
        private const double powerExponentDividerElo = 400;
        private const double factorElo = 32;
        private const int _maxPointsAllowedElo = 31;
        private const int _minPointsAllowedElo = 1;

        public static List<CalculationResult> GetIndividualRatingsDeltas(List<Player> dbPlayers, List<ApiPlayer> players)
        {
            var loserObj = ExtractLoser(dbPlayers, players);
            var winnerObj = ExtractWinner(dbPlayers, players);

            return GetRatingCalcResults(
                winnerObj.Id,
                loserObj.Id,
                winnerObj.Rating,
                loserObj.Rating);
        }
        public static List<CalculationResult> GetMonthlyIndividualRatingsDeltas(List<Player> dbPlayers, List<ApiPlayer> players)
        {
            var loserObj = ExtractLoser(dbPlayers, players);
            var winnerObj = ExtractWinner(dbPlayers, players);

            return GetRatingCalcResults(
                winnerObj.Id,
                loserObj.Id,
                winnerObj.MonthlyRating,
                loserObj.MonthlyRating,
                Constants.LenienceRatingCap,
                Constants.BonusMultiplierCap,
                Constants.BonusDivisor);
        }
        private static List<CalculationResult> GetRatingCalcResults(
            int winnerID,
            int loserID,
            double winnerRating,
            double looserRating,
            int LenienceRatingCap = 0,
            int BonusMultiplierCap = 0,
            int BonusDivisor = 1)
        {
            var winnerPrice = Math.Pow(powerBasisElo, winnerRating / powerExponentDividerElo);
            var looserPrice = Math.Pow(powerBasisElo, looserRating / powerExponentDividerElo);

            var delta = Math.Abs(factorElo * (1 - (winnerPrice / (winnerPrice + looserPrice))));
            var deltaRoundedUp = Math.Round(delta, 0);

            if (deltaRoundedUp > _maxPointsAllowedElo)
            {
                deltaRoundedUp = _maxPointsAllowedElo;
            }
            if (deltaRoundedUp < _minPointsAllowedElo)
            {
                deltaRoundedUp = _minPointsAllowedElo;
            }

            var newDbLoserRating = looserRating - deltaRoundedUp;
            var loserDelta = Math.Floor(looserRating - newDbLoserRating);

            if (looserRating > LenienceRatingCap)
            {
                if (looserRating - loserDelta < LenienceRatingCap)
                {
                    loserDelta = Math.Abs(looserRating - LenienceRatingCap);
                }
            }
            else
            {
                loserDelta = 0;
            }

            var result = new List<CalculationResult>();
            var winnerDelta = deltaRoundedUp + (deltaRoundedUp / BonusDivisor * Convert.ToInt32(winnerRating < BonusMultiplierCap));
            result.Add(new CalculationResult() { PlayerId = loserID, Delta = -loserDelta, IsLoser = true });
            result.Add(new CalculationResult() { PlayerId = winnerID, Delta = Math.Ceiling(winnerDelta) });

            return result;
        }

        private static Player ExtractWinner(List<Player> dbPlayers, List<ApiPlayer> players)
        {
            var winnerNickname = players.Find(y => !y.IsLooser).Name;
            return dbPlayers.Find(x => x.Name == winnerNickname);
        }

        private static Player ExtractLoser(List<Player> dbPlayers, List<ApiPlayer> players)
        {
            var loserNickname = players.Find(y => y.IsLooser).Name;
            return dbPlayers.Find(x => x.Name == loserNickname);
        }
    }
}