﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.PointsSystem
{
    public class CalculationResult
    {
        public int PlayerId { get; set; }

        public double Delta { get; set; }

        public bool IsLoser { get; set; }
    }
}
