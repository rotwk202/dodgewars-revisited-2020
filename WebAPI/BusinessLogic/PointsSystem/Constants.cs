﻿namespace BusinessLogic.PointsSystem
{
    public static class Constants
    {
        public static double InitialRatingValue = 1600;
        public static double InitialMonthlyRatingValue = 1000;
        public static int LenienceRatingCap = 1200;
        public static int BonusMultiplierCap = 1500;
        public static int BonusDivisor = 5;
        public static string ParmanentLadderType = "Permanent";
        public static string MonthlyLadderType = "Monthly";
    }
}
