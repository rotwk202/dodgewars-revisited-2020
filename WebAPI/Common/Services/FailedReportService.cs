﻿using Common.Model;
using DALMySql;
using DALMySql.POCO;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Services
{
    public class FailedReportService
    {
        private readonly LadderContext _dbContext;

        public FailedReportService()
        {
        }

        public FailedReportService(LadderContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IList<ApiFailedReport> GetAllEntries()
        {
            var reports = _dbContext.FailedReport.AsQueryable().OrderByDescending(x => x.OccuredOn).ToList();
            var result = new List<ApiFailedReport>();

            foreach (var report in reports)
            {
                result.Add(new ApiFailedReport()
                {
                    GameId = report.GameId,
                    Message = report.Message,
                    OccuredOn = report.OccuredOn
                });
            }

            return result;
        }

        public void AddEntry(string gameIdentifier, string message, string players = null)
        {
            var reports = _dbContext.FailedReport.AsQueryable().ToList();

            if (gameIdentifier != null && reports.Any(x => x.GameId == gameIdentifier))
            {
                return;
            }

            if(string.IsNullOrEmpty(gameIdentifier))
            {
                gameIdentifier = "Not specified";
            }

            var entry = new FailedReport()
            {
                GameId = gameIdentifier,
                Message = message,
                OccuredOn = DateTime.Now
            };

            _dbContext.FailedReport.Add(entry);
            _dbContext.SaveChanges();
        }
    }
}
