﻿using Common.Interfaces;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace Common.Services
{
    public class MySqlConnectionProvider : ISqlConnectionProvider
    {
        private readonly IConfiguration _config;

        public MySqlConnectionProvider(IConfiguration config)
        {
            _config = config;
        }

        public MySqlConnection GetConnection()
        {
            var connString = _config.GetConnectionString("Default");

            return new MySqlConnection(connString);
        }
    }
}
