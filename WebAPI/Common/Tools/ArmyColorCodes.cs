﻿using System;

namespace Common.Tools
{
    public static class ArmyColorCodes
    {
        public static string GetArmyColorCode(string armyName)
        {
            if (string.IsNullOrEmpty(armyName))
            {
                throw new ArgumentException("armyName was null or empty.");
            }

            switch (armyName)
            {
                case Constants.Men:
                    return "#0a4dc9";
                case Constants.Elves:
                    return "#458c0e";
                case Constants.Dwarves:
                    return "#b5b80d";
                case Constants.Mordor:
                    return "#b02f12";
                case Constants.Isengard:
                    return "#878483";
                case Constants.Goblins:
                    return "#a1660d";
                case Constants.Angmar:
                    return "#09aba8";
                default:
                    return "#ffffff";
            }
        }
    }
}
