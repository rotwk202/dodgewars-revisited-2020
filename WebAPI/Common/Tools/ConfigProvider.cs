﻿using Common.Interfaces;
using Common.Model;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace Common.Tools
{
    public class ConfigProvider : IConfigProvider
    {
        IConfiguration _config;

        public ConfigProvider(IConfiguration config)
        {
            _config = config;
        }

        public bool GetAllowUnknownMapsSetting()
        {
            return _config.GetValue<bool>("AllowUnknownMaps");
        }

        public IList<RotwkPatch> GetRotwkPatchesSetting()
        {
            return _config.GetSection("RotwkPatches").Get<RotwkPatch[]>().ToList();
        }
    }
}
