﻿namespace Common
{
    public static class Constants
    {
        public static string UnkownMapReplayMapTextId = "unknown";

        public const string Men = "Men";
        public const string Elves = "Elves";
        public const string Dwarves = "Dwarves";
        public const string Mordor = "Mordor";
        public const string Isengard = "Isengard";
        public const string Goblins = "Goblins";
        public const string Angmar = "Angmar";
        public const string Random = "Random";
        public const string Observer = "Observer";
        public static string[] AllArmies = { Men, Elves, Dwarves, Mordor, Isengard, Goblins, Angmar };
        
        public const string ParserClientName = "console";
        public const string WebClientName = "web";
        public const string SecretAdmin = "adminPasswordUser";
    }
}
