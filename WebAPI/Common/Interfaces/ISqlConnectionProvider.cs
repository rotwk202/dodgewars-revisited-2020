﻿using MySql.Data.MySqlClient;

namespace Common.Interfaces
{
    public interface ISqlConnectionProvider
    {
        MySqlConnection GetConnection();
    }
}
