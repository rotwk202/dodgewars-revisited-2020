﻿using Common.Model;
using System.Collections.Generic;

namespace Common.Interfaces
{
    public interface IConfigProvider
    {
        bool GetAllowUnknownMapsSetting();
        IList<RotwkPatch> GetRotwkPatchesSetting();
    }
}
