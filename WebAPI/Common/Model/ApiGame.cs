﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Model
{
    public class ApiGame
    {
        public string Identifier { get; set; }

        public List<ApiPlayer> Players { get; set; }

        public DateTime PlayedOn { get; set; }

        public string ReplayMapTextId { get; set; }

        public byte[] ReplayFile { get; set; }

        public int LastReplayFileLength { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            var gameId = $"Game identifier: {Identifier}";
            var playedOn = $"Played On: {PlayedOn}";
            var mapId = $"Replay map text it: {ReplayMapTextId}";
            var players = $"Players: {JsonConvert.SerializeObject(Players)}";

            sb.Append(gameId);
            sb.Append(Environment.NewLine);
            sb.Append(playedOn);
            sb.Append(Environment.NewLine);
            sb.Append(mapId);
            sb.Append(Environment.NewLine);
            sb.Append(players);

            return sb.ToString();
        }
    }
}
