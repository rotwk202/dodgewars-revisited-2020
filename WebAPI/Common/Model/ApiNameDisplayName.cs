﻿namespace Common.Model
{
    public class ApiNameDisplayName
    {
        public string InReplayName { get; set; }
        public string DisplayName { get; set; }
    }
}
