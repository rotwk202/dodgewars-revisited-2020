﻿namespace Common.Model
{
    public class ApiGamesCountData
    {
        public int GamesCurrentMonthCount { get; set; }

        public int GamesAllTimeCount { get; set; }
    }
}
