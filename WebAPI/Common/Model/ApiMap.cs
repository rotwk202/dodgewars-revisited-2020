﻿namespace Common.Model
{
    public class ApiMap
    {
        public string CodeName { get; set; }
        public string DisplayName { get; set; }
        public string ReplayMapTextId { get; set; }
        public byte[] Image { get; set; }
    }
}
