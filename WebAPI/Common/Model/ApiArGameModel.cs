﻿using System;
using System.Collections.Generic;

namespace Common.Model
{
    public class ApiArGameModel
    {
        public DateTime PlayedOn { get; set; }
        public List<ApiPlayer> Players { get; set; }
    }
}
