﻿namespace Common.Model
{
    public class ApiPlayer
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }

        public int Team { get; set; }

        public bool IsLooser { get; set; }

        public int ArmyReplayId { get; set; }

        public string Army { get; set; }
        
        public string ArmyColorCode { get; set; }

        public int Color { get; set; }

        public int Spot { get; set; }

        public bool IsObserver { get; set; }

        public bool? IsGameHost { get; set; }

        public double Delta { get; set; }
    }
}
