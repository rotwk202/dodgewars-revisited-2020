﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Common.Model.Statistics
{

    public class PlayerMedalDetails
    {
        public string PlayerName { get; set; }
        public string Season { get; set; }

        public MedalModel Activity { get; set; }
        public MedalModel TotalWins { get; set; }
        public MedalModel Ambition { get; set; }

        //public MedalStage TotalPlayerWinsMedal { get; set; }

        public Dictionary<MedalStage, double> TotalPlayersWinsMedalStagesDict { get; set; }
    }
}
