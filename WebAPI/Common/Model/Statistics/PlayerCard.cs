﻿using System.Collections.Generic;

namespace Common.Model.Statistics
{
    public class PlayerCard
    {
        public string PlayerName { get; set; }
        public double PermaIndividualRating { get; set; }
        public double PermaTeamRating { get; set; }
        public double MonthlyIndividualRating { get; set; }
        public double MonthlyTeamRating { get; set; }
        public int PermaIndividualWinsCount { get; set; }
        public int PermaIndividualLosesCount { get; set; }
        public int PermaTeamWinsCount { get; set; }
        public int PermaTeamLosesCount { get; set; }

        public double PermaIndividualWinRatio { get; set; }
        public double PermaTeamWinRatio { get; set; }
        public IList<PlayerOpponentInfo> PlayerOpponents { get; set; }

        public PlayerMedalDetails PlayerMedalDetails { get; set; }
    }
}
