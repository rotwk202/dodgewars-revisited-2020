﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Common.Model.Statistics
{
    public enum MedalStage
    {
        Bronze_1,
        Bronze_2,
        Bronze_3,
        Silver_1,
        Silver_2,
        Silver_3,
        Gold_1,
        Gold_2,
        Gold_3,
    }
}
