﻿using System;

namespace Common.Model.Statistics
{
    public class ActivityStat
    {
        public string DateString { get; set; }

        public int GamesCount { get; set; }
    }
}
