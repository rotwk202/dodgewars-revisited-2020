﻿namespace Common.Model.Statistics
{
    public class MedalModel
    {
        public MedalModel(int level, double progressPercentage, MedalStage medalCode)
        {
            Level = level;
            ProgressPercentage = progressPercentage;
            MedalCode = medalCode;
        }

        public int Level { get; set; }
        public double ProgressPercentage { get; set; }
        public MedalStage MedalCode { get; set; }
    }
}
