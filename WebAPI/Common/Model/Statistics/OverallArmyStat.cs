﻿namespace Common.Model.Statistics
{
    public class OverallArmyStat
    {
        public string Army { get; set; }
        public double WinPercentage { get; set; }
    }
}
