﻿namespace Common.Model.Statistics
{
    public class PlayerOpponentInfo
    {
        public string PlayerName { get; set; }

        public int WonAgainst { get; set; }
        
        public int LostTo { get; set; }
    }
}
