﻿using System;
using System.Collections.Generic;

namespace Common.Model
{
    public class RecentGame
    {
        public DateTime PlayedOn { get; set; }

        public int GamePrimaryKeyId { get; set; }

        public string GameId { get; set; }
        public int DisplayGameId { get; set; }

        public string MapCodeName { get; set; }

        public string MapDisplayName { get; set; }

        public bool IsNeutralHost { get; set; }

        public List<ApiPlayer> Players { get; set; }
    }
}
