﻿using System;

namespace Common.Model
{
    public class RotwkPatch
    {
        public string Version { get; set; }
        public DateTime StartDate { get; set; }
    }
}
