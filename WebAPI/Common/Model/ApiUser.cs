﻿namespace Common.Model
{
    public class ApiUser
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string Token { get; set; }

        public string TokenExpiresOn { get; set; }

        public string InitialNickname { get; set; }
        public string InitialDisplayName { get; set; }
    }

}
