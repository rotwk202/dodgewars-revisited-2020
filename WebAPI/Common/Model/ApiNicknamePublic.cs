﻿namespace Common.Model
{
    public class ApiNicknamePublic
    {
        public string Nickname { get; set; }

        public int AccountID { get; set; }
    }
}
