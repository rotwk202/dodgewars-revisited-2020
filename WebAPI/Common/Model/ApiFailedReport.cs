﻿using System;

namespace Common.Model
{
    public class ApiFailedReport
    {
        public string Message { get; set; }

        public string GameId { get; set; }

        public DateTime OccuredOn { get; set; }
    }
}
