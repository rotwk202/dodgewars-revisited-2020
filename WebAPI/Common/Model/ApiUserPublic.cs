﻿using System;

namespace Common.Model
{
    public class ApiUserPublic
    {
        public int Id { get; set; }

        public string AccountName { get; set; }

        public DateTime CreationDate { get; set; }
    }

}
