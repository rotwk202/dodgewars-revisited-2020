﻿namespace Common.Model
{
    public class QueryResult
    {
        public string Name { get; set; }
        public double Rating { get; set; }
        public int WinsCount { get; set; }
        public int LosesCount { get; set; }
    }
}
