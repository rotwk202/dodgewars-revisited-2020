﻿namespace ReplayParserTools.Model
{
    public class ApiNickname
    {
        public int UserId { get; set; }

        public string Nickname { get; set; }
    }
}
