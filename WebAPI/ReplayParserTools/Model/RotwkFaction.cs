using System.Collections.Generic;
using System.Linq;
using Common;

namespace ReplayParser.Tools.Model
{
    public class RotwkFaction
    {
        private static readonly Dictionary<int, string> _factions = new Dictionary<int, string>
        {
            {-2, Constants.Observer},
            {-1, Constants.Random},
            {3, Constants.Men},
            {5, Constants.Elves},
            {6, Constants.Dwarves},
            {7, Constants.Isengard},
            {8, Constants.Mordor},
            {9, Constants.Goblins},
            {10, Constants.Angmar}
        };

        private int _identifier;

        private RotwkFaction(int id)
        {
            _identifier = id;
        }

        public static RotwkFaction FromName(string factionName)
        {
            var faction = new RotwkFaction(_factions.FirstOrDefault(x => x.Value.Contains(factionName)).Key);
            return faction;
        }

        public static RotwkFaction FromReplayID(int factionId)
        {
            var faction = new RotwkFaction(factionId);
            return faction;
        }

        public static RotwkFaction FromRandom(int factionId)
        {
            var i = 0;
            foreach (var item in GetPlayableFactions())
            {
                if (factionId == i)
                {
                    return new RotwkFaction(item.Key);
                }
                i++;
            }
            return null;
        }

        public static int GetNumberOfPlayableFactions()
        {
            return GetPlayableFactions().Count();
        }

        private static IEnumerable<KeyValuePair<int, string>> GetPlayableFactions()
        {
            return _factions.Where(pair => pair.Key > 0);
        }

        public int GetRawIdentifier()
        {
            return _identifier;
        }

        public bool IsObserver()
        {
            return FromName(Constants.Observer).GetRawIdentifier() == _identifier;
        }

        public bool IsRandom()
        {
            return FromName(Constants.Random).GetRawIdentifier() == _identifier;
        }

        public bool IsPlayable()
        {
            return _identifier > 0;
        }

        public override string ToString()
        {
            return _factions[_identifier];
        }
    }
}
