using Common.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using ReplayParser.Tools.Services;
using ReplayParser.Tools.Model;
using System.Security.Cryptography;
using System.Numerics;

namespace ReplayParserTools.ParserTools
{
    public class ReplayParser : IDisposable
    {
        private const string validFileType = "BFME2RPL";
        private const int _nickIndex = 0;
        private const int _teamIndex = 7;
        private const int _armyIndex = 5;
        private const int _colorIndex = 4;
        private const int _spotIndex = 6;

        private Stream _fileStream;
        private BinaryReader _reader;

        public ReplayParser(Stream replayFileStream)
        {
            CreateReader(replayFileStream);
        }

        private void CreateReader(Stream replayFileStream)
        {
            _fileStream = new MemoryStream();
            //replayFileStream.CopyTo(_fileStream);
            replayFileStream.Seek(0L, SeekOrigin.Begin);
            replayFileStream.CopyTo(_fileStream);
            replayFileStream.Seek(0L, SeekOrigin.Begin);
            _fileStream.Seek(0L, SeekOrigin.Begin);
            _reader = new BinaryReader(_fileStream, Encoding.ASCII);
        }

        public void Dispose()
        {
            _reader?.Dispose();
            _fileStream?.Dispose();
        }

        public ReplayData Parse()
        {
            var replayData = new ReplayData();

            string fileType = new string(_reader.ReadChars(8));
            if (fileType != validFileType)
            {
                return replayData;
            }

            replayData.ReplayFileType = fileType;
            replayData.TimestampStart = _reader.ReadUInt32();
            replayData.TimestampEnd = _reader.ReadUInt32();
            if (replayData.TimestampEnd == 0)
            {
                replayData.TimestampEnd = replayData.TimestampStart;
            }

            replayData.Unknown1 = _reader.ReadBytes(21);
            replayData.FileName = Read2ByteString();

            replayData.DateTimeArray[0] = _reader.ReadUInt16();
            replayData.DateTimeArray[1] = _reader.ReadUInt16();
            replayData.DateTimeArray[2] = _reader.ReadUInt16();
            replayData.DateTimeArray[3] = _reader.ReadUInt16();
            replayData.DateTimeArray[4] = _reader.ReadUInt16();
            replayData.DateTimeArray[5] = _reader.ReadUInt16();
            replayData.DateTimeArray[6] = _reader.ReadUInt16();
            replayData.DateTimeArray[7] = _reader.ReadUInt16();

            replayData.Version = Read2ByteString();
            replayData.BuildDate = Read2ByteString();
            replayData.VersionMinor = _reader.ReadUInt16();
            replayData.VersionMajor = _reader.ReadUInt16();

            replayData.MagicHash = _reader.ReadBytes(13);

            replayData.HeaderRawData = Read1ByteString();

            if (!string.IsNullOrWhiteSpace(replayData.HeaderRawData))
            {
                ParseHeader(replayData);
            }

            replayData.Unknown2 = _reader.ReadUInt16();
            replayData.Unknown3[0] = _reader.ReadUInt32();
            replayData.Unknown3[1] = _reader.ReadUInt32();
            replayData.Unknown3[2] = _reader.ReadUInt32();
            replayData.Unknown3[3] = _reader.ReadUInt32();
            replayData.Unknown3[4] = _reader.ReadUInt32();
            replayData.Unknown3[5] = _reader.ReadUInt32();

            CalculateGameIdentifyingHash(_reader, replayData);

            return replayData;
        }

        /// Hash of player independent data.
        /// Multiple replays of the same game should result in the same hash. 
        /// Since C# doesn't have native crc32, I use md5 (128 bits) and then modulo it to 32 bits.
        private void CalculateGameIdentifyingHash(BinaryReader reader, ReplayData replayData)
        {
            var md5 = IncrementalHash.CreateHash(HashAlgorithmName.MD5);

            md5.AppendData(Encoding.ASCII.GetBytes(replayData.HeaderRawData));

            if (reader.BaseStream.Length - reader.BaseStream.Position > 10000)
            {
                md5.AppendData(_reader.ReadBytes(10000));
            }
            
            replayData.IdentifyingHash = ByteArrayToString(md5.GetHashAndReset());
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);

            for (int i = 0; i < ba.Length; i++) // <-- Use for loop is faster than foreach   
            {
                hex.Append(ba[i].ToString("X2"));  // <-- ToString is faster than AppendFormat   
            }

            return hex.ToString();
        }

        private void ParseHeader(ReplayData replayData)
        {
            string[] headerRawData = replayData.HeaderRawData.Split(';');

            var headerFields = new HeaderFields
            {
                MapName = headerRawData[0],
                MapCrc = headerRawData[1].Replace("MC=", string.Empty),
                MapFileSize = headerRawData[2].Replace("MS=", string.Empty),
                Seed = headerRawData[3].Replace("SD=", string.Empty),
                MatchId = headerRawData[4].Replace("GSID=", string.Empty),
                Gt = headerRawData[5].Replace("GT=", string.Empty),
                Si = headerRawData[6].Replace("SI=", string.Empty),
                Gr = headerRawData[7].Replace("GR=", string.Empty),
                Unknown1 = headerRawData[9]
            };

            headerFields.Players = ParsePlayers(headerRawData[8]);

            try
            {
                var parsedSeed = int.Parse(headerFields.Seed);

                if (parsedSeed > 0)
                {
                    headerFields.Players = ExtractRandomFactionsFromPlayerInfo(
                        (uint)parsedSeed,
                        headerFields.Players,
                        headerFields.Players.Any(x => x.Spot >= 0));
                }
                else
                {
                    foreach (var player in headerFields.Players)
                    {
                        player.Army = RotwkFaction.FromReplayID(player.ArmyReplayId).ToString();
                    }
                }
            }
            catch (System.FormatException e)
            {
                var e2 = new Exception("SD parameter (random seed) in replay file has invalid format. Not a valid uint32: " + headerFields.Seed);
                e2.Data.Add("Original exception", e);
                throw e2;
            }

            replayData.HeaderFields = headerFields;
        }

        private List<ApiPlayer> ParsePlayers(string playerData)
        {
            var players = new List<ApiPlayer>();

            if (string.IsNullOrWhiteSpace(playerData) || string.Equals(playerData.Substring(1, 2), "S=", StringComparison.InvariantCultureIgnoreCase))
            {
                return players;
            }

            foreach (string token in playerData.Substring(2, playerData.Length - 2).Split(':'))
            {
                if (string.IsNullOrWhiteSpace(token))
                {
                    continue;
                }

                bool isPlayer = string.Equals(token.Substring(0, 1), "H", StringComparison.InvariantCultureIgnoreCase);

                if (!isPlayer)
                {
                    continue;
                }

                players.Add(ParsePlayerData(token));
            }
            return players;
        }

        private ApiPlayer ParsePlayerData(string token)
        {
            var playerData = token.Split(',');
            return new ApiPlayer
            {
                Name = playerData[_nickIndex].Substring(1, playerData[_nickIndex].Length - 1),
                ArmyReplayId = int.Parse(playerData[_armyIndex]),
                Team = int.Parse(playerData[_teamIndex]),
                Color = int.Parse(playerData[_colorIndex]),
                Spot = int.Parse(playerData[_spotIndex])
            };
        }

        private string ParseHexIPAddress(string ipHexString)
        {
            uint ipAsUint = uint.Parse(ipHexString, NumberStyles.AllowHexSpecifier);

            if (BitConverter.IsLittleEndian)
            {
                ipAsUint = BitConverter.ToUInt32(BitConverter.GetBytes(ipAsUint).Reverse().ToArray(), 0);
            }

            return new IPAddress(ipAsUint).ToString();
        }

        private string Read1ByteString()
        {
            var stringBuilder = new StringBuilder();

            while (true)
            {
                var character = _reader.ReadChar();

                if (character == '\0')
                {
                    break;
                }

                stringBuilder.Append(character);
            }

            return stringBuilder.ToString();
        }

        private string Read2ByteString()
        {
            var stringBuilder = new StringBuilder();

            while (true)
            {
                short characterValue = _reader.ReadInt16();

                if (characterValue == 0)
                {
                    break;
                }

                stringBuilder.Append(Convert.ToChar(char.ConvertFromUtf32(characterValue)));
            }

            return stringBuilder.ToString();
        }
        private List<ApiPlayer> ExtractRandomFactionsFromPlayerInfo(
             uint seed,
             List<ApiPlayer> players,
             bool spotsAreManual
         )
        {
            var rng = new RotwkRngService(seed);

            //If no spots are set, trigger one random event simulating the first player's position being picked
            if (!spotsAreManual)
            {
                rng.NextRng();
            }

            var occupiedColors = new HashSet<int>();
            foreach (var player in players)
            {
                if (player.Color >= 0) // -1 means random
                    occupiedColors.Add(player.Color);
            }

            //Each observer also triggers a random spot event
            var obsCount = players.Count(p => RotwkFaction.FromReplayID(p.ArmyReplayId).IsObserver());
            RepeatAction(obsCount, () => rng.NextRng());

            var skip = (int)seed % 7;

            //Resolve factions and colors for all players who didn't pick them
            foreach (var player in players)
            {
                var factionInReplay = RotwkFaction.FromReplayID(player.ArmyReplayId);

                if (factionInReplay.IsRandom())
                {
                    // RNG skip the engine does, we don't know why.
                    RepeatAction(skip, () => rng.NextRng());

                    var resolvedFaction = rng.NextRandomFaction();
                    player.Army = resolvedFaction.ToString();
                }
                else
                {
                    player.Army = factionInReplay.ToString();
                }

                if (player.Color == -1)
                {
                    while (true)
                    {
                        var randomColor = rng.NextRngInRange(0, 11 - 1);
                        if (!occupiedColors.Contains(randomColor))
                        {
                            occupiedColors.Add(randomColor);
                            player.Color = randomColor;

                            break;
                        }
                    }
                }
            }

            return players;
        }

        private void RepeatAction(int repeatCount, Action action)
        {
            for (int i = 0; i < repeatCount; i++)
            {
                action();
            }
        }
    }
}