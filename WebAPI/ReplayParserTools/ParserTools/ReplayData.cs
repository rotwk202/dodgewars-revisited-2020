﻿namespace ReplayParserTools.ParserTools
{
    public class ReplayData
    {
        public string ReplayFileType;

        public uint TimestampStart;

        public uint TimestampEnd;

        public byte[] Unknown1;

        public string FileName;

        public ushort[] DateTimeArray = new ushort[8];

        public string Version;

        public string BuildDate;

        public ushort VersionMinor;

        public ushort VersionMajor;

        public byte[] MagicHash;

        public string HeaderRawData;

        public HeaderFields HeaderFields;

        public ushort Unknown2;

        public uint[] Unknown3 = new uint[6];

        public string IdentifyingHash;

        // TODO: command structure
    }
}
