﻿using Common.Model;
using System.Collections.Generic;

namespace ReplayParserTools.ParserTools
{

    public class HeaderFields
    {
        public string MapName; // M=

        public string MapCrc; // MC=

        public string MapFileSize; // MS=

        public string Seed; // SD=

        public string MatchId; //GSID= // GameSpy (Match) ID

        public string Gt;

        public string Gr;

        public string Si;

        public string Unknown1;

        public List<ApiPlayer> Players;
    }
}
