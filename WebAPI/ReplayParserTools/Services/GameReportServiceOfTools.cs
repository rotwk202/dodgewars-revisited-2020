﻿using Common.Model;
using Common.Services;
using DALMySql;
using log4net;
using log4net.Repository;
using ReplayParserTools.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace ReplayParserTools.Services
{
    public class GameReportServiceOfTools
    {
        private List<ApiPlayer> _playersInGame;
        private string _reporterNickname;
        private readonly GameDataService _gameDataService;
        private readonly LadderContext _dbContext;
        private readonly FailedReportService _failedReportService;
        private readonly ILog _logger;
        private const string repoName = "repo";
        private string _gameIdentifier;

        public GameReportServiceOfTools(LadderContext dbContext, FailedReportService failedReportService)
        {
            _gameDataService = new GameDataService();
            _dbContext = dbContext;
            _failedReportService = failedReportService;
            ILoggerRepository repo = null;
            if(!LogManager.GetAllRepositories().Select(x => x.Name).ToList().Any(x => x == repoName))
            {
                repo = LogManager.CreateRepository(repoName);
            }
            _logger = LogManager.GetLogger(repoName, "InfoLogger");
        }

        public ApiGame GetApiGame(Stream replayFileStream, string userEmail)
        {
            _logger.Info($"Going to parse replay I got from user {userEmail}. File stream length: {replayFileStream.Length} and position: {replayFileStream.Position}");
            _gameDataService.LoadReplayData(replayFileStream);
            _playersInGame = _gameDataService.ReplayData.HeaderFields.Players;
            _gameIdentifier = _gameDataService.ReplayData.HeaderFields.Seed;

            _gameDataService.SetGameHostProperties(_playersInGame);
            _gameDataService.SetIsObserverProperties(_playersInGame);

            if (IsReportValid(userEmail))
            {
                return GetReportModel(replayFileStream);
            }

            return null;
        }

        private bool IsReportValid(string userEmail)
        {
            var user = _dbContext.User.AsQueryable().FirstOrDefault(x => x.Email.Equals(userEmail));

            if (user is null)
            {
                throw new KeyNotFoundException($"User with given login {userEmail} was not found");
            }

            var userNicknames = _dbContext.Player.AsQueryable().Where(x => x.UserId == user.Id).Select(x => x.Name).ToList();
            List<ApiNickname> nicknames = new List<ApiNickname>();

            foreach (var un in userNicknames)
            {
                nicknames.Add(new ApiNickname() { UserId = user.Id, Nickname = un });
            }

            if (_gameDataService.IsFFAGame(_playersInGame))
            {
                throw new InvalidOperationException("You're unable to report FFA games.");
            }

            SetReporterNickname(nicknames, _playersInGame);
            _gameDataService.DetermineGameLoosers(_reporterNickname, _playersInGame);

            return true;
        }

        private ApiGame GetReportModel(Stream replayFileStream)
        {
            var apiGame = new ApiGame();

            apiGame.PlayedOn = DateTimeOffset.FromUnixTimeSeconds(_gameDataService.ReplayData.TimestampEnd).UtcDateTime;
            apiGame.LastReplayFileLength = (int)replayFileStream.Length;
            Debug.WriteLine($"casted hash: {_gameDataService.ReplayData.IdentifyingHash}");
            apiGame.Identifier = _gameDataService.ReplayData.IdentifyingHash;
            apiGame.ReplayMapTextId = _gameDataService.ReplayData.HeaderFields.MapName.Replace("s/", string.Empty);
            apiGame.Players = _playersInGame;

            replayFileStream.Seek(0L, SeekOrigin.Begin);
            using (var memoryStream = new MemoryStream())
            {
                replayFileStream.CopyTo(memoryStream);
                memoryStream.Seek(0L, SeekOrigin.Begin);
                apiGame.ReplayFile = memoryStream.ToArray();
            }

            return apiGame;
        }

        private void SetReporterNickname(List<ApiNickname> userNicknames, List<ApiPlayer> gamePlayers)
        {
            if (userNicknames == null || userNicknames.Count == 0)
            {
                throw new ArgumentNullException("userNicknames", "No nicknames bound to this dodgewars account were found.");
            }
            
            string combinames = String.Join(Environment.NewLine, gamePlayers.Select(x => "- " + x.Name).ToArray());
            string combinames2 = String.Join(Environment.NewLine, userNicknames.Select(x => "- " + x.Nickname).ToArray());
            string combinames3 = String.Join(", ", gamePlayers.Select(x => x.Name).ToList());
            
            foreach (var nickname in userNicknames.Select(x => x.Nickname).ToList())
            {
                if (gamePlayers.Any(x => x.Name == nickname))
                {
                    _reporterNickname = nickname;
                }
            }
            if (string.IsNullOrEmpty(_reporterNickname))
            {
                var exMessage = $"This replay was played by: {combinames}. You're not present in this replay! Your nicknames: {Environment.NewLine}{combinames2}";
                var dbMessage = $"This replay was played by: {combinames3}. You're not present in this replay!";
                _failedReportService.AddEntry(_gameIdentifier, dbMessage);
                throw new InvalidOperationException(exMessage);
            }
        }
    }
}
