using System;
using ReplayParser.Tools.Model;

namespace ReplayParser.Tools.Services
{
    public class RotwkRngService
    {
        private const ulong _mask = 0xFFFFFFFF;
        private const uint _mult = 134775813;
        private const uint _incr = 1;

        private uint _currentSeed;

        public RotwkRngService(uint initialSeed)
        {
            _currentSeed = PerformMask((ulong)initialSeed * 0x7FFFFFED);
        }

        public uint NextRng()
        {
            // Generate the xorfactor
            var xorFactor = (ulong)_currentSeed * _mult >> 32;
            //Just for safety in case the bitshift shifts in 1s on the most significant side
            var xorFactorMasked = PerformMask(xorFactor);

            // Update the seed
            _currentSeed = PerformMask((ulong)(_currentSeed * _mult + _incr));

            // Return random value
            return _currentSeed ^ xorFactorMasked;
        }

        public int NextRngInRange(int start, int end)
        {
            var next = NextRng();
            long modu = next % (end - start + 1);

            return start + (int)modu;
        }

        public RotwkFaction NextRandomFaction()
        {
            var r = NextRngInRange(0, 1000);
            var faction = r % RotwkFaction.GetNumberOfPlayableFactions();

            return RotwkFaction.FromRandom(faction); ;
        }
        private uint PerformMask(ulong largenumber)
        {
            return (uint)(largenumber & _mask);
        }

        public uint GetSeed()
        {
            return _currentSeed;
        }
    }
}
