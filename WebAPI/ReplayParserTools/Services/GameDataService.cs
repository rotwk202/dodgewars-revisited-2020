﻿using Common.Model;
using ReplayParserTools.Model;
using ReplayParserTools.ParserTools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ReplayParserTools.Services
{
    public class GameDataService
    {
        private const int _observerIdentifier = -2;
        private const int _notSetTeamIdentifier = -1;
        private ReplayData _replayData;

        public ReplayData ReplayData { get { return _replayData; } }

        public void LoadReplayData(Stream replayFileStream)
        {

            using (var replayParser = new ParserTools.ReplayParser(replayFileStream))
            {
                _replayData = replayParser.Parse();
            }
        }

        public void SetGameHostProperties(List<ApiPlayer> playersInGame)
        {
            var firstPlayerInReplayFile = playersInGame.FirstOrDefault();
            firstPlayerInReplayFile.IsGameHost = true;

            foreach (var p in playersInGame)
            {
                if (p.Name != firstPlayerInReplayFile.Name)
                {
                    p.IsGameHost = false;
                }
            }
        }

        public void SetIsObserverProperties(List<ApiPlayer> playersInGame)
        {
            foreach (var player in playersInGame)
            {
                player.IsObserver = player.ArmyReplayId == _observerIdentifier;
            }
        }

        public bool IsFFAGame(List<ApiPlayer> playersInGame)
        {
            var activeGameParticipants = playersInGame.Count(x => x.ArmyReplayId != _observerIdentifier);

            var duplicateTeamsDifferentThanNonSet = playersInGame
              .Where(x => x.Team != _notSetTeamIdentifier)
              .GroupBy(x => x.Team)
              .Where(g => g.Count() > 1)
              .Select(y => y.Key)
              .ToList();

            var pairFound = duplicateTeamsDifferentThanNonSet.Count > 0;

            var moreThanTwoTeams = playersInGame
              .Where(x => x.Team != _notSetTeamIdentifier)
              .Select(x => x.Team)
              .Distinct().Count() > 2;

            var isFFAGAme = activeGameParticipants > 2
                && !pairFound
                && !moreThanTwoTeams;

            return isFFAGAme;
        }

        public void DetermineGameLoosers(string reporterNickname, List<ApiPlayer> playersInGame)
        {
            if (string.IsNullOrEmpty(reporterNickname))
            {
                throw new ArgumentNullException(nameof(reporterNickname));
            }
            if (!playersInGame.Any(x => x.Name == reporterNickname))
            {
                throw new InvalidOperationException("You probably have tried to cheat/troll someone. Please make sure your in-game name matches one of your nicknames registered in the RotWK League system.");
            }

            var activeGameParticipants = playersInGame.Count(x => x.ArmyReplayId != _observerIdentifier);
            bool isOneVsOneMatch = activeGameParticipants == 2;

            if (isOneVsOneMatch)
            {
                var loosingPlayerNickname = playersInGame.FirstOrDefault(x => x.Name == reporterNickname && !x.IsObserver);

                if (loosingPlayerNickname is null)
                {
                    throw new InvalidOperationException("You're unable to report games which you observed.");
                }

                foreach (var player in playersInGame)
                {
                    player.IsLooser = player.Name == loosingPlayerNickname.Name;
                }
            }
            else
            {
                var firstLosingPlayer = playersInGame.FirstOrDefault(x => x.Name == reporterNickname);

                if (firstLosingPlayer != null && firstLosingPlayer.IsObserver)
                {
                    throw new InvalidOperationException("You're unable to report games which you observed.");
                }

                foreach (var player in playersInGame)
                {
                    player.IsLooser = player.Team == firstLosingPlayer.Team;
                }
            }
        }

        public bool IsSelfReport(List<ApiPlayer> playersInGame, List<ApiNickname> userNicknames)
        {
            var gameLoosers = playersInGame
                .Where(x => x.IsLooser && !x.IsObserver)
                .ToList();
            var gameWinners = playersInGame
                .Where(x => !x.IsLooser && !x.IsObserver)
                .ToList();

            var userDbNicknames = userNicknames.Select(x => x.Nickname).ToList();

            bool occuredOnLoosersSite = false;
            bool occuredOnWinnersSite = false;

            //occured on loosers site
            occuredOnLoosersSite = gameLoosers.Any(x => userDbNicknames.Contains(x.Name));

            //occured on winners site
            occuredOnWinnersSite = gameWinners.Any(x => userDbNicknames.Contains(x.Name));

            return occuredOnLoosersSite && occuredOnWinnersSite;
        }
    }
}
