﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace DAL.POCO
{
    public class User
    {
        public User()
        {
            Players = new Collection<Player>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime CreatedOn { get; set; }
        public string TokenConsole { get; set; }
        public DateTime? TokenConsoleExpiresOn { get; set; }
        public string TokenWeb { get; set; }
        public DateTime? TokenWebExpiresOn { get; set; }

        public virtual ICollection<Player> Players { get; set; }
    }
}
