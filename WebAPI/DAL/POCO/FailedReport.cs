﻿using System;

namespace DAL.POCO
{
    public class FailedReport
    {
        public int Id { get; set; }

        public string Message { get; set; }
        
        public string GameId { get; set; }

        public DateTime OccuredOn { get; set; }
    }
}
