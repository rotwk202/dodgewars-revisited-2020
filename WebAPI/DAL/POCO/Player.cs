﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DAL.POCO
{
    public class Player
    {
        public Player()
        {
            GameDetails = new Collection<GameDetail>();
            MonthlyRatings = new Collection<MonthlyRating>();
            Ratings = new Collection<Rating>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public double Rating { get; set; }
        public double TeamRating { get; set; }
        public double MonthlyRating { get; set; }
        public double MonthlyTeamRating { get; set; }

        public User User { get; set; }
        public virtual ICollection<GameDetail> GameDetails { get; set; }
        public virtual ICollection<MonthlyRating> MonthlyRatings { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}
