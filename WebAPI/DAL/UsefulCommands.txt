﻿***********************
*** Database First	***
***********************

Scaffold-DbContext 'Data Source=DESKTOP-S5Q1TRK\SQLEXPRESS;Initial Catalog=PointsSystem;User Id=sa;Password=lokalny123' Microsoft.EntityFrameworkCore.SqlServer -force
Add-Migration
Add-Database

***********************
***	  Code First	***
***********************

Update-Database
dotnet ef --startup-project Api/Api.csproj migrations add InitialModel -p DAL/DAL.csproj --verbose
dotnet ef database update --startup-project Api/Api.csproj --project DAL/DAL.csproj