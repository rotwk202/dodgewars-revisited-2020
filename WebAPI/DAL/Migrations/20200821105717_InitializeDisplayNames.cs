﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class InitializeDisplayNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                update Player
                set DisplayName = [Name]
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
