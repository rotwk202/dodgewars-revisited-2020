﻿using DAL.Configurations;
using DAL.POCO;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class LadderContext : DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<Player> Player { get; set; }
        public DbSet<Game> Game { get; set; }
        public DbSet<GameDetail> GameDetail { get; set; }
        public DbSet<Rating> Rating { get; set; }
        public DbSet<MonthlyRating> MonthlyRating { get; set; }
        public DbSet<Map> Map { get; set; }
        public DbSet<FailedReport> FailedReport { get; set; }

        public LadderContext(DbContextOptions<LadderContext> options)
            : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UserConfiguration());
            builder.ApplyConfiguration(new PlayerConfiguration());
            builder.ApplyConfiguration(new GameConfiguration());
            builder.ApplyConfiguration(new GameDetailConfiguration());
            builder.ApplyConfiguration(new RatingConfiguration());
            builder.ApplyConfiguration(new MonthlyRatingConfiguration());
            builder.ApplyConfiguration(new MapConfiguration());
        }
    }
}
