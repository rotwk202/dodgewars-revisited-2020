﻿using DAL.POCO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Configurations
{
    public class FailedReportConfiguration : IEntityTypeConfiguration<FailedReport>
    {
        public void Configure(EntityTypeBuilder<FailedReport> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).UseIdentityColumn();
            builder.Property(p => p.Message).IsRequired();
            builder.Property(p => p.GameId).IsRequired();
            builder.Property(p => p.OccuredOn).IsRequired();

            builder.ToTable("FailedReport");
        }
    }
}
