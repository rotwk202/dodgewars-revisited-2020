﻿using DAL.POCO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(m => m.Id);

            builder.Property(m => m.Id).UseIdentityColumn();

            builder.Property(m => m.Email).IsRequired();
            builder.Property(m => m.Password).IsRequired();
            builder.Property(m => m.CreatedOn).IsRequired();

            builder.ToTable("User");
        }
    }
}
