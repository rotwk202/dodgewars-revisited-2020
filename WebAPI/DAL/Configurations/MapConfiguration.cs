﻿using DAL.POCO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Configurations
{
    public class MapConfiguration : IEntityTypeConfiguration<Map>
    {
        public void Configure(EntityTypeBuilder<Map> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).UseIdentityColumn();

            builder.Property(p => p.CodeName).IsRequired();
            builder.Property(p => p.DisplayName).IsRequired();
            builder.Property(p => p.ReplayMapTextId).IsRequired();
            builder.Property(p => p.Image).IsRequired();


            builder.ToTable("Map");
        }
    }
}
