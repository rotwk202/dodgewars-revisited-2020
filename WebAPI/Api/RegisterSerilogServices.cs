﻿using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;

namespace Api
{
    public static class RegisterSerilogServices
    {
        public static IServiceCollection AddSerilogServices(this IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Information()
            .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
            .Enrich.FromLogContext()
            .WriteTo.File(@"Logs\jobs-log.txt")
            .CreateLogger();

            return services.AddSingleton(Log.Logger);
        }
    }
}
