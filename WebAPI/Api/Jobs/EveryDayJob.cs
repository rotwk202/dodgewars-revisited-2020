﻿using Quartz;
using Serilog;
using System.Threading.Tasks;

namespace Api.Jobs
{
    public class EveryDayJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            Log.Information("Every day test job executed.");
        }
    }

}
