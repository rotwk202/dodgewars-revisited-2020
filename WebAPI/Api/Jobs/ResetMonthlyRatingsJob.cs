﻿using BusinessLogic.PointsSystem;
using Dapper;
using Quartz;
using Serilog;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Api.Jobs
{
    //TODO: ciekawe czy tu uda sie rozwiazac ten problem z sql connecition
    public class ResetMonthlyRatingsJob : IJob
    {
        public static string ConnectionString { get; set; }

        public async Task Execute(IJobExecutionContext context)
        {
            using (IDbConnection db = new SqlConnection(ConnectionString))
            {
                var parameters = new { InitialMonthlyRating = Constants.InitialMonthlyRatingValue };

                await db.QueryAsync(@"update [dbo].[Player]
                        set MonthlyRating = @InitialMonthlyRating, MonthlyTeamRating = @InitialMonthlyRating", parameters);

                Log.Information("ResetMonthlyRatings completed.");
            }
        }
    }

}
