﻿using Quartz;
using Serilog;
using System.Threading.Tasks;

namespace Api.Jobs
{
    public class EveryMinuteJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            Log.Information("Every few minutes test job executed.");
        }
    }

}
