﻿using DAL.DbConfig;
using Microsoft.Extensions.Configuration;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Api.Jobs
{
    public static class SchedulerBuilder
    {
        public static async Task StartAsync()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            IScheduler scheduler = await schedulerFactory.GetScheduler().ConfigureAwait(false);
            await scheduler.Start().ConfigureAwait(false);

            ScheduleResetMonthlyRatingsJob(scheduler);
            ScheduleEveryWeekJob(scheduler);
            ScheduleEveryDayJob(scheduler);
            ScheduleEveryHourJob(scheduler);
            ScheduleEveryMinuteJob(scheduler);
        }

        private static void ScheduleResetMonthlyRatingsJob(IScheduler scheduler)
        {
            IJobDetail job = JobBuilder.Create<ResetMonthlyRatingsJob>()
                .WithIdentity("ResetMonthlyRatings")
                .Build();

            ResetMonthlyRatingsJob.ConnectionString = Startup.StaticConfig.GetConnectionString(DbConnectionString.Configured);

            ITrigger trigger = TriggerBuilder
                .Create()
                .StartNow()
                .WithSchedule(CronScheduleBuilder.MonthlyOnDayAndHourAndMinute(1, 2, 0))
                //.WithSimpleSchedule(x => x.WithIntervalInSeconds(10).RepeatForever())
                .Build();

            DiagnoseTrigger(trigger);
            scheduler.ScheduleJob(job, trigger);
        }

        private static void ScheduleEveryWeekJob(IScheduler scheduler)
        {
            IJobDetail job = JobBuilder.Create<EveryWeekJob>()
                .WithIdentity("EveryWeekJob")
                .Build();

            ITrigger trigger = TriggerBuilder
                .Create()
                .StartNow()
                .WithSchedule(CronScheduleBuilder.WeeklyOnDayAndHourAndMinute(System.DayOfWeek.Monday, 2, 0))
                .Build();

            DiagnoseTrigger(trigger);
            scheduler.ScheduleJob(job, trigger);
        }

        private static void ScheduleEveryDayJob(IScheduler scheduler)
        {
            IJobDetail job = JobBuilder.Create<EveryDayJob>()
                .WithIdentity("EveryDayJob")
                .Build();

            ITrigger trigger = TriggerBuilder
                .Create()
                .StartNow()
                .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(1, 0))
                .Build();

            DiagnoseTrigger(trigger);
            scheduler.ScheduleJob(job, trigger);
        }

        private static void ScheduleEveryHourJob(IScheduler scheduler)
        {
            IJobDetail job = JobBuilder.Create<EveryHourJob>()
                .WithIdentity("EveryHourJob")
                .Build();

            ITrigger trigger = TriggerBuilder
                .Create()
                .StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInHours(1).RepeatForever())
                .Build();

            DiagnoseTrigger(trigger);
            scheduler.ScheduleJob(job, trigger);
        }

        private static void ScheduleEveryMinuteJob(IScheduler scheduler)
        {
            IJobDetail job = JobBuilder.Create<EveryMinuteJob>()
                .WithIdentity("EveryMinuteJob")
                .Build();

            ITrigger trigger = TriggerBuilder
                .Create()
                .StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(15).RepeatForever())
                .Build();

            DiagnoseTrigger(trigger);
            scheduler.ScheduleJob(job, trigger);
        }

        private static void DiagnoseTrigger(ITrigger trigger)
        {
            var times = TriggerUtils.ComputeFireTimes(trigger as IOperableTrigger, null, 10);
            foreach (var time in times) Debug.WriteLine(time.ToLocalTime());
        }

    }
}
