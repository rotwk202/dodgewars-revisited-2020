﻿using Quartz;
using Serilog;
using System.Threading.Tasks;

namespace Api.Jobs
{
    public class EveryHourJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            Log.Information("Every few hours test job executed.");
        }
    }

}
