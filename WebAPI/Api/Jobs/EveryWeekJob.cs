﻿using Quartz;
using Serilog;
using System.Threading.Tasks;

namespace Api.Jobs
{
    public class EveryWeekJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            Log.Information("Weekly test job executed.");
        }
    }

}
