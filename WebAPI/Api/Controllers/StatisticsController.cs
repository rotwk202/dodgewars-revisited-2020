﻿using System;
using System.Threading.Tasks;
using BusinessLogic.Services;
using log4net;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticsController : ControllerBase
    {
        private readonly StatisticsService _statisticsService;
        private readonly ILog _logger;

        public StatisticsController(StatisticsService statisticsService)
        {
            _statisticsService = statisticsService;
            _logger = LogManager.GetLogger("log4net-default-repository", "ExceptionLogger");
        }

        [HttpGet]
        [Route("overall")]
        public async Task<IActionResult> GetOverallArmiesStats()
        {
            var result = _statisticsService.GetArmiesStatsInIndividualGames();

            return Ok(result);
        }

        [HttpGet]
        [Route("month-activity")]
        public async Task<IActionResult> GetMonthActivityStats(string dateString)
        {
            try
            {
                var result = await _statisticsService.GetMonthActivityStats(dateString);
                return Ok(result);
            }
            catch (Exception e)
            {
                _logger.Error("Error during getting month activity stats.", e);
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("player-card")]
        public async Task<IActionResult> GetPlayerCard(string playerName, string season)
        {
            try
            {
                var result = await _statisticsService.GetPlayerCard(playerName, season);
                return Ok(result);
            }
            catch (Exception e)
            {
                _logger.Error("Error during getting player card.", e);
                return BadRequest(e.Message);
            }
        }
    }
}
