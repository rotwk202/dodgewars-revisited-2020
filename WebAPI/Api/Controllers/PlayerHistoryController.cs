﻿using System.Threading.Tasks;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerHistoryController : ControllerBase
    {
        private readonly PlayerHistoryService _playerHistoryService;

        public PlayerHistoryController(PlayerHistoryService playerHistoryService)
        {
            _playerHistoryService = playerHistoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetPlayerHistory(int gamesCount, string playerName, int pageNumber, string ladderType)
        {
            var result = await _playerHistoryService.GetHistory(gamesCount, playerName, pageNumber, ladderType);

            return Ok(result);
        }
    }
}