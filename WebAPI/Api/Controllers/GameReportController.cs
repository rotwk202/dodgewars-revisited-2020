﻿using System;
using System.Threading.Tasks;
using BusinessLogic.Services;
using Common.Model;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using ReplayParserTools.Services;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameReportController : ControllerBase
    {
        private readonly GameReportService _gameReportService;
        private readonly GameReportServiceOfTools _gameReportServiceOfTools;
        private readonly AccountService _accountService;
        private readonly AuthenticationService _authenticationService;
        private readonly ReplayFtpService _replayUploadService;
        private readonly ILog _logger;

        public GameReportController(GameReportService gameReportService,
            GameReportServiceOfTools gameReportServiceOfTools,
            AccountService accountService,
            AuthenticationService authenticationService,
            ReplayFtpService replayUploadService)
        {
            _gameReportService = gameReportService;
            _gameReportServiceOfTools = gameReportServiceOfTools;
            _accountService = accountService;
            _authenticationService = authenticationService;
            _replayUploadService = replayUploadService;
            _logger = LogManager.GetLogger("log4net-default-repository", "ExceptionLogger");
        }

        [HttpPost("ReportLoss")]
        public async Task<ActionResult> ReportLoss()
        {
            var res = await Request.ReadFormAsync();
            StringValues userEmail, token, clientName;
            Request.Headers.TryGetValue("Email", out userEmail);
            Request.Headers.TryGetValue("Token", out token);
            Request.Headers.TryGetValue("ClientName", out clientName);
            bool callerIsAdmin = await _accountService.IsValidAdminPwd(token);

            try
            {
                if (callerIsAdmin || await _authenticationService.AuthenticateUser(userEmail, token, clientName))
                {
                    ApiGame apiGame;
                    using (var fileStream = res.Files[0].OpenReadStream())
                    {
                        apiGame = _gameReportServiceOfTools.GetApiGame(fileStream, userEmail.ToString());
                    }

                    var identifier = _gameReportService.ReportLoss(apiGame);
                    using (var fileStream = res.Files[0].OpenReadStream())
                    {
                        _replayUploadService.UploadReplay(identifier, fileStream);
                    }

                    return Ok();
                }

                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.Error("Error during reporting replay", ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("ReportLossRecalculation")]
        public async Task<ActionResult> ReportLossRecalculation()
        {
            var res = await Request.ReadFormAsync();
            StringValues userEmail;
            Request.Headers.TryGetValue("Email", out userEmail);

            try
            {
                ApiGame apiGame;
                using (var fileStream = res.Files[0].OpenReadStream())
                {
                    apiGame = _gameReportServiceOfTools.GetApiGame(fileStream, userEmail.ToString());
                }

                _gameReportService.ResetMonthlyRatings(apiGame);

                var identifier = _gameReportService.ReportLoss(apiGame);
                using (var fileStream = res.Files[0].OpenReadStream())
                {
                    _replayUploadService.UploadReplay(identifier, fileStream);
                }

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.Error("Error during report loss recalculation", ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("process-ar-web-client")]
        public async Task<ActionResult> ProcessARWebClient(IFormCollection data)
        {
            data.TryGetValue("loserPlayerName", out StringValues loserPlayerName);
            data.TryGetValue("adminToken", out StringValues adminToken);

            try
            {
                if (data.Files == null || data.Files.Count == 0)
                {
                    throw new ArgumentNullException("AR replay file was not provided.");
                }
                if(!await _accountService.IsValidAdminPwd(adminToken))
                {
                    throw new UnauthorizedAccessException("You're unable to process AR because given admin token is invalid");
                }

                var userEmail = _accountService.GetUserName(loserPlayerName);
                ApiGame apiGame;
                using (var fileStream = data.Files[0].OpenReadStream())
                {
                    apiGame = _gameReportServiceOfTools.GetApiGame(fileStream, userEmail.ToString());
                }

                var identifier = _gameReportService.ReportLoss(apiGame);
                using (var fileStream = data.Files[0].OpenReadStream())
                {
                    _replayUploadService.UploadReplay(identifier, fileStream);
                }

                return Ok(apiGame.Identifier);
            }
            catch (Exception e)
            {
                _logger.Error("Error during processing AR replay the web way.", e);
                return BadRequest(e.Message);
            }
        }
    }
}