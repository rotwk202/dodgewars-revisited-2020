﻿using System.Threading.Tasks;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly ImageService _imageService;

        public ImageController(ImageService imageService)
        {
            _imageService = imageService;
        }

        [HttpGet]
        [Route("map-image/{mapCodeName}")]
        public async Task<ActionResult> GetMapImage(string mapCodeName)
        {
            var image = _imageService.GetMapImage(mapCodeName);
            return Ok(image);
        }
    }
}