﻿using System;
using System.Threading.Tasks;
using BusinessLogic.POC;
using BusinessLogic.Services;
using log4net;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        private readonly RateService _rateService;
        private readonly ReplayFtpService _replayFtpService;
        private readonly POCService _pocService;
        private readonly ILog _logger;

        public RatingController(RateService rateService,
            ReplayFtpService replayFtpService,
            POCService pocService)
        {
            _rateService = rateService;
            _replayFtpService = replayFtpService;
            _pocService = pocService;
            _logger = LogManager.GetLogger("log4net-default-repository", "ExceptionLogger");
        }

        [HttpGet]
        [Route("recent-games")]
        public async Task<ActionResult> GetRecentGames(int gamesCount, int pageNumber)
        {
            try
            {
                var result = _rateService.GetRecentGames(gamesCount, pageNumber);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.Error("Error trying to get recent games.", ex);

                return BadRequest(ex.Message);
            }
        }

        [HttpGet("individual")]
        public async Task<ActionResult> GetIndividualAllTimeRatings()
        {
            var result = _rateService.GetIndividualAllTimeRatings();

            return Ok(result);
        }

        [HttpGet("monthly")]
        public async Task<ActionResult> GetIndividualMonthlyRatings(string dateString)
        {
            var result = _rateService.GetIndividualMonthlyRatings(dateString);

            return Ok(result);
        }

        [HttpGet("team")]
        public async Task<ActionResult> GetTeamAllTimeRatings()
        {
            var result = _rateService.GetTeamAllTimeRatings();

            return Ok(result);
        }

        [HttpGet("monthly-team")]
        public async Task<ActionResult> GetTeamMonthlyRatings(string dateString)
        {
            var result = _rateService.GetTeamMonthlyRatings(dateString);

            return Ok(result);
        }

        [HttpGet("replay")]
        public async Task<ActionResult> DownloadReplay(string gameIdentifier)
        {
            var file = _replayFtpService.DownloadReplay(gameIdentifier);

            var contentType = "application/octet-stream";
            var fileName = $"Game_{gameIdentifier}_Replay.BfME2Replay";
            return File(file, contentType, fileName);
        }

        [HttpGet]
        [Route("games-count-data")]
        public async Task<ActionResult> GetGamesCountData()
        {
            var data = _rateService.GetGamesCountData();

            return Ok(data);
        }

        [HttpGet]
        [Route("get-player-medals")]
        public async Task<ActionResult> GetPlayerMedals(string playerName, string season)
        {
            try
            {
                var result = await _pocService.GetTotalPlayerWinsMedal(season, playerName);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.Error("Error trying to get recent games.", ex);

                return BadRequest(ex.Message);
            }
        }
    }
}
