﻿using System;
using System.Threading.Tasks;
using BusinessLogic.Services;
using Common;
using Common.Model;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AuthenticationService _authenticationService;
        private readonly AccountService _accountService;
        private readonly ILog _logger;

        public AccountController(AuthenticationService authenticationService,
            AccountService accountService)
        {
            _authenticationService = authenticationService;
            _accountService = accountService;
            _logger = LogManager.GetLogger("log4net-default-repository", "AccountControllerLogger");
        }

        [HttpPost]
        [Route("Test")]
        public async Task<ActionResult> Test()
        {
            return Ok();
        }

        [HttpPost]
        [Route("GetToken")]
        public async Task<ActionResult> GetToken(ApiUser user)
        {
            try
            {
                if (user == null)
                {
                    throw new ArgumentNullException(nameof(user), "User was not provided.");
                }

                if (string.IsNullOrEmpty(user.Email) || string.IsNullOrEmpty(user.Password))
                {
                    throw new ArgumentException("Either Email or Password were not provided.");
                }

                if(user.Email == Constants.SecretAdmin)
                {
                    string adminToken = await _authenticationService.GetAdminToken(user);
                    return Ok(adminToken);
                }

                StringValues clientName;
                Request.Headers.TryGetValue("ClientName", out clientName);
                string token = await _authenticationService.GetToken(user, clientName);
                return Ok(token);
            }
            catch (Exception ex)
            {
                _logger.Error("Error trying to get token", ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("CreateNickname")]
        public async Task<ActionResult> CreateNickname(string userEmail, string nickname, string nicknameDisplay)
        {
            var errorMessage = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(nickname))
                {
                    throw new ArgumentNullException(nameof(nickname), "In game name was not provided");
                }
                if (string.IsNullOrEmpty(nicknameDisplay))
                {
                    throw new ArgumentNullException(nameof(nicknameDisplay), "League display name was not provided");
                }

                StringValues clientName;
                StringValues token;
                Request.Headers.TryGetValue("ClientName", out clientName);
                Request.Headers.TryGetValue("Token", out token);

                if (await _authenticationService.IsTokenOutOfDate(token, clientName, DateTime.Now))
                {
                    errorMessage = "Your token has expired.";
                    return Unauthorized(errorMessage);
                }


                var wasNicknameSuccessfullyCreated = await _accountService.CreatePlayer(userEmail, nickname, nicknameDisplay);

                if (wasNicknameSuccessfullyCreated)
                {
                    var result = new
                    {
                        Message =
                        $"In game name: {nickname} (league display name: {nicknameDisplay}) has been successfully created."
                    };

                    return Ok(result);
                }
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            return BadRequest(errorMessage);
        }

        [HttpPut]
        [Route("player-name")]
        public async Task<ActionResult> UpdatePlayerName(string userEmail, string playerName, string playerDisplayName)
        {
            var errorMessage = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(playerName))
                {
                    throw new ArgumentNullException(nameof(playerName), "In game name was not provided");
                }
                if (string.IsNullOrEmpty(playerDisplayName))
                {
                    throw new ArgumentNullException(nameof(playerDisplayName), "League display name was not provided");
                }

                StringValues clientName;
                StringValues token;
                Request.Headers.TryGetValue("ClientName", out clientName);
                Request.Headers.TryGetValue("Token", out token);

                if (await _authenticationService.IsTokenOutOfDate(token, clientName, DateTime.Now))
                {
                    errorMessage = "Your token has expired.";
                    return Unauthorized(errorMessage);
                }

                var wasSuccessfullyUpdated = await _accountService.UpdatePlayerName(userEmail, playerName, playerDisplayName);

                if (wasSuccessfullyUpdated)
                {
                    var result = new
                    {
                        Message =
                        $"In game name {playerName} league display name changed to {playerDisplayName} successfully."
                    };

                    return Ok(result);
                }
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            return BadRequest(errorMessage);
        }

        [HttpPost]
        [Route("RegisterUser")]
        public async Task<ActionResult> RegisterUser(ApiUser user)
        {
            var errorMessage = string.Empty;

            try
            {
                if (user == null)
                {
                    throw new ArgumentNullException(nameof(user), "No user was provided.");
                }

                StringValues clientName;
                Request.Headers.TryGetValue("ClientName", out clientName);
                var wasSuccessfullyRegistered = await _accountService.RegisterUser(user, clientName);
                if (wasSuccessfullyRegistered)
                {
                    var result = new
                    {
                        Message =
                        $"The user has been successfully registered."
                    };

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error while registering user", ex);
                errorMessage = ex.Message;
            }

            return BadRequest(errorMessage);
        }

        [HttpPut("reset-password")]
        public async Task<ActionResult> ResetPassword(ApiUser user)
        {
            try
            {
                await _accountService.ResetPassword(user);
                return Ok("Password reset successfully.");
            }
            catch (Exception ex)
            {
                _logger.Error("Error while resetting password.", ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("login-nicknames")]
        public async Task<ActionResult> GetLoginNicknames(string login)
        {
            try
            {
                var loginNicknames = await _accountService.GetPlayerNames(login);
                return Ok(loginNicknames);
            }
            catch (Exception ex)
            {
                _logger.Error("Error while getting login nicknames.", ex);
                return BadRequest(ex.Message);
            }
        }
        
        [HttpGet("all-accounts")]
        public async Task<ActionResult> GetAllAccs(string secret)
        {
            try
            {
                if (!await _accountService.IsValidAdminPwd(secret))
                {
                    throw new Exception("Error trying to authenticate admin. Wrong password provided.");
                }

                var loginNicknames = await _accountService.GetAllAccountNames();
                return Ok(loginNicknames);
            }
            catch (Exception ex)
            {
                _logger.Error("Error while doing GetAllAccs.", ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("all-nicknames")]
        public async Task<ActionResult> GetAllNicks(string secret)
        {
            try
            {
                if (!await _accountService.IsValidAdminPwd(secret))
                {
                    throw new Exception("Error trying to authenticate admin. Wrong password provided.");
                }

                var loginNicknames = await _accountService.GetAllNickNames();
                return Ok(loginNicknames);
            }
            catch (Exception ex)
            {
                _logger.Error("Error while doing GetAllNicks.", ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("all-nicknames-public")]
        public async Task<ActionResult> GetAllNicksPublic()
        {
            try
            {
                var loginNicknames = await _accountService.GetAllNickNamesPublic();
                return Ok(loginNicknames);
            }
            catch (Exception ex)
            {
                _logger.Error("Error while doing GetAllNicksPublic.", ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("is-admin")]
        public async Task<ActionResult> IsAdminUser(string secret)
        {
            try
            {
                if (!await _accountService.IsValidAdminPwd(secret))
                {
                    throw new Exception("Error trying to authenticate admin. Wrong password provided.");
                }

                return Ok(true);
            }
            catch (Exception ex)
            {
                _logger.Error("Error while calling is-admin method.", ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("player-names")]
        public async Task<ActionResult> GetPlayerNames(string login)
        {
            try
            {
                var loginNicknames = await _accountService.GetPlayerNamesDisplayNames(login);
                return Ok(loginNicknames);
            }
            catch (Exception ex)
            {
                _logger.Error("Error while getting login names display names.", ex);
                return BadRequest(ex.Message);
            }
        }

    }
}