﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogic.Services;
using Common.Model;
using log4net;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly AdminService _adminService;
        private readonly ILog _logger;

        public AdminController(AdminService adminService)
        {
            _adminService = adminService;
            _logger = LogManager.GetLogger("log4net-default-repository", "ExceptionLogger");
        }

        //[HttpGet]
        //[Route("GetRecalcReplayAndLogins")]
        //public async Task<ActionResult> GetRecalcReplayAndLogins()
        //{
        //    var zipFile = _adminService.GetRecalcReplayAndLogins();

        //    const string contentType = "application/zip";
        //    HttpContext.Response.ContentType = contentType;
        //    var result = new FileContentResult(zipFile, contentType)
        //    {
        //        FileDownloadName = $"AllReplayReportDetails.zip"
        //    };

        //    return result;
        //}

        //[HttpGet]
        //[Route("DownloadRepsWithUniqueIds")]
        //public async Task<ActionResult> DownloadRepsWithUniqueIds()
        //{
        //    var zipFile = _adminService.DownloadRepsWithUniqueIds();

        //    const string contentType = "application/zip";
        //    HttpContext.Response.ContentType = contentType;
        //    var result = new FileContentResult(zipFile, contentType)
        //    {
        //        FileDownloadName = $"AllReplayReportDetails.zip"
        //    };

        //    return result;
        //}

        [HttpPost]
        [Route("UploadMaps")]
        public async Task<ActionResult> UploadMaps(List<ApiMap> mapModels)
        {
            try
            {
                var successfullyAdded = await _adminService.UploadAllMaps(mapModels);
                return Ok("Maps were uploaded successfully.");
            }
            catch (Exception ex)
            {
                _logger.Error("Error during uploading maps.", ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("game-primarykeyids")]
        public async Task<IActionResult> GetGamePrimaryKeyIds()
        {
            try
            {
                var result = await _adminService.GetGamePrimaryKeyIds();

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.Error("Error during GetGamePrimaryKeyIds.", ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("delete-game")]
        public async Task<IActionResult> DeleteGame(int gamePrimaryKeyId)
        {
            try
            {
                await _adminService.DeleteGame(gamePrimaryKeyId);

                return Ok($"Game of display ID {gamePrimaryKeyId} was removed successfully.");
            }
            catch (Exception ex)
            {
                _logger.Error("Error during deleting games.", ex);
                return BadRequest(ex.Message);
            }
        }

    }
}