﻿using System;
using System.Threading.Tasks;
using Common.Services;
using log4net;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FailedReportController : ControllerBase
    {
        private readonly FailedReportService _failedReportService;
        private readonly ILog _logger;

        public FailedReportController(FailedReportService failedReportService)
        {
            _failedReportService = failedReportService;
            _logger = LogManager.GetLogger("log4net-default-repository", "ExceptionLogger");
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            try
            {
                var result = _failedReportService.GetAllEntries();

                return Ok(result);
            }
            catch (Exception e)
            {
                _logger.Error("Error reading failed reports.", e);
                return BadRequest(e.Message);
            }
        }
    }
}
