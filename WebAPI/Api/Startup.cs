using Api.Jobs;
using BusinessLogic.Services;
using BusinessLogic.Validators;
using Common.Interfaces;
using Common.Services;
using Common.Model;
using Common.Tools;
//using DALMySql;
//using DAL.DbConfig;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ReplayParserTools.Services;
using System;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Threading;
using Newtonsoft.Json;
using BusinessLogic.POC;
using System.Text.Json.Serialization;
using Polly;
using MySql.Data.MySqlClient;
using DALMySql;
using DALMySql.DbConfig;

namespace Api
{
    public class Startup
    {
        public static IConfiguration StaticConfig { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            StaticConfig = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options => {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                }
            );

            services.AddScoped(typeof(LadderContext), typeof(LadderContext));

            services.AddTransient(typeof(AuthenticationService), typeof(AuthenticationService));
            services.AddTransient(typeof(GameReportService), typeof(GameReportService));
            services.AddTransient(typeof(GameValidator), typeof(GameValidator));
            services.AddTransient(typeof(GameDataService), typeof(GameDataService));
            services.AddTransient(typeof(AdminService), typeof(AdminService));
            services.AddTransient(typeof(GameReportServiceOfTools), typeof(GameReportServiceOfTools));
            services.AddTransient(typeof(RateService), typeof(RateService));
            services.AddTransient(typeof(POCService), typeof(POCService));
            services.AddTransient(typeof(ImageService), typeof(ImageService));
            services.AddTransient(typeof(AccountService), typeof(AccountService));
            services.AddTransient(typeof(AuthenticationService), typeof(AuthenticationService));
            services.AddTransient(typeof(PlayerHistoryService), typeof(PlayerHistoryService));
            services.AddTransient(typeof(FailedReportService), typeof(FailedReportService));
            services.AddTransient(typeof(StatisticsService), typeof(StatisticsService));
            services.AddTransient(typeof(ReplayFtpService), typeof(ReplayFtpService));
            services.AddTransient(typeof(IConfigProvider), typeof(ConfigProvider));
            services.AddSerilogServices();

            if (!Configuration.GetValue<bool>("IsSqlServer"))
            {
                services.AddSingleton<ISqlConnectionProvider, MySqlConnectionProvider>();
            }
            

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                               .AllowAnyMethod()
                               .AllowAnyHeader();
                    });
            });

            var connectionString = Configuration.GetConnectionString(DbConnectionString.Configured);


            //.UseSqlServer(connectionString, x => x.MigrationsAssembly("DAL")));
            //services.AddDbContext<LadderContext>(options => options.UseMySQL.UseMySql(connectionString, x => x.MigrationsAssembly("DAL")));


            //var builder = new MySqlConnectionStringBuilder();
            //builder.Server = "192.168.0.105,3306";
            //builder.Database = "RotwkLeague_6000games";
            //builder.UserID = "root";
            //builder.Password = "my-secret-pw";

            services.AddDbContext<LadderContext>(options => options.UseMySQL(connectionString, x => x.MigrationsAssembly("DALMySql")));

            Task.Run(async () => await SchedulerBuilder.StartAsync().ConfigureAwait(false));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, LadderContext dataContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            loggerFactory.AddLog4Net();

            app.UseCors("AllowAll");

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var builder = new MySqlConnectionStringBuilder();
            builder.Server = "192.168.0.105,3306";
            builder.Database = "RotwkLeague_6000games";
            builder.UserID = "root";
            builder.Password = "my-secret-pw";

            var connectionString = Configuration.GetConnectionString(DbConnectionString.Configured);
            //var connectionString = builder.ToString();
            
            var retryPolicy = Policy.Handle<Exception>()
                .WaitAndRetryForever(i => TimeSpan.FromSeconds(5));

            retryPolicy.Execute(() => NewMethod(dataContext, connectionString));
        }

        private static void NewMethod(LadderContext dataContext, string connectionString)
        {
            var builder = new MySqlConnectionStringBuilder(connectionString);
            builder.Database = "master";

            using var connection = new MySqlConnection(builder.ToString());
            connection.Open();
            dataContext.Database.Migrate();
        }
    }
}
